<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}
	public function index()
	{

		// $this->load->vars($data);
		// $this->template->load('template/template', 'login');
		$this->load->view('login');
	}

	public function dologin()
	{

		$username 	= $this->input->post('username');
		$tahun_anggaran  = $this->input->post('tahun_anggaran');
		$password = $this->input->post('password');

		$data = $this->login_model->cek_login($username,  $password);


		if (!empty($data)) {
			$newdata = array(
				'username'  => $data['username'],
				'tahun_anggaran'     => $tahun_anggaran,
				'nama_pegawai' => $data['nama_pegawai'],
				'nip_pegawai_lama' => $data['nip_pegawai_lama'],
				'nip_pegawai' => $data['nip_pegawai'],
				'kode_satker' => $data['kode_satker'],
				'kode_bidang' => $data['kode_bidang'],
				'kode_unit_kerja' => $data['kode_unit_kerja'],
				'nama_unit_kerja' => $data['nama_unit_kerja'],
				'nama_satker' => $data['nama_satker'],
				'pangkat' => $data['pangkat'],
				'level_user' => $data['level_user'],
				'logged_in' => TRUE,
				'kode_jabatan_fungsional' => $data['kode_jabatan_fungsional'],


			);

			$this->session->set_userdata($newdata);
			redirect('home');
		} else {

			echo "gagal";
			redirect('login');
		}
	}

	function logout() // fungsi logout
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}
