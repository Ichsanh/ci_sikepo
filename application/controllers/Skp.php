<?php
defined('BASEPATH') or exit('No direct script access allowed');
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Skp extends CI_Controller
{

    

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }

        $this->load->model('pegawai_model');
        $this->load->model('kegiatan_model');
        $this->load->model('skp_model');
        $this->load->model('matriks_kinerja_model');
        $this->load->model('master_jabatan_fungsional_model');
        $this->load->model('master_butir_fungsional_model');
        
    }
    public function index()
    {
        $pegawai = $this->pegawai_model->get_pegawai();
        $data['data'] = null;
        $data['tahun'] = null;
        $data['pegawai_terpilih'] = null;
        $data['pegawai'] = $pegawai;
        $this->load->vars($data);
        $this->template->load('template/template', 'skp/skp');
    }

    public function lihat_skp()
    {

        $nip_pegawai = $this->input->get('nip_pegawai');
        $tahun = $this->input->get('tahun');
        $data_skp = $this->skp_model->get_skp($nip_pegawai, $tahun);
        $pegawai_terpilih = $this->pegawai_model->get_pegawai_nip($nip_pegawai);
        $pegawai = $this->pegawai_model->get_pegawai();
        if (empty($data_skp[0]['id_skp'])) {
            $data['data'] = null;
        } else {
            $data['data'] = $data_skp;
        }
        

        $kode_jenis_jabatan_fungsional = $this->master_jabatan_fungsional_model->get_id($pegawai_terpilih['kode_jabatan_fungsional']);
        $butir_fungsional = $this->master_butir_fungsional_model->get_butir_tingkat_jabatan($kode_jenis_jabatan_fungsional['kode_jenis_jabatan_fungsional'],$kode_jenis_jabatan_fungsional['tingkat_jabatan']);

        $data['tahun'] = $tahun;
        $data['pegawai_terpilih'] = $pegawai_terpilih;
        $data['pegawai'] = $pegawai;
        $data['nip_pegawai'] = $nip_pegawai;
        $data['butir_fungsional'] = $butir_fungsional;
        $this->load->vars($data);
        $this->template->load('template/template', 'skp/skp');

    }
    

    function tgl_indo($tanggal){
        $tahun = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        
        return $pecahkan[2] . ' ' . $tahun[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    public function download_skp($nip_pegawai, $tahun)
    {
        $data = $this->kegiatan_model->get_all();
        
        // $nip_pegawai = $this->input->get('nip_pegawai');
        // $tahun = $this->input->get('bulan');
        $a_date = $this->session->userdata('tahun_anggaran')."-".$tahun."-01";
        $awal_bulan = $this->tgl_indo($a_date);
        $akhir_bulan = $this->tgl_indo(date("Y-m-t", strtotime($a_date)));
        $data_ckp = $this->ckp_model->get_ckp($nip_pegawai, $tahun,1);
        $baris_data_ckp = count($data_ckp);
        $data_ckp_tambahan = $this->ckp_model->get_ckp($nip_pegawai, $tahun,0);
        $baris_data_ckp_tambahan = count($data_ckp_tambahan);

        $pegawai = $this->pegawai_model->get_pegawai_nip($nip_pegawai);

        if (empty($data_ckp[0]['id_skp'])) {
            $data_ckp = null;
        } 
        if (empty($data_ckp_tambahan[0]['id_skp'])) {
            $data_ckp_tambahan = null;
        } 

        $spreadsheet = new Spreadsheet();
        
        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Segoe UI')
            ->setSize(10);


        $sheet = $spreadsheet->getActiveSheet();
        
        $sheet->setCellValue('A2', 'FORMULIR SASARAN KERJA');
        $sheet->mergeCells('A2:K2');
        $sheet->setCellValue('A3', 'PEGAWAI NEGERI SIPIL');
        $sheet->mergeCells('A3:K3');

        $sheet->setCellValue('A4', 'NO');
        $sheet->setCellValue('A5', '1');
        $sheet->setCellValue('A6', '2');
        $sheet->setCellValue('A7', '3');
        $sheet->setCellValue('A8', '4');
        $sheet->setCellValue('A9', '5');

        $sheet->setCellValue('B4', 'I. PEJABAT PENILAI');
        $sheet->mergeCells('B4:C4');

        $sheet->setCellValue('B5', 'Nama');
        $sheet->setCellValue('B6', 'NIP');
        $sheet->setCellValue('B7', 'Pangkat/Gol.Ruang');
        $sheet->setCellValue('B8', 'Jabatan');
        $sheet->setCellValue('B9', 'Unit Kerja');

        $sheet->setCellValue('C5', $pegawai['nama_pegawai']);
        $sheet->setCellValue('C6', $pegawai['nIP_pegawai']);
        $sheet->setCellValue('C7', 'Pangkat/Gol.Ruang');
        $sheet->setCellValue('C8', $pegawai['nama_jabatan_fungsional']);
        $sheet->setCellValue('C9', 'Unit Kerja');

        $sheet->mergeCells('C5:D5');
        $sheet->mergeCells('C6:D6');
        $sheet->mergeCells('C7:D7');
        $sheet->mergeCells('C8:D8');
        $sheet->mergeCells('C9:D9');

        $sheet->setCellValue('E4', 'NO');
        $sheet->setCellValue('E5', '1');
        $sheet->setCellValue('E6', '2');
        $sheet->setCellValue('E7', '3');
        $sheet->setCellValue('E8', '4');
        $sheet->setCellValue('E9', '5');

        $sheet->setCellValue('F4', 'II. PEGAWAI NEGERI SIPIL YANG DINILAI');
        $sheet->mergeCells('F4:K4');

        $sheet->setCellValue('F5', 'Nama');
        $sheet->setCellValue('F6', 'NIP');
        $sheet->setCellValue('F7', 'Pangkat/Gol.Ruang');
        $sheet->setCellValue('F8', 'Jabatan');
        $sheet->setCellValue('F9', 'Unit Kerja');

        $sheet->mergeCells('F5:G5');
        $sheet->mergeCells('F6:G6');
        $sheet->mergeCells('F7:G7');
        $sheet->mergeCells('F8:G8');
        $sheet->mergeCells('F9:G9');

        $sheet->setCellValue('H5', $pegawai['nama_pegawai']);
        $sheet->setCellValue('H6', $pegawai['nIP_pegawai']);
        $sheet->setCellValue('H7', 'Pangkat/Gol.Ruang');
        $sheet->setCellValue('H8', $pegawai['nama_jabatan_fungsional']);
        $sheet->setCellValue('H9', 'Unit Kerja');

        $sheet->mergeCells('H5:K5');
        $sheet->mergeCells('H6:K6');
        $sheet->mergeCells('H7:K7');
        $sheet->mergeCells('H8:K8');
        $sheet->mergeCells('H9:K9');
        
        $sheet->setCellValue('A10', 'N0');
        $sheet->mergeCells('A10:A11');

        $sheet->setCellValue('B10', 'III. KEGIATAN TUGAS JABATAN');
        $sheet->mergeCells('B10:D11');

        $sheet->setCellValue('E10', 'AK');
        $sheet->mergeCells('E10:E11');

        $sheet->setCellValue('F10', 'TARGET');
        $sheet->mergeCells('F10:K10');

        $sheet->setCellValue('F11', 'KUANT/OUTPUT');
        $sheet->mergeCells('F11:G11');

        $sheet->setCellValue('H11', 'KUAL/MUTU');  
        
        $sheet->setCellValue('I11', 'WAKTU');
        $sheet->mergeCells('I11:J11');

        $sheet->setCellValue('K11', 'BIAYA');
        
        

        foreach ($data_skp as $key => $value) {
            $sheet->setCellValue('A'.($key+13), ($key+1));
            $sheet->setCellValue('B'.($key+13), $value['nama_pekerjaan_non_pok'].$value['rincian_pekerjaan'].' '.$value['nama_kegiatan']);
            $sheet->mergeCells('B'.($key+13).':D'.($key+13));
            $sheet->setCellValue('E'.($key+13), $value['ak']);
            $sheet->setCellValue('F'.($key+13), $value['sum_target']);
            $sheet->setCellValue('G'.($key+13), $value['satuan']);
            $sheet->setCellValue('H'.($key+13), $value['kualitas']);
            $sheet->setCellValue('I'.($key+13), $value['waktu']);
            $sheet->setCellValue('J'.($key+13), 'Bulan');
            $sheet->setCellValue('K'.($key+13), '-');

        }

        
        $cell_tambahan = $baris_data_ckp+12+1 ;
        
        $sheet->setCellValue('A'.($cell_tambahan), 'TAMBAHAN');
        $sheet->getStyle('A'.($cell_tambahan))->getFont()->setBold(true);
        $merge_tambahan = 'A'.$cell_tambahan.':'.'C'.$cell_tambahan;
        $sheet->mergeCells($merge_tambahan);

        

        
        $atasan = $this->pegawai_model->get_atasan($pegawai['kode_unit_kerja'], $pegawai['kode_jabatan_struktur']);

        if ($pegawai['kode_jabatan_struktur'] == 0 ) {
            $sheet->setCellValue('C'.($cell_jumlah+10), $pegawai['nama_pegawai']);
            $sheet->setCellValue('G'.($cell_jumlah+10), "silahkan update profil anda");
            $sheet->setCellValue('C'.($cell_jumlah+11), $pegawai['nip_pegawai']);
            $sheet->setCellValue('G'.($cell_jumlah+11), "silahkan update profil anda");
        } else {
            $sheet->setCellValue('C'.($cell_jumlah+10), $pegawai['nama_pegawai']);
            $sheet->setCellValue('G'.($cell_jumlah+10), $atasan['nama_pegawai']);
            $sheet->setCellValueExplicit('C'.($cell_jumlah+11), $pegawai['nip_pegawai'],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('G'.($cell_jumlah+11), $atasan['nip_pegawai'],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        }
        

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
        ];
        $style_ckp_atas = [
            'font' => [
                'bold'  =>  true,
                'size'  =>  16,
                'name'  =>  'Arrus Blk BT'
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                // 'wrapText' => true
            ],
        ];
        $style_bold = [
            'font' => [
                'bold'  =>  true,
                // 'size'  =>  14,
                // 'name'  =>  'Arial'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ],
            
        ];
        $style_center = [
            
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                // 'wrapText' => true
            ],
            
        ];

        $sheet->getStyle('K1')->applyFromArray($style_ckp_atas);
        $sheet->getStyle('A2:K2')->applyFromArray($style_bold);
        $sheet->getStyle('A9:K10')->applyFromArray($style_bold);
        $sheet->getStyle('A11:K11')->applyFromArray($style_center);
        $sheet->getStyle('A'.($cell_jumlah).':K'.($cell_jumlah+2))->applyFromArray($style_bold);
        $sheet->getStyle('A9:K'.($cell_jumlah+2))->applyFromArray($styleArray);

        $sheet->getStyle('C'.($cell_jumlah+7))->applyFromArray($style_center);
        $sheet->getStyle('G'.($cell_jumlah+7))->applyFromArray($style_center);
        $sheet->getStyle('C'.($cell_jumlah+10))->applyFromArray($style_center);
        $sheet->getStyle('G'.($cell_jumlah+10))->applyFromArray($style_center);
        $sheet->getStyle('C'.($cell_jumlah+11))->applyFromArray($style_center);
        $sheet->getStyle('G'.($cell_jumlah+11))->applyFromArray($style_center);
        $writer = new Xlsx($spreadsheet);

        $filename = "CKP";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}