<?php
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
    header('Access-Control-Allow-Headers: token, Content-Type');
    header('Access-Control-Max-Age: 1728000');
    header('Content-Length: 0');
    header('Content-Type: text/plain');
    die();
}

header('Access-Control-Allow-Origin: http://localhost:8100 ');
header('Content-Type: application/json');

require APPPATH . 'libraries/REST_Controller.php';

class Student extends REST_Controller
{

    // construct
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
    }

    // method index untuk menampilkan semua data student menggunakan method get
    public function index_get()
    {
        $response = $this->student_model->all_student();
        $this->response($response);
    }
    // method index untuk menampilkan semua data student menggunakan method get
    public function student_get($id)
    {
        $response = $this->student_model->student_id($id);
        $this->response($response);
    }

    // untuk menambah student menaggunakan method post
    public function login_post()
    {
        $response = $this->student_model->login_student(
            $this->post('username'),
            $this->post('password')
        );
        //$response = $this->post('username');
        $this->response($response);
    }
    // untuk menambah student menaggunakan method post
    public function add_post()
    {
        $response = $this->student_model->add_student(
            $this->post('name'),
            $this->post('address'),
            $this->post('age')
        );
        $this->response($response);
    }

    // update data student menggunakan method put
    public function update_put()
    {
        $response = $this->student_model->update_student(
            $this->put('id'),
            $this->put('name'),
            $this->put('address'),
            $this->put('age')
        );
        $this->response($response);
    }

    // hapus data student menggunakan method delete
    public function delete_delete($id)
    {
        // $response = $this->student_model->delete_student(
        //     $this->delete('id')
        // );
        $response = $this->student_model->delete_student($id);
        $this->response($response);
    }
}
