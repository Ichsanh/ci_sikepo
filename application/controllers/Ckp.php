<?php
defined('BASEPATH') or exit('No direct script access allowed');
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Ckp extends CI_Controller
{

    

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }

        $this->load->model('pegawai_model');
        $this->load->model('kegiatan_model');
        $this->load->model('ckp_model');
        $this->load->model('matriks_kinerja_model');
        $this->load->model('master_jabatan_fungsional_model');
        $this->load->model('master_butir_fungsional_model');
        
    }
    public function index()
    {
        $pegawai = $this->pegawai_model->get_pegawai();
        $data['data'] = null;
        $data['bulan'] = null;
        $data['pegawai_terpilih'] = null;
        $data['pegawai'] = $pegawai;
        $this->load->vars($data);
        $this->template->load('template/template', 'ckp/ckp');
    }

    public function lihat_ckp()
    {

        $nip_pegawai = $this->input->get('nip_pegawai');
        $bulan = $this->input->get('bulan');
        $data_ckp = $this->ckp_model->get_ckp($nip_pegawai, $bulan,1);
        $data_ckp_tambahan = $this->ckp_model->get_ckp($nip_pegawai, $bulan,0);
        // print_r($data_ckp);
        // exit;
        $pegawai_terpilih = $this->pegawai_model->get_pegawai_nip($nip_pegawai);
        $pegawai = $this->pegawai_model->get_pegawai();
        if (empty($data_ckp[0]['id_skp'])) {
            $data['data'] = null;
        } else {
            $data['data'] = $data_ckp;
        }
        if (empty($data_ckp_tambahan[0]['id_skp'])) {
            $data['data_tambahan'] = null;
        } else {
            $data['data_tambahan'] = $data_ckp_tambahan;
        }

        $kode_jenis_jabatan_fungsional = $this->master_jabatan_fungsional_model->get_id($pegawai_terpilih['kode_jabatan_fungsional']);
        $butir_fungsional = $this->master_butir_fungsional_model->get_butir_tingkat_jabatan($kode_jenis_jabatan_fungsional['kode_jenis_jabatan_fungsional'],$kode_jenis_jabatan_fungsional['tingkat_jabatan']);

        $data['bulan'] = $bulan;
        $data['pegawai_terpilih'] = $pegawai_terpilih;
        $data['pegawai'] = $pegawai;
        $data['nip_pegawai'] = $nip_pegawai;
        $data['butir_fungsional'] = $butir_fungsional;
        $this->load->vars($data);
        $this->template->load('template/template', 'ckp/ckp');

    }
    public function update_ckp()
    {
        $size = count($_POST['kualitas']) - 10;
        $i = 0;
        
        
        while (
            $i < $size
        ) {

            $id_skp = $_POST['id_skp'][$i];
            $realisasi = (!empty($_POST['realisasi'][$i])) ? $_POST['realisasi'][$i] : null;
            $kualitas = (!empty($_POST['kualitas'][$i])) ? $_POST['kualitas'][$i] : null;
            $params = array(
                'id_skp' => $id_skp,
                'realisasi' => $realisasi,
                'kualitas' => $kualitas,
            );

            $this->ckp_model->update($id_skp, $params);
            ++$i;
        }
        $i = 50;
        while (
            $i < 55
        ) {
            if (!empty($_POST['nama_pekerjaan_non_pok'][$i])) {
                $params = array(
                'nama_pekerjaan_non_pok' => $_POST['nama_pekerjaan_non_pok'][$i],
                'nip_pegawai_mandiri' => $_POST['nip_pegawai'][$i],
                'bulan_mandiri' => $_POST['bulan'][$i],
                'target_mandiri' => $_POST['target'][$i],
                'satuan' => $_POST['satuan'][$i],
                'flag_jenis_matriks' => 3,
                'utama' => 1,
                );

                $id_matriks = $this->matriks_kinerja_model->tambah($params);

            
                $realisasi = (!empty($_POST['realisasi'][$i])) ? $_POST['realisasi'][$i] : null;
                $kualitas = (!empty($_POST['kualitas'][$i])) ? $_POST['kualitas'][$i] : null;
            
                $params = array(

                'nip_pegawai' => $_POST['nip_pegawai'][$i],
                
                'target' => $_POST['target'][$i],
                'id_matriks' => $id_matriks,
                'kode_jabatan_fungsional' => $this->session->userdata('kode_jabatan_fungsional'),
                'tahun' => $this->session->userdata('tahun_anggaran'),
                'realisasi' => $realisasi,
                'kualitas' => $kualitas,
                'flag_jenis_matriks' => 3,
                'bulan' => $_POST['bulan'][$i],

                );
                // print_r($params);
                // exit;

                $this->matriks_kinerja_model->tambah_kinerja_individu($params);
                
            }
            ++$i;
        }
        $i = 70;
        while (
            $i < 75
        ) { 
            if (!empty($_POST['nama_pekerjaan_non_pok'][$i])) {
                    $params = array(
                    'nama_pekerjaan_non_pok' => $_POST['nama_pekerjaan_non_pok'][$i],
                    'nip_pegawai_mandiri' => $_POST['nip_pegawai'][$i],
                    'bulan_mandiri' => $_POST['bulan'][$i],
                    'target_mandiri' => $_POST['target'][$i],
                    'satuan' => $_POST['satuan'][$i],
                    'flag_jenis_matriks' => 3,
                    'utama' => 0,
                    );

                    $id_matriks = $this->matriks_kinerja_model->tambah($params);
                
                    $realisasi = (!empty($_POST['realisasi'][$i])) ? $_POST['realisasi'][$i] : null;
                    $kualitas = (!empty($_POST['kualitas'][$i])) ? $_POST['kualitas'][$i] : null;
                
                    $params = array(

                    'nip_pegawai' => $_POST['nip_pegawai'][$i],
                    
                    'target' => $_POST['target'][$i],
                    'id_matriks' => $id_matriks,
                    'kode_jabatan_fungsional' => $this->session->userdata('kode_jabatan_fungsional'),
                    'tahun' => $this->session->userdata('tahun_anggaran'),
                    'realisasi' => $realisasi,
                    'kualitas' => $kualitas,
                    'flag_jenis_matriks' => 3,
                    'bulan' => $_POST['bulan'][$i],

                    );

                    $this->matriks_kinerja_model->tambah_kinerja_individu($params);
                    
            }
            ++$i;
        }
        $this->session->set_flashdata('sukses', "Data berhasil diupdate");
        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }

    public function link_ak()
    {
        $id_skp = $_POST['id_skp'];
        $id_butir = $_POST['id_butir'];
        $nip_pegawai = $_POST['nip_pegawai'];
        $pegawai = $this->pegawai_model->get_pegawai_nip($nip_pegawai);
        $butir = $this->master_butir_fungsional_model->get_id($id_butir);
        
        if ( $butir['kode_jabatan_fungsional'] <= $pegawai['kode_jabatan_fungsional']) {
            $nilai_ak = $butir['ak'];
        } elseif($butir['kode_jabatan_fungsional'] > $pegawai['kode_jabatan_fungsional']) {
            $nilai_ak = ($butir['ak'] * 0.8);
        }

        $params = array(
            'id_skp' => $_POST['id_skp'],
            'nilai_ak' => $nilai_ak,
            'id_butir' => $_POST['id_butir']);
        
        $this->ckp_model->update($id_skp, $params);
        print_r($params);
        
    }

    public function download_ckp($nip_pegawai, $bulan)
    {
        $data = $this->kegiatan_model->get_all();
        
        // $nip_pegawai = $this->input->get('nip_pegawai');
        // $bulan = $this->input->get('bulan');
        $a_date = $this->session->userdata('tahun_anggaran')."-".$bulan."-01";
        $awal_bulan = $this->tgl_indo($a_date);
        $akhir_bulan = $this->tgl_indo(date("Y-m-t", strtotime($a_date)));
        $data_ckp = $this->ckp_model->get_ckp($nip_pegawai, $bulan,1);
        $baris_data_ckp = count($data_ckp);
        $data_ckp_tambahan = $this->ckp_model->get_ckp($nip_pegawai, $bulan,0);
        $baris_data_ckp_tambahan = count($data_ckp_tambahan);

        $pegawai = $this->pegawai_model->get_pegawai_nip($nip_pegawai);

        if (empty($data_ckp[0]['id_skp'])) {
            $data_ckp = null;
        } 
        if (empty($data_ckp_tambahan[0]['id_skp'])) {
            $data_ckp_tambahan = null;
        } 

        $spreadsheet = new Spreadsheet();
        
        $spreadsheet->getDefaultStyle()
            ->getFont()
            ->setName('Segoe UI')
            ->setSize(10);


        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('K1', 'CKP-R');
        $sheet->setCellValue('A2', 'CAPAIAN KINERJA PEGAWAI TAHUN '.$this->session->userdata('tahun_anggaran'));
        $sheet->mergeCells('A2:K2');

        $sheet->setCellValue('A4', 'Satuan Organisasi');
        $sheet->setCellValue('C4', ': '.$pegawai['nama_unit_kerja'].' '.$pegawai['nama_satker']);

        $sheet->setCellValue('A5', 'Nama');
        $sheet->setCellValue('C5', ': '.$pegawai['nama_pegawai']);

        $sheet->setCellValue('A6', 'Jabatan');
        $sheet->setCellValue('C6', ': '.$pegawai['nama_jabatan_fungsional']);

        $sheet->setCellValue('A7', 'Periode');
        $sheet->setCellValue('C7', ': '.$awal_bulan.' - '.$akhir_bulan);

        $sheet->setCellValue('A9', 'No');
        $sheet->mergeCells('A9:A10');

        $sheet->setCellValue('B9', 'Uraian Kegiatan');
        $sheet->mergeCells('B9:C10');
        
        $sheet->setCellValue('D9', 'Satuan');
        $sheet->mergeCells('D9:D10');

        $sheet->setCellValue('E9', 'Kuantitas');
        $sheet->mergeCells('E9:G9');

        $sheet->setCellValue('E10', 'Target');
        $sheet->setCellValue('F10', 'Realisasi');
        $sheet->setCellValue('G10', '%');

        $sheet->setCellValue('H9', 'Tingkat Kualitas (%)');
        $sheet->mergeCells('H9:H10');

        $sheet->setCellValue('I9', 'Kode Butir Kegiatan');
        $sheet->mergeCells('I9:I10');

        $sheet->setCellValue('J9', 'Angka Kredit');
        $sheet->mergeCells('J9:J10');

        $sheet->setCellValue('K9', 'Keterangan');
        $sheet->mergeCells('K9:K10');

        $sheet->setCellValue('A11', '(1)');
        $sheet->setCellValue('B11', '(2)');
        $sheet->mergeCells('B11:C11');
        $sheet->setCellValue('D11', '(3)');
        $sheet->setCellValue('E11', '(4)');
        $sheet->setCellValue('F11', '(5)');
        $sheet->setCellValue('G11', '(6)');
        $sheet->setCellValue('H11', '(7)');
        $sheet->setCellValue('I11', '(8)');
        $sheet->setCellValue('J11', '(9)');
        $sheet->setCellValue('K11', '(10)');

        $sheet->setCellValue('A12', 'UTAMA');
        $sheet->getStyle('A12')->getFont()->setBold(true);
        $sheet->mergeCells('A12:C12');
        

        foreach ($data_ckp as $key => $value) {
            $sheet->setCellValue('A'.($key+13), ($key+1));
            $sheet->setCellValue('B'.($key+13), $value['nama_pekerjaan_non_pok'].$value['rincian_pekerjaan'].' '.$value['nama_kegiatan']);
            $sheet->mergeCells('B'.($key+13).':C'.($key+13));
            $sheet->setCellValue('D'.($key+13), $value['satuan']);
            $sheet->setCellValue('E'.($key+13), $value['target']);
            $sheet->setCellValue('F'.($key+13), $value['realisasi']);

            $target = (int)$value['target'] ; 
            $realisasi = (int)$value['realisasi'] ; 
            if ($realisasi == 0) {
               $persen = 0;
            } else {
                $persen = (double)$target / (double)$realisasi; 
            }
            
            $sheet->setCellValue('G'.($key+13), $persen);
            $sheet->setCellValue('H'.($key+13), $value['kualitas']);
            $sheet->setCellValue('I'.($key+13), $value['butir_fungsional']);
            $sheet->setCellValue('J'.($key+13), $value['nilai_ak']);
            $sheet->setCellValue('K'.($key+13), $value['keterangan']);
        }

        
        $cell_tambahan = $baris_data_ckp+12+1 ;
        
        $sheet->setCellValue('A'.($cell_tambahan), 'TAMBAHAN');
        $sheet->getStyle('A'.($cell_tambahan))->getFont()->setBold(true);
        $merge_tambahan = 'A'.$cell_tambahan.':'.'C'.$cell_tambahan;
        $sheet->mergeCells($merge_tambahan);

        foreach ($data_ckp_tambahan as $key => $value) {
            $sheet->setCellValue('A'.($key+$cell_tambahan+1), ($key+1));
            $sheet->setCellValue('B'.($key+$cell_tambahan+1), $value['nama_pekerjaan_non_pok'].$value['rincian_pekerjaan'].' '.$value['nama_kegiatan']);
            $sheet->mergeCells('B'.($key+$cell_tambahan+1).':C'.($key+$cell_tambahan+1));
            $sheet->setCellValue('D'.($key+$cell_tambahan+1), $value['satuan']);
            $sheet->setCellValue('E'.($key+$cell_tambahan+1), $value['target']);
            $sheet->setCellValue('F'.($key+$cell_tambahan+1), $value['realisasi']);

            $target = (int)$value['target'] ; 
            $realisasi = (int)$value['realisasi'] ; 
            if ($realisasi == 0) {
               $persen = 0;
            } else {
                $persen = (double)$target / (double)$realisasi; 
            }
            
            $sheet->setCellValue('G'.($key+$cell_tambahan+1), $persen);
            $sheet->setCellValue('H'.($key+$cell_tambahan+1), $value['kualitas']);
            $sheet->setCellValue('I'.($key+$cell_tambahan+1), $value['butir_fungsional']);
            $sheet->setCellValue('J'.($key+$cell_tambahan+1), $value['nilai_ak']);
            $sheet->setCellValue('K'.($key+$cell_tambahan+1), $value['keterangan']);
        }

        $cell_jumlah = $baris_data_ckp+12+1+$baris_data_ckp_tambahan+1 ;
        $sheet->setCellValue('A'.($cell_jumlah), 'JUMLAH');
        $merge_jumlah = 'A'.$cell_jumlah.':'.'H'.$cell_jumlah;
        $sheet->mergeCells($merge_jumlah);
        $sheet->setCellValue('J'.($cell_jumlah), '=SUM(J13:J'.($cell_jumlah-1).')');
        
        $sheet->setCellValue('A'.($cell_jumlah+1), 'RATA-RATA');
        $merge_jumlah = 'A'.($cell_jumlah+1).':'.'F'.($cell_jumlah+1);
        $sheet->mergeCells($merge_jumlah);

        $sheet->setCellValue('G'.($cell_jumlah+1), '=AVERAGE(G13:G'.($cell_jumlah-1).')');
        $sheet->setCellValue('H'.($cell_jumlah+1), '=AVERAGE(H13:H'.($cell_jumlah-1).')');

        $sheet->setCellValue('A'.($cell_jumlah+2), 'CAPAIAN KINERJA PEGAWAI (CKP)');
        $merge_jumlah = 'A'.($cell_jumlah+2).':'.'F'.($cell_jumlah+2);
        $sheet->mergeCells($merge_jumlah);

        $sheet->setCellValue('G'.($cell_jumlah+2), '=AVERAGE(G'.($cell_jumlah+1).':H'.($cell_jumlah+1).')');
        $merge_jumlah = 'G'.($cell_jumlah+2).':'.'H'.($cell_jumlah+2);
        $sheet->mergeCells($merge_jumlah);

        $sheet->setCellValue('B'.($cell_jumlah+4), 'Penilaian Kinerja');
        
        $sheet->setCellValue('B'.($cell_jumlah+5), "Tanggal: ".$akhir_bulan);

        $sheet->setCellValue('C'.($cell_jumlah+7), "Pegawai Yang Dinilai");
        $sheet->setCellValue('G'.($cell_jumlah+7), "Pejabat Penilai");
        
        $atasan = $this->pegawai_model->get_atasan($pegawai['kode_unit_kerja'], $pegawai['kode_jabatan_struktur']);

        if ($pegawai['kode_jabatan_struktur'] == 0 ) {
            $sheet->setCellValue('C'.($cell_jumlah+10), $pegawai['nama_pegawai']);
            $sheet->setCellValue('G'.($cell_jumlah+10), "silahkan update profil anda");
            $sheet->setCellValue('C'.($cell_jumlah+11), $pegawai['nip_pegawai']);
            $sheet->setCellValue('G'.($cell_jumlah+11), "silahkan update profil anda");
        } else {
            $sheet->setCellValue('C'.($cell_jumlah+10), $pegawai['nama_pegawai']);
            $sheet->setCellValue('G'.($cell_jumlah+10), $atasan['nama_pegawai']);
            $sheet->setCellValueExplicit('C'.($cell_jumlah+11), $pegawai['nip_pegawai'],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValueExplicit('G'.($cell_jumlah+11), $atasan['nip_pegawai'],\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        }
        

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
        ];
        $style_ckp_atas = [
            'font' => [
                'bold'  =>  true,
                'size'  =>  16,
                'name'  =>  'Arrus Blk BT'
            ],
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                // 'wrapText' => true
            ],
        ];
        $style_bold = [
            'font' => [
                'bold'  =>  true,
                // 'size'  =>  14,
                // 'name'  =>  'Arial'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                'wrapText' => true
            ],
            
        ];
        $style_center = [
            
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER,
                'vertical' => Alignment::VERTICAL_CENTER,
                // 'wrapText' => true
            ],
            
        ];

        $sheet->getStyle('K1')->applyFromArray($style_ckp_atas);
        $sheet->getStyle('A2:K2')->applyFromArray($style_bold);
        $sheet->getStyle('A9:K10')->applyFromArray($style_bold);
        $sheet->getStyle('A11:K11')->applyFromArray($style_center);
        $sheet->getStyle('A'.($cell_jumlah).':K'.($cell_jumlah+2))->applyFromArray($style_bold);
        $sheet->getStyle('A9:K'.($cell_jumlah+2))->applyFromArray($styleArray);

        $sheet->getStyle('C'.($cell_jumlah+7))->applyFromArray($style_center);
        $sheet->getStyle('G'.($cell_jumlah+7))->applyFromArray($style_center);
        $sheet->getStyle('C'.($cell_jumlah+10))->applyFromArray($style_center);
        $sheet->getStyle('G'.($cell_jumlah+10))->applyFromArray($style_center);
        $sheet->getStyle('C'.($cell_jumlah+11))->applyFromArray($style_center);
        $sheet->getStyle('G'.($cell_jumlah+11))->applyFromArray($style_center);
        $writer = new Xlsx($spreadsheet);

        $filename = "CKP";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    function tgl_indo($tanggal){
        $bulan = array (
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}