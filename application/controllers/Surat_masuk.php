<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Surat_masuk extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
       
        $this->load->model('Surat_masuk_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        // $this->load->vars($data);
		$this->template->load('template/template', 'surat_masuk/sipadu_surat_masuk_list');
        // $this->template->load('template','surat_masuk/sipadu_surat_masuk_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Surat_masuk_model->json();
    }

    public function read($id) 
    {
        $row = $this->Surat_masuk_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_surat_masuk' => $row->id_surat_masuk,
		'kategori' => $row->kategori,
		'unit_kerja_tujuan' => $row->unit_kerja_tujuan,
		'perihal' => $row->perihal,
		'nip' => $row->nip,
		'timestamp' => $row->timestamp,
		'file_surat' => $row->file_surat,
		'tanggal_surat' => $row->tanggal_surat,
		'no_surat' => $row->no_surat,
		'tanggal_upload' => $row->tanggal_upload,
		'instansi' => $row->instansi,
	    );
            $this->template->load('template','surat_masuk/sipadu_surat_masuk_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('surat_masuk'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('surat_masuk/create_action'),
	    'id_surat_masuk' => set_value('id_surat_masuk'),
	    'kategori' => set_value('kategori'),
	    'unit_kerja_tujuan' => set_value('unit_kerja_tujuan'),
	    'perihal' => set_value('perihal'),
	    'nip' => set_value('nip'),
	    'timestamp' => set_value('timestamp'),
	    'file_surat' => set_value('file_surat'),
	    'tanggal_surat' => set_value('tanggal_surat'),
	    'no_surat' => set_value('no_surat'),
	    'tanggal_upload' => set_value('tanggal_upload'),
	    'instansi' => set_value('instansi'),
	);
        $this->template->load('template','surat_masuk/sipadu_surat_masuk_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kategori' => $this->input->post('kategori',TRUE),
		'unit_kerja_tujuan' => $this->input->post('unit_kerja_tujuan',TRUE),
		'perihal' => $this->input->post('perihal',TRUE),
		'nip' => $this->input->post('nip',TRUE),
		'timestamp' => $this->input->post('timestamp',TRUE),
		'file_surat' => $this->input->post('file_surat',TRUE),
		'tanggal_surat' => $this->input->post('tanggal_surat',TRUE),
		'no_surat' => $this->input->post('no_surat',TRUE),
		'tanggal_upload' => $this->input->post('tanggal_upload',TRUE),
		'instansi' => $this->input->post('instansi',TRUE),
	    );

            $this->Surat_masuk_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('surat_masuk'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Surat_masuk_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('surat_masuk/update_action'),
		'id_surat_masuk' => set_value('id_surat_masuk', $row->id_surat_masuk),
		'kategori' => set_value('kategori', $row->kategori),
		'unit_kerja_tujuan' => set_value('unit_kerja_tujuan', $row->unit_kerja_tujuan),
		'perihal' => set_value('perihal', $row->perihal),
		'nip' => set_value('nip', $row->nip),
		'timestamp' => set_value('timestamp', $row->timestamp),
		'file_surat' => set_value('file_surat', $row->file_surat),
		'tanggal_surat' => set_value('tanggal_surat', $row->tanggal_surat),
		'no_surat' => set_value('no_surat', $row->no_surat),
		'tanggal_upload' => set_value('tanggal_upload', $row->tanggal_upload),
		'instansi' => set_value('instansi', $row->instansi),
	    );
            $this->template->load('template','surat_masuk/sipadu_surat_masuk_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('surat_masuk'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_surat_masuk', TRUE));
        } else {
            $data = array(
		'kategori' => $this->input->post('kategori',TRUE),
		'unit_kerja_tujuan' => $this->input->post('unit_kerja_tujuan',TRUE),
		'perihal' => $this->input->post('perihal',TRUE),
		'nip' => $this->input->post('nip',TRUE),
		'timestamp' => $this->input->post('timestamp',TRUE),
		'file_surat' => $this->input->post('file_surat',TRUE),
		'tanggal_surat' => $this->input->post('tanggal_surat',TRUE),
		'no_surat' => $this->input->post('no_surat',TRUE),
		'tanggal_upload' => $this->input->post('tanggal_upload',TRUE),
		'instansi' => $this->input->post('instansi',TRUE),
	    );

            $this->Surat_masuk_model->update($this->input->post('id_surat_masuk', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('surat_masuk'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Surat_masuk_model->get_by_id($id);

        if ($row) {
            $this->Surat_masuk_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('surat_masuk'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('surat_masuk'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kategori', 'kategori', 'trim|required');
	$this->form_validation->set_rules('unit_kerja_tujuan', 'unit kerja tujuan', 'trim|required');
	$this->form_validation->set_rules('perihal', 'perihal', 'trim|required');
	$this->form_validation->set_rules('nip', 'nip', 'trim|required');
	$this->form_validation->set_rules('timestamp', 'timestamp', 'trim|required');
	$this->form_validation->set_rules('file_surat', 'file surat', 'trim|required');
	$this->form_validation->set_rules('tanggal_surat', 'tanggal surat', 'trim|required');
	$this->form_validation->set_rules('no_surat', 'no surat', 'trim|required');
	$this->form_validation->set_rules('tanggal_upload', 'tanggal upload', 'trim|required');
	$this->form_validation->set_rules('instansi', 'instansi', 'trim|required');

	$this->form_validation->set_rules('id_surat_masuk', 'id_surat_masuk', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Surat_masuk.php */
/* Location: ./application/controllers/Surat_masuk.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2021-05-04 10:22:22 */
/* http://harviacode.com */