<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pok extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }
        $this->load->model('master_pok_model');
        $this->load->model('master_bidang_model');
    }
    public function index()
    {
        $pok = $this->master_pok_model->get_all();
        $bidang = $this->master_bidang_model->get_all();
        $data['pok'] = $pok;
        $data['bidang'] = $bidang;
        $this->load->vars($data);
        $this->template->load('template/template', 'pok/list');
    }

    public function ambil_pok_bidang()
    {
        $kode_bidang = $this->input->get('kode_bidang');
        $pok = $this->master_pok_model->get_pok_bidang($kode_bidang);
        echo '<option value="">Pilih POK</option>';
        foreach ($pok as $key => $value) {
            echo '<option value="' . $value['id_pok'] . '">' . $value['rincian_pok'] . '</option>';
        }
    }
}