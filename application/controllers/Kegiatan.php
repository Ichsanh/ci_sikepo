<?php
defined('BASEPATH') or exit('No direct script access allowed');
    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Kegiatan extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }
        $this->load->model('kegiatan_model');
        $this->load->model('master_bidang_model');
        $this->load->model('master_pok_model');
        $this->load->model('master_pekerjaan_model');
        $this->load->model('waktu_deskripsi_model');
        $this->load->model('matriks_kinerja_model');
    }
    public function index()
    {
        // $kegiatan = $this->kegiatan_model->get_kegiatan();
        $bidang = $this->master_bidang_model->get_all();
        $data['bidang'] = $bidang;
        $data['bidang_terpilih'] = null;
        $data['kegiatan'] = null;
        $this->load->vars($data);
        $this->template->load('template/template', 'kegiatan/list');
    }

    public function lihat_kegiatan()
    {
        $kode_bidang = $this->input->get('kode_bidang');
        $id_pok = $this->input->get('id_pok');
        $kegiatan = $this->kegiatan_model->get_kegiatan_pok($id_pok);
        // print_r($kegiatan);
        // exit;
        $bidang_terpilih = $this->master_bidang_model->get_id($kode_bidang);
        $bidang = $this->master_bidang_model->get_all();
        $data['bidang'] = $bidang;
        $data['bidang_terpilih'] = $bidang_terpilih;
        $data['pok_terpilih'] = $this->master_pok_model->get_id($id_pok);
        $data['pok'] = $this->master_pok_model->get_pok_bidang($kode_bidang);
        $data['kegiatan'] = $kegiatan;
        $this->load->vars($data);
        $this->template->load('template/template', 'kegiatan/list');
    }


    public function tambah()
    {
        $params = array(
            'nama_kegiatan' => $this->input->post('nama_kegiatan'),
            'id_pok' => $this->input->post('id_pok'),
            'flag_jenis_kegiatan' => 1,

        );
        $this->kegiatan_model->tambah($params);
        $this->session->set_flashdata('sukses', "Data berhasil diupdate");
        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }
    public function hapus($id_kegiatan)
    {
        $this->kegiatan_model->hapus($id_kegiatan);
        $this->session->set_flashdata('sukses', "Data berhasil dihapus");
        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }
    public function edit()
    {
        $params = array(
            'nama_kegiatan' => $this->input->post('nama_kegiatan'),
            'id_pok' => $this->input->post('id_pok'),

        );
        // print_r($params);
        // print_r($this->input->post('id_kegiatan'));

        $this->kegiatan_model->update($this->input->post('id_kegiatan'), $params);
        // echo $this->db->error();

        $this->session->set_flashdata('sukses', "Data berhasil diupdate");
        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }

    public function tambah_pekerjaan($id_kegiatan)
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_master_pekerjaan', 'nama id_master_pekerjaan', 'required');


        if ($this->form_validation->run()) {

            $params = array(
                'tahun' => $this->session->userdata('tahun_anggaran'),
                'id_master_pekerjaan' => $this->input->post("id_master_pekerjaan"),
                'id_pok' => $this->input->post("id_pok"),
                'target_bulan_01' => $this->input->post("target_bulan_01"),
                'target_bulan_02' => $this->input->post("target_bulan_02"),
                'target_bulan_03' => $this->input->post("target_bulan_03"),
                'target_bulan_04' => $this->input->post("target_bulan_04"),
                'target_bulan_05' => $this->input->post("target_bulan_05"),
                'target_bulan_06' => $this->input->post("target_bulan_06"),
                'target_bulan_07' => $this->input->post("target_bulan_07"),
                'target_bulan_08' => $this->input->post("target_bulan_08"),
                'target_bulan_09' => $this->input->post("target_bulan_09"),
                'target_bulan_10' => $this->input->post("target_bulan_10"),
                'target_bulan_11' => $this->input->post("target_bulan_11"),
                'target_bulan_12' => $this->input->post("target_bulan_12"),
                'satuan' => $this->input->post("satuan"),
                'waktu' => $this->input->post("waktu"),
                'id_waktu_deskripsi' => $this->input->post("id_waktu_deskripsi"),
                'flag_jenis_matriks' => 1,
                'id_kegiatan' => $id_kegiatan,

            );

            $insert = $this->matriks_kinerja_model->tambah($params);
            $this->session->set_flashdata('sukses', "Data berhasil ditambahkan");

            redirect('kegiatan');
        } else {
            $data['kegiatan'] = $this->kegiatan_model->get_kegiatan_id($id_kegiatan);
            $data['jenis_pekerjaan'] = $this->master_pekerjaan_model->get_all_jenis_pekerjaan();
            $data['pekerjaan'] = $this->master_pekerjaan_model->get_all();
            $data['waktu_deskripsi'] = $this->waktu_deskripsi_model->get_all();

            $this->load->vars($data);
            $this->template->load('template/template', 'kegiatan/tambah_pekerjaan');
        }
    }
    public function edit_pekerjaan($id_matriks_kinerja)
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_master_pekerjaan', 'nama id_master_pekerjaan', 'required');


        if ($this->form_validation->run()) {

            $params = array(
                'tahun' => $this->session->userdata('tahun_anggaran'),
                'id_master_pekerjaan' => $this->input->post("id_master_pekerjaan"),
                'id_pok' => $this->input->post("id_pok"),
                'target_bulan_01' => $this->input->post("target_bulan_01"),
                'target_bulan_02' => $this->input->post("target_bulan_02"),
                'target_bulan_03' => $this->input->post("target_bulan_03"),
                'target_bulan_04' => $this->input->post("target_bulan_04"),
                'target_bulan_05' => $this->input->post("target_bulan_05"),
                'target_bulan_06' => $this->input->post("target_bulan_06"),
                'target_bulan_07' => $this->input->post("target_bulan_07"),
                'target_bulan_08' => $this->input->post("target_bulan_08"),
                'target_bulan_09' => $this->input->post("target_bulan_09"),
                'target_bulan_10' => $this->input->post("target_bulan_10"),
                'target_bulan_11' => $this->input->post("target_bulan_11"),
                'target_bulan_12' => $this->input->post("target_bulan_12"),
                'satuan' => $this->input->post("satuan"),
                'waktu' => $this->input->post("waktu"),
                'id_waktu_deskripsi' => $this->input->post("id_waktu_deskripsi"),
                'flag_jenis_matriks' => 1,
                'id_kegiatan' => $this->input->post('id_kegiatan'),

            );
            $url = $this->input->post('url');

            $insert = $this->matriks_kinerja_model->edit($id_matriks_kinerja, $params);
            $this->session->set_flashdata('sukses', "Data berhasil diupdate");

            redirect($url);
        } else {
            $data['data'] = $this->matriks_kinerja_model->get_id($id_matriks_kinerja);
            $data['kegiatan'] = $this->kegiatan_model->get_kegiatan_id($data['data']['id_kegiatan']);
            $data['pekerjaan'] = $this->master_pekerjaan_model->get_all();
            $data['pekerjaan_terpilih'] = $this->master_pekerjaan_model->get_id($data['data']['id_master_pekerjaan']);
            $data['jenis_pekerjaan'] = $this->master_pekerjaan_model->get_all_jenis_pekerjaan();
            $data['jenis_pekerjaan_terpilih'] = $this->master_pekerjaan_model->get_id_jenis_pekerjaan($data['pekerjaan_terpilih']['id_jenis_pekerjaan']);

            $data['waktu_deskripsi'] = $this->waktu_deskripsi_model->get_all();
            $data['waktu_deskripsi_terpilih'] = $this->waktu_deskripsi_model->get_id($data['data']['id_waktu_deskripsi']);

            $this->load->library('user_agent');
            $data['url'] = $this->agent->referrer();
            $this->load->vars($data);
            $this->template->load('template/template', 'kegiatan/edit_pekerjaan');
        }
    }

    public function hapus_pekerjaan($id_matriks_kinerja)
    {
        $this->matriks_kinerja_model->hapus($id_matriks_kinerja);
        $this->session->set_flashdata('sukses', "Data berhasil dihapus");
        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }

    public function download_template()
    {
        $this->load->helper('download');
        force_download('kumpulan_template/template_master_kegiatan.xlsx',NULL);
    }

    public function download_master()
    {
        $data = $this->kegiatan_model->get_all();
        $baris = count($data);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'id_kegiatan');
        $sheet->setCellValue('B1', 'id_pok');
        $sheet->setCellValue('C1', 'kode');
        $sheet->setCellValue('D1', 'nama_kegiatan');
        $sheet->setCellValue('E1', 'jenis_kegiatan');
        
        foreach ($data as $key => $value) {
            $sheet->setCellValue('A'.($key+2), $value['id_kegiatan']);
            $sheet->setCellValue('B'.($key+2), $value['id_pok']);
            $sheet->setCellValue('C'.($key+2), $value['kode']);
            $sheet->setCellValue('D'.($key+2), $value['nama_kegiatan']);
            $sheet->setCellValue('E'.($key+2), $value['jenis_kegiatan']);
        }
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:E'.($baris+1))->applyFromArray($styleArray);
        $writer = new Xlsx($spreadsheet);

        $filename = "master kegiatan";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
    
    public function Upload()
    {

        $this->load->library('upload');
        $new_name = 'kegiatan_'.$this->session->userdata('nip_pegawai').'_'.$_FILES["template"]['name'] ;
        $config['file_name'] = $new_name;
        $config['upload_path'] = './upload_file/'; //path folder
        $config['allowed_types'] = 'xlsx'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '50000'; //maksimum besar file 2M

        $this->upload->initialize($config);

        // $files = $_FILES['template'];
        // $this->upload->do_upload('template');

        if ($this->upload->do_upload('template')) {
                $gbr[] = $this->upload->data();
                $nama_file= $gbr['0']['file_name'];
                // print_r($gbr);
            } else {
                echo 'gagal upload file';
                echo  $this->upload->display_errors('<p>', '</p>');
                //return false;
            }

        $inputFileName = "upload_file/".$nama_file;

        /**  Identify the type of $inputFileName  **/
        // $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

        // /**  Create a new Reader of the type that has been identified  **/
        // $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        // /**  Load $inputFileName to a Spreadsheet Object  **/
        // $spreadsheet = $reader->load($inputFileName);

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $data = $spreadsheet->getActiveSheet()->toArray();
        // print_r($data);
        unset($data[0]);
        foreach ($data as $key => $value) {
            if (is_null($value['0']) ) {
                break;
            }
            $params = array(
                'id_pok' => $value['0'],
                'pok' => $value['1'],
                'nama_kegiatan' => $value['2'],
                'jenis_kegiatan' => $value['3'],
                'flag_jenis_kegiatan' => 1,
            );

            try {
                $this->kegiatan_model->tambah_dummy($params);
                
                
            } catch (Exception $error) {
                echo 'ERROR:'.$error->getMessage();
            }
            
        }
        
        $data_duplikat = $this->kegiatan_model->cek_duplikat();
        $this->db->query('TRUNCATE TABLE dummy_tabel_master_kegiatan');
        if (empty($data_duplikat)) {
            $this->session->set_flashdata('sukses', "Data berhasil ditambahkan");
            $this->load->library('user_agent');
            redirect($this->agent->referrer());
        } else {
            $this->download_error_upload($data_duplikat);
        }
        

    }

    public function download_error_upload($data_duplikat)
    {
        $spreadsheet2 = new Spreadsheet();
        
        // $spreadsheet->getDefaultStyle()
        //     ->getFont()
        //     ->setName('Segoe UI')
        //     ->setSize(11);


        $sheet = $spreadsheet2->getActiveSheet();
        $sheet->setCellValue('A1', 'id_pok');
        $sheet->setCellValue('B1', 'POK');
        $sheet->setCellValue('C1', 'nama_kegiatan');
        $sheet->setCellValue('D1', 'jenis_kegiatan');
        $sheet->setCellValue('E1', 'status');
        
        foreach ($data_duplikat as $key => $value) {
            $sheet->setCellValue('A'.($key+2), $value['id_pok']);
            $sheet->setCellValue('B'.($key+2), $value['pok']);
            $sheet->setCellValue('C'.($key+2), $value['nama_kegiatan']);
            $sheet->setCellValue('D'.($key+2), $value['jenis_kegiatan']);
            $sheet->setCellValue('E'.($key+2), 'Duplikat');
            
        }

        $styleGaris = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
        ];
        $sheet->getStyle('A1:E'.(count($data_duplikat)+1))->applyFromArray($styleGaris);

        $writer = new Xlsx($spreadsheet2);
        
        $filename = "error upload master kegiatan";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}