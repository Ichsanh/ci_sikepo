<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }
        $this->load->model('master_pok_model');
        $this->load->model('matriks_kinerja_model');
    }
    public function matriks_kinerja_1()
    {
        $data = $this->matriks_kinerja_model->get_matriks_kinerja_all();

        $data['data'] = $data;

        $this->load->vars($data);
        $this->template->load('template/template', 'report/report_matriks_kinerja_1');
    }

    public function matriks_kinerja_2()
    {
        $data = $this->matriks_kinerja_model->get_matriks_kinerja_2();

        $data['data'] = $data;

        $this->load->vars($data);
        $this->template->load('template/template', 'report/report_matriks_kinerja_2');
    }
    public function matriks_kinerja_3()
    {
        $data = $this->matriks_kinerja_model->get_matriks_kinerja_3();

        $data['data'] = $data;

        $this->load->vars($data);
        $this->template->load('template/template', 'report/report_matriks_kinerja_3');
    }
    public function matriks_kinerja_4()
    {
        $data = $this->matriks_kinerja_model->get_matriks_kinerja_3();

        $data['data'] = $data;

        $this->load->vars($data);
        $this->template->load('template/template', 'report/report_matriks_kinerja_4');
    }
}
