<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }
        $this->load->model('pegawai_model');
        $this->load->model('master_pangkat_model');
        $this->load->model('master_satker_model');
        $this->load->model('master_unit_kerja_model');
        $this->load->model('master_jabatan_struktur_model');
        $this->load->model('master_jabatan_fungsional_model');
    }
    public function index()
    {
        $pegawai = $this->pegawai_model->get_pegawai();
        $data['pegawai'] = $pegawai;
        $this->load->vars($data);
        $this->template->load('template/template', 'pegawai/list');
    }
    public function edit($id_pegawai)
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_pegawai', 'nama pegawai', 'required');


        if ($this->form_validation->run()) {

            $params = array(
                'nama_pegawai' => $this->input->post('nama_pegawai'),
                'nip_pegawai' => $this->input->post('nip_pegawai'),
                'kode_satker' => $this->input->post('kode_satker'),
                'pangkat' => $this->input->post('pangkat'),
                'kode_jabatan_struktur' => $this->input->post('kode_jabatan_struktur'),
                'kode_jabatan_fungsional' => $this->input->post('kode_jabatan_fungsional'),
                'kode_unit_kerja' => $this->input->post('kode_unit_kerja'),
                'status_pegawai' => $this->input->post('status_pegawai'),
            );

            $update = $this->pegawai_model->update($id_pegawai, $params);
            $this->session->set_flashdata('sukses', "Data berhasil diupdate");

            redirect('pegawai');
        } else {
            $data['data'] = $this->pegawai_model->get_pegawai_id($id_pegawai);
            $data['master_pangkat'] = $this->master_pangkat_model->get_all();
            $data['master_satker'] = $this->master_satker_model->get_all();
            $data['master_unit_kerja'] = $this->master_unit_kerja_model->get_all();
            $data['master_jabatan_struktur'] = $this->master_jabatan_struktur_model->get_all();
            $data['master_jabatan_fungsional'] = $this->master_jabatan_fungsional_model->get_all();

            $this->load->vars($data);
            $this->template->load('template/template', 'pegawai/edit');
        }
    }
}
