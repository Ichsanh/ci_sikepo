<?php
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
    header('Access-Control-Allow-Headers: token, Content-Type');
    header('Access-Control-Max-Age: 1728000');
    header('Content-Length: 0');
    header('Content-Type: text/plain');
    die();
}

header('Access-Control-Allow-Origin: * ');
header('Content-Type: application/json');

require APPPATH . 'libraries/REST_Controller.php';

class Api extends REST_Controller
{

    // construct
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pegawai_model');
        $this->load->model('gaji_model');
        $this->load->model('survei_model');
    }

    // method index untuk menampilkan semua data student menggunakan method get
    public function index_get()
    {
        // $response = $this->pegawai_model->all_student();
        // $this->response($response);
    }
    // method index untuk menampilkan semua data student menggunakan method get
    public function student_get($id)
    {
        $response = $this->pegawai_model->student_id($id);
        $this->response($response);
    }

    // untuk menambah student menaggunakan method post
    public function login_post()
    {
        $response = $this->pegawai_model->login_pegawai(
            $this->post('username'),
            $this->post('password')
        );
        //$response = $this->post('username');
        $this->response($response);
    }
    public function surveiInsert_post()
    {

        $response = $this->survei_model->insert_survei(
            $this->post('nilai')
        );

        //$response = $this->post('username');
        $this->response($response);
    }

    public function feed_post()
    {

        $response = $this->gaji_model->get_gajiTukin_terakhir(
            $this->post('nip_pegawai_lama')
        );


        //$response = $this->post('username');
        $this->response($response);
    }
    public function gaji_post()
    {

        if (empty($this->post('tahun')) & empty($this->post('bulan'))) {
            $response = $this->gaji_model->get_gaji_terakhir(
                $this->post('nip_pegawai_lama')
            );
        } else {
            $tahun =  $this->post('tahun');
            $bulan =  $this->post('bulan');
            $response = $this->gaji_model->get_gaji(
                $this->post('nip_pegawai_lama'),
                $tahun,
                $bulan
            );
        }

        //$response = $this->post('username');
        $this->response($response);
    }

    public function tukin_post()
    {
        if (empty($this->post('tahun')) & empty($this->post('bulan'))) {
            $response = $this->gaji_model->get_tukin_terakhir(
                $this->post('nip_pegawai_lama')
            );
        } else {
            $tahun =  $this->post('tahun');
            $bulan =  $this->post('bulan');
            $response = $this->gaji_model->get_tukin(
                $this->post('nip_pegawai_lama'),
                $tahun,
                $bulan
            );
        }
        //$response = $this->post('username');
        $this->response($response);
    }
    // untuk menambah student menaggunakan method post
    public function add_post()
    {
        $response = $this->pegawai_model->add_student(
            $this->post('name'),
            $this->post('address'),
            $this->post('age')
        );
        $this->response($response);
    }

    // update data student menggunakan method put
    public function update_put()
    {
        $response = $this->pegawai_model->update_student(
            $this->put('id'),
            $this->put('name'),
            $this->put('address'),
            $this->put('age')
        );
        $this->response($response);
    }

    public function password_put()
    {
        $response = $this->pegawai_model->update_password(
            $this->put('nip_pegawai_lama'),
            $this->put('password')
        );
        $this->response($response);
    }

    // hapus data student menggunakan method delete
    public function delete_delete($id)
    {
        // $response = $this->pegawai_model->delete_student(
        //     $this->delete('id')
        // );
        $response = $this->pegawai_model->delete_student($id);
        $this->response($response);
    }
}