<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Pekerjaan extends CI_Controller
{
// pekerjaan adalah uraian dari kegiatan

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }
        $this->load->model('master_pok_model');
        $this->load->model('master_bidang_model');
        $this->load->model('master_pekerjaan_model');
    }
    public function index()
    {
        $pok = $this->master_pok_model->get_all();
        $jenis_pekerjaan = $this->master_pekerjaan_model->get_all_jenis_pekerjaan();
        $data['pok'] = $pok;
        $data['jenis_pekerjaan'] = $jenis_pekerjaan;
        $this->load->vars($data);
        $this->template->load('template/template', 'pekerjaan/list');
    }
    public function ajax_list() //untuk ajax datatable server side
    {
        // $list = $this->master_pekerjaan_model->get_datatables($jenis_pekerjaan);
        $list = $this->master_pekerjaan_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $isi) {
            $no++;

            $row = array();
            $row[] = $no;
            $row[] = $isi->jenis_pekerjaan;
            $row[] = $isi->rincian_pekerjaan;
            $row[] = $isi->keterangan_pekerjaan;
            if ($isi->status_approval == 0) {
                $row[] = '<td><center><span class="badge badge-info">' . $isi->keterangan_approval . '</span> </td>';
            }
            if ($isi->status_approval == 1) {
                $row[] = '<td><center><span class="badge badge-success">' . $isi->keterangan_approval . '</span> </td>';
            }
            if ($isi->status_approval == 2) {
                $row[] = '<td><center><span class="badge badge-danger">' . $isi->keterangan_approval . '</span> </td>';
            }
            // $row[] = $isi->status_approval;
            $row[] = $isi->pegawai_created;
            $row[] = $isi->pegawai_approval;
            if ($this->session->userdata('level_user') <> 'user' and $this->session->userdata('level_user') <> 'pejabat') {
                $row[] = '<a onclick="return confirm_approve()" href="' . base_url('pekerjaan/approve/') . $isi->id_master_pekerjaan . '"> <span class="badge badge-success"><i class="fa fa-check-circle"></i> approve</span></a> <a  ></a>    
            <a onclick="return confirm_reject()"
            href="' . base_url('pekerjaan/reject/') . $isi->id_master_pekerjaan . '" > <span class="badge badge-danger"><i class="fa fa-close"></i> reject </span> </a>';
            }

            // $row[] = '<ahref="' . site_url('indikator/view/') . $isi->mfd . '/' . $isi->tipe . '/' . $isi->id_api . '/' . $isi->kode_kelompok . '">'. $isi->judul . '</ahref=>';

            // $row[] = '<a value="'.$data->id_data.'" data-toggle="modal" data-target="#modal_aksi"data-id_data="'.$data->id_data.'" data-flag="'.$data->flag.'" data-file="'.$data->file.'"><button class="btn btn-danger btn-sm">Aksi</button></a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->master_pekerjaan_model->count_all(),
            "recordsFiltered" => $this->master_pekerjaan_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function approve($id_master_pekerjaan)
    {
        $params = array(
            'status_approval' => 1,
            'approval_by' => $this->session->userdata('nip_pegawai')
        );
        $this->master_pekerjaan_model->update($id_master_pekerjaan, $params);
        redirect("pekerjaan");
    }
    public function reject($id_master_pekerjaan)
    {
        $params = array(
            'status_approval' => 2,
            'approval_by' => $this->session->userdata('nip_pegawai')
        );
        $this->master_pekerjaan_model->update($id_master_pekerjaan, $params);
        redirect("pekerjaan");
    }

    public function tambah()
    {
        $params = array(
            'rincian_pekerjaan' => $this->input->post('rincian_pekerjaan'),
            'keterangan_pekerjaan' => (!empty($this->input->post('keterangan_pekerjaan'))) ? $this->input->post('keterangan_pekerjaan') : null,
            'id_jenis_pekerjaan' => $this->input->post('id_jenis_pekerjaan'),
            'status_approval' => 0,
            'status_flag' => 1,
            'created_by' => $this->session->userdata('nip_pegawai')
        );
        $this->master_pekerjaan_model->tambah($params);
        redirect("pekerjaan");
    }

    public function ambil_pekerjaan_fungsional()
    {
        $id_jenis_pekerjaan = $this->input->get('id_jenis_pekerjaan');
        $pekerjaan = $this->master_pekerjaan_model->get_pekerjaan_fungsional($id_jenis_pekerjaan);
        echo '<option value="">Pilih Pekerjaan</option>';
        foreach ($pekerjaan as $key => $value) {
            echo '<option value="' . $value['id_master_pekerjaan'] . '">' . $value['rincian_pekerjaan'] . '</option>';
        }
    }

    public function download_template()
    {
        $this->load->helper('download');
        force_download('kumpulan_template/template_master_uraian_kegiatan.xlsx',NULL);
    }

    public function download_master()
    {
        $data = $this->master_pekerjaan_model->get_all_master();
        $baris = count($data);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'id_master_pekerjaan');
        $sheet->setCellValue('B1', 'rincian_pekerjaan');
        $sheet->setCellValue('C1', 'keterangan_pekerjaan');
        $sheet->setCellValue('D1', 'id_jenis_pekerjaan');
        $sheet->setCellValue('E1', 'jenis_pekerjaan');
        
        foreach ($data as $key => $value) {
            $sheet->setCellValue('A'.($key+2), $value['id_master_pekerjaan']);
            $sheet->setCellValue('B'.($key+2), $value['rincian_pekerjaan']);
            $sheet->setCellValue('C'.($key+2), $value['keterangan_pekerjaan']);
            $sheet->setCellValue('D'.($key+2), $value['id_jenis_pekerjaan']);
            $sheet->setCellValue('E'.($key+2), $value['jenis_pekerjaan']);
        }
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
        ];

        $sheet->getStyle('A1:E'.($baris+1))->applyFromArray($styleArray);
        $writer = new Xlsx($spreadsheet);

        $filename = "master uraian kegiatan";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function Upload()
    {

        $this->load->library('upload');
        $new_name = 'pekerjaan_'.$this->session->userdata('nip_pegawai').'_'.$_FILES["template"]['name'] ;
        $config['file_name'] = $new_name;
        $config['upload_path'] = './upload_file/'; //path folder
        $config['allowed_types'] = 'xlsx'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '50000'; //maksimum besar file 2M

        $this->upload->initialize($config);

        // $files = $_FILES['template'];
        // $this->upload->do_upload('template');

        if ($this->upload->do_upload('template')) {
                $gbr[] = $this->upload->data();
                $nama_file= $gbr['0']['file_name'];
                // print_r($gbr);
            } else {
                echo 'gagal upload file';
                echo  $this->upload->display_errors('<p>', '</p>');
                //return false;
            }

        $inputFileName = "upload_file/".$nama_file;

        /**  Identify the type of $inputFileName  **/
        // $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

        // /**  Create a new Reader of the type that has been identified  **/
        // $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        // /**  Load $inputFileName to a Spreadsheet Object  **/
        // $spreadsheet = $reader->load($inputFileName);

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $data = $spreadsheet->getActiveSheet()->toArray();
        // print_r($data);
        unset($data[0]);
        foreach ($data as $key => $value) {
            if (is_null($value['0']) ) {
                break;
            }
            $params = array(
                
                'rincian_pekerjaan' => $value['0'],
                'keterangan_pekerjaan' => $value['1'],
                'id_jenis_pekerjaan' => $value['2'],
                'status_approval' => $value['3'],
                'created_by'=> $this->session->userdata('nip_pegawai'),
                
            );

            try {
                $this->master_pekerjaan_model->tambah_dummy($params);
                
                
            } catch (Exception $error) {
                echo 'ERROR:'.$error->getMessage();
            }
            
        }
        $data_duplikat = $this->master_pekerjaan_model->cek_duplikat();
        $this->db->query('TRUNCATE TABLE dummy_tabel_master_pekerjaan');
        if (empty($data_duplikat)) {
            $this->session->set_flashdata('sukses', "Data berhasil ditambahkan");
            $this->load->library('user_agent');
            redirect($this->agent->referrer());
        } else {
            $this->download_error_upload($data_duplikat);
        }
        
    }

    public function download_error_upload($data_duplikat)
    {
        
        $spreadsheet2 = new Spreadsheet();
        
        // $spreadsheet->getDefaultStyle()
        //     ->getFont()
        //     ->setName('Segoe UI')
        //     ->setSize(11);


        $sheet = $spreadsheet2->getActiveSheet();

        
        $sheet->setCellValue('A1', 'rincian_pekerjaan');
        $sheet->setCellValue('B1', 'keterangan_pekerjaan');
        $sheet->setCellValue('C1', 'id_jenis_pekerjaan');
        $sheet->setCellValue('D1', 'status_approval');
        $sheet->setCellValue('E1', 'status');
        
        foreach ($data_duplikat as $key => $value) {
            
            $sheet->setCellValue('A'.($key+2), $value['rincian_pekerjaan']);
            $sheet->setCellValue('B'.($key+2), $value['keterangan_pekerjaan']);
            $sheet->setCellValue('C'.($key+2), $value['id_jenis_pekerjaan']);
            $sheet->setCellValue('D'.($key+2), $value['status_approval']);
            $sheet->setCellValue('E'.($key+2), 'Duplikat');
            
        }

        $styleGaris = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
        ];
        $sheet->getStyle('A1:E'.(count($data_duplikat)+1))->applyFromArray($styleGaris);

        $writer = new Xlsx($spreadsheet2);
        
        $filename = "error upload master uraian kegiatan";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}