<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Excel extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello, World!');

        $writer = new Xlsx($spreadsheet);

        $filename = "helloworld";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }

    public function Read()
    {
        $inputFileName = "kumpulan_template/template_master_kegiatan.xlsx";

        /**  Identify the type of $inputFileName  **/
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

        /**  Create a new Reader of the type that has been identified  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($inputFileName);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $schdeules = $spreadsheet->getActiveSheet()->toArray();
        print_r($schdeules);
        // foreach ($schdeules as $single_schedule) {
        //     echo '<div class="row">';
        //     foreach ($single_schedule as $single_item) {
        //         echo '<p class="item">' . $single_item . '</p>';
        //     }
        //     echo '</div>';
        // }

        $this->load->library('upload');


        $config['upload_path'] = './upload_file/'; //path folder
        $config['allowed_types'] = 'rar|xls|xlsx|zip'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '50000'; //maksimum besar file 2M

        $this->upload->initialize($config);

        $judul_permintaan = $this->input->post('judul_permintaan');
        $deadline         = $this->input->post('deadline');
        $catatan          = $this->input->post('catatan'); //
        $id_sm            = $this->input->post('id_sm');
        $total            = count($judul_permintaan);


        $files = $_FILES['file'];
        // print_r($files);
        // echo '<br>';
        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name'] = $files['name'][$key];
            $_FILES['images[]']['type'] = $files['type'][$key];
            $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
            $_FILES['images[]']['error'] = $files['error'][$key];
            $_FILES['images[]']['size'] = $files['size'][$key];

            if ($this->upload->do_upload('images[]')) {
                $gbr[] = $this->upload->data();
                //$params['dummy']= $gbr['file_name'];

            } else {
                echo 'gagal upload file';
                //return false;
            }
        }
    }
}
