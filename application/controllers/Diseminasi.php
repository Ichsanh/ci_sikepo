<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diseminasi extends CI_Controller {
	private $filename = "import_data"; // Kita tentukan nama filenya
	function __construct(){
		parent::__construct();
		if($this->session->userdata('level') <> '1')
			{ if($this->session->userdata('level')    == '2'){
				redirect('sm');
			} 
			else if($this->session->userdata('level') == '3'){
				redirect('kl');
			} {
				redirect('login');
			}
		}
		
		$this->load->model('transaksi_model');
		$this->load->model('model_email');
		$this->load->model('model_download');
		$this->load->model('mou_model');
		$this->load->model('pks_model');
		$this->load->model('subject_model');
		$this->load->model('master_tabel_model');		
		$this->load->model('master_sm_model');
		$this->load->model('master_kl_model');
		$this->load->model('arc_model');
		$this->load->model('relasi_transaksi_model');
	}

	public function index()
	{

		$data['data_transaksi_dari_kl_di_sm'] =$this->transaksi_model->get_count_all_by_status_dis_flag1_jenis(1);
		$data['data_transaksi_dari_kl_di_sm_menunggu'] =$this->transaksi_model->get_count_all_by_status_dis_flag1_jenis_menunggu(1);
		$data['data_transaksi_ke_kl_di_sm'] =$this->transaksi_model->get_count_all_by_status_dis_flag2_jenis(1);
		$data['beban_sm'] = $this->transaksi_model->get_all_count_dl(1); // total permintaan ke BPS
		$data['beban_kl'] = $this->transaksi_model->get_all_count_dl(2); // total permintaan ke KL
		$data['get_persen_sm'] = $this->transaksi_model->get_persen_sm();
		$data['get_persen_kl'] = $this->transaksi_model->get_persen_kl();
		$data['halaman'] = 'dashboard';

		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/dashboard');

	}

	public function permintaan_kl()
	{
		
		$sm_1 =$this->transaksi_model->get_all_by_status_flag1_jenis(1);
		$sm_3 =$this->transaksi_model->get_all_by_status_flag1_jenis(3);
		$data['data_transaksi']  = array_merge($sm_1, $sm_3);
		
		$data['halaman'] = 'semua1';
		$data['halaman_atas'] = 'permintaan_kl';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/permintaan_kl');
	}

	public function permintaan_kl_menunggu()
	{
		
		$data['data_transaksi'] =$this->transaksi_model->get_all_menunggu_kirim();
		$data['halaman'] = 'menunggu1';
		$data['halaman_atas'] = 'permintaan_kl';

		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/permintaan_kl_menunggu');
	}

	public function permintaan_kl_selesai()
	{
		
		$data['data_transaksi'] =$this->transaksi_model->get_all_by_status_dis_flag1(5);
		$data['halaman'] = 'selesai1';
		$data['halaman_atas'] = 'permintaan_kl';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/permintaan_kl');
	}

	public function permintaan_kl_delegasi()
	{
		
		$data['data_transaksi'] =$this->transaksi_model->get_all_by_flag1_delegasi();
		$data['halaman'] = 'delegasi1';
		$data['halaman_atas'] = 'permintaan_kl';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/permintaan_kl_delegasi');
	}

	public function permintaan_kuesioner()
	{
		$data['data_transaksi'] =$this->transaksi_model->get_all_by_status_flag2_jenis(2);
		$data['data_transaksi'] =$this->transaksi_model->get_all_by_status_flag1_jenis(2);
		$data['halaman'] = 'permintaan_kuesioner';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/permintaan_kuesioner');
	}

	public function permintaan_sm()
	{
		//$data['data_transaksi'] =$this->transaksi_model->get_all_by_status_flag2_kl();
		$data['data_transaksi'] =$this->transaksi_model->get_all_by_flag2();
		$data['halaman'] = 'semua2';
		$data['halaman_atas'] = 'permintaan_sm';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/permintaan_sm');
	}

	public function permintaan_sm_menunggu()
	{
		//$data['data_transaksi'] =$this->transaksi_model->get_all_by_status_flag2_kl();
		$data['data_transaksi'] =$this->transaksi_model->get_all_by_status_dis_flag2(6);
		$data['halaman'] = 'menunggu2';
		$data['halaman_atas'] = 'permintaan_sm';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/permintaan_sm');
	}

	public function permintaan_sm_selesai()
	{
		//$data['data_transaksi'] =$this->transaksi_model->get_all_by_status_flag2_kl();
		$data['data_transaksi'] =$this->transaksi_model->get_all_by_status_dis_flag2(5);
		$data['halaman'] = 'selesai2';
		$data['halaman_atas'] = 'permintaan_sm';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/permintaan_sm');
	}

	public function delegasi_permintaan_kl($id_transaksi)  // mendelegasikan permintaan dari KL ke SM oleh diseminasi
	{
		$id_transaksi = $this->encrypt->decode($id_transaksi);
		$this->load->library('form_validation');
		$this->form_validation->set_rules('judul_permintaan[]','No Induk','max_length[40]');
		
		$this->form_validation->set_rules('deadline[]','penerbit','required');
		
		
		if($this->form_validation->run())     
		{   

			$this->load->library('upload');

			
            $config['upload_path'] = './upload_file/'; //path folder
            $config['allowed_types'] = 'rar|xls|xlsx|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '50000'; //maksimum besar file 2M

            $this->upload->initialize($config);

            $judul_permintaan = $this->input->post('judul_permintaan');
            $deadline         = $this->input->post('deadline');
			$catatan          = $this->input->post('catatan_ke_sm');//
			$id_sm            = $this->input->post('id_sm');
			$total            = count($judul_permintaan);
			

			$files = $_FILES['file']; 
            // print_r($files);
            // echo '<br>';
			foreach ($files['name'] as $key => $image) {
				$_FILES['images[]']['name']= $files['name'][$key];
				$_FILES['images[]']['type']= $files['type'][$key];
				$_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
				$_FILES['images[]']['error']= $files['error'][$key];
				$_FILES['images[]']['size']= $files['size'][$key];
				
				

				if ($this->upload->do_upload('images[]')) {
					$gbr[] = $this->upload->data();
	                //$params['dummy']= $gbr['file_name'];
					
				} else {
					echo 'gagal upload file';
	                //return false;
				}
			}
	        // foreach ($params as $key => $value) {
	        // 	$params[$key]['dummy']= $gbr[$key]['file_name'];
	        // 	//$data[] = array_merge($params[$key], $gbr[$key]);
	        // }
			$files = NULL;
			$_FILES['images[]'] = NULL;
			for ($i=0; $i <$total ; $i++) { 

				$params[] = array(
				//'id_transaksi'  => $this->input->post('id_transaksi'),
					'id_sm'            => $id_sm[$i],
					'judul_permintaan' => $judul_permintaan[$i],
					'deadline'         => $deadline[$i],
					'catatan_ke_sm'    => $catatan[$i],
					'deadline'         =>date('Y-m-d', strtotime($deadline[$i])),
					'status'           => '1',
					'tgl_delegasi'     => date('Y-m-d'),
					'dummy'            => $gbr[$i]['file_name'],
					'flag'           => '1', //permintaan dari K/L
					'jenis'           => '1', //permintaan PKS

				);

				

	            //$this->transaksi_model->add_transaksi($params);
			}

			$params_kl = array(
	            	'status' => '2', //telah didelegasikan = setuju
	            );
			

			$last_id = $this->transaksi_model->add_batch_transaksi($params);
			$this->transaksi_model->update_transaksi($this->input->post('id_transaksi'),$params_kl);
			$params = NULL;
			$gbr = NULL;

			$first_id = $last_id - ($total-1);
			for ($i=$first_id; $i <= $last_id ; $i++) { 

				$id_transaksi[] = array(
					'id_transaksi' => $i,
					'id_transaksi' => $this->input->post('id_transaksi'),
				);

			}
			$this->transaksi_model->add_batch_transaksi($id_transaksi);
			
			
            //echo 'sukses';
            //print_r($params);
			
			$this->response = $this->upload->display_errors();
            //print_r($this->upload->display_errors());
            //print_r(date('Y-m-d', strtotime($this->input->post('deadline'))));
			
			$this->session->set_flashdata('sukses', "Data yg anda masukan berhasil");

			redirect('diseminasi/index');
			
		}
		else
		{            
			$data['data'] =$this->transaksi_model->get_id($id_transaksi);
			$data['halaman'] = 'permintaan_kl';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/delegasi_permintaan_kl');
		}
		
	}

	public function tambah_kuesioner()  // menambah kuesioner
	{

		$this->load->library('form_validation');
		$this->form_validation->set_rules('judul_permintaan[]','No Induk','max_length[40]');
		// $this->form_validation->set_rules('judul_permintaan','judul','max_length[100]|required');
		// // $this->form_validation->set_rules('indikator[]','penulis','max_length[100]|required');
		$this->form_validation->set_rules('deadline[]','penerbit','required');
		
		
		if($this->form_validation->run())     
		{   

			$this->load->library('upload');

			
            $config['upload_path'] = './upload_file/'; //path folder
            $config['allowed_types'] = 'rar|xls|xlsx|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '50000'; //maksimum besar file 2M

            $this->upload->initialize($config);

            $judul_permintaan = $this->input->post('judul_permintaan');
            $deadline         = $this->input->post('deadline');
			$catatan          = $this->input->post('catatan');//
			$id_sm            = $this->input->post('id_sm');
			$total            = count($judul_permintaan);
			

			$files = $_FILES['file']; 
            // print_r($files);
            // echo '<br>';
			foreach ($files['name'] as $key => $image) {
				$_FILES['images[]']['name']= $files['name'][$key];
				$_FILES['images[]']['type']= $files['type'][$key];
				$_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
				$_FILES['images[]']['error']= $files['error'][$key];
				$_FILES['images[]']['size']= $files['size'][$key];

				if ($this->upload->do_upload('images[]')) {
					$gbr[] = $this->upload->data();
	                //$params['dummy']= $gbr['file_name'];
					
				} else {
					echo 'gagal upload file';
	                //return false;
				}
			}

			$files = NULL;
			$_FILES['images[]'] = NULL;
			for ($i=0; $i <$total ; $i++) { 

				$params[] = array(
				//'id_transaksi'  => $this->input->post('id_transaksi'),
					'id_kl'            => $this->input->post('id_kl'),
					'id_sm'            => $id_sm[$i],
					'judul_permintaan' => $judul_permintaan[$i],
					'deadline'         => $deadline[$i],
					'catatan_ke_sm'          => $catatan[$i],
					'deadline'         =>date('Y-m-d', strtotime($deadline[$i])),
					'status'           => '1',
					'status_dis'           => '1',
					'tgl_delegasi'     => date('Y-m-d'),
					'dummy'            => $gbr[$i]['file_name'],
					'flag'           => '1', //permintaan dari K/L
					'jenis'				=> '2' // kuesioner
				);

			}

			
			$params_kl = array(
				'id_kl'            => $this->input->post('id_kl'),
				//'id_kl'            => $this->input->post('id_kl'),
				'judul_permintaan' => $this->input->post('judul_permintaan_kl'),
				'deadline'         => date('Y-m-d', strtotime($this->input->post('deadline_kl'))),
				'catatan'            => $this->input->post('catatan_kl'),
				'status'            => '2',
				// 'tgl' 				=> date('Y-m-d'),
				'flag'            => '1', 
				'jenis'				=> '2' // kuesioner
				
			);
			if ($this->upload->do_upload('file_kl'))
			{
				$gbr_kl = $this->upload->data();
				$params_kl['dummy'] = $gbr_kl['file_name'];
				
				
			} else {print_r($this->upload->display_errors());}
			
			$last_id = $this->transaksi_model->add_batch_transaksi($params);
				// buat permintaan kl baru dengan flag 3= kuesioner
			$last_id_kl = $this->transaksi_model->add_transaksi($params_kl);
			$params = NULL;
			$gbr = NULL;

			$first_id = $last_id - ($total-1);
			for ($i=$first_id; $i <= $last_id ; $i++) { 

				$id_transaksi[] = array(
					'id_transaksi' => $i,
					'id_transaksi' => $last_id_kl,
				);

			}
			$this->transaksi_model->add_batch_transaksi($id_transaksi);
			
			
			$this->response = $this->upload->display_errors();
			$this->session->set_flashdata('sukses', "Data yg anda masukan berhasil");

			redirect('diseminasi/index');
			
		}
		else
		{            
			$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
			$data['halaman'] = 'permintaan_kuesioner';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/form_permintaan_kuesioner');
		}
		
	}

	public function tambah($id_transaksi)
	{
		$id_transaksi = $this->encrypt->decode($id_transaksi);
		$this->load->library('upload');

		
            $config['upload_path'] = './upload_file/'; //path folder
            $config['allowed_types'] = 'rar|xls|xlsx|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '50000'; //maksimum besar file 2M

            $this->upload->initialize($config);

            $judul_permintaan = $this->input->post('judul_permintaan');
            $deadline         = $this->input->post('deadline');
			$catatan          = $this->input->post('catatan');//
			$id_sm            = $this->input->post('id_sm');
			$total            = count($judul_permintaan);
			

			for ($i=0; $i <$total ; $i++) { 

				$params[] = array(
					'id_transaksi'  => $this->input->post('id_transaksi'),
					'id_sm'            => $id_sm[$i],
					'judul_permintaan' => $judul_permintaan[$i],
					'deadline'         => $deadline[$i],
					'catatan'          => $catatan[$i],
					'deadline'         =>date('Y-m-d', $deadline[$i]),
					'status'           => '1',
					'tgl_delegasi'     => date('Y-m-d'),
					
				);

			}

			$files = $_FILES['file']; 

			foreach ($files['name'] as $key => $image) {
				$_FILES['images[]']['name']= $files['name'][$key];
				$_FILES['images[]']['type']= $files['type'][$key];
				$_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
				$_FILES['images[]']['error']= $files['error'][$key];
				$_FILES['images[]']['size']= $files['size'][$key];

				if ($this->upload->do_upload('images[]')) {
					$gbr[] = $this->upload->data();

					
				} else {
					echo 'gagal upload file';
	                //return false;
				}
			}
			foreach ($params as $key => $value) {
				$params[$key]['dummy']= $gbr[$key]['file_name'];
	        	//$data[] = array_merge($params[$key], $gbr[$key]);
			}
			$this->response = $this->upload->display_errors();

			$this->session->set_flashdata('sukses', "Data yg anda masukan berhasil");
		}

	public function download_($file, $id, $jenis, $trans) { //$jenis = 1 -> dummy, $jenis = 2 -> file
		$file 	 = $this->encrypt->decode($file);
		$id   	 = $this->encrypt->decode($id);
		$trans   = $this->encrypt->decode($trans);
        //load the download helper
		$this->load->helper('download');
		
		$this->model_download->insert_($file, $id, $jenis, $trans);
		//$this->transaksi_model->update_read($id_transaksi);

		header("Content-Type:  application/vnd.ms-excel, application/rar, application/zip, ");
		force_download("upload_file/".$file, NULL);
	}

	public function download($file) {
		$file = $this->encrypt->decode($file);
        //load the download helper
		$this->load->helper('download');
        //set the textfile's content 
		$data = 'Hello world! Codeigniter rocks!';
        //set the textfile's name
		$name = 'filedownload.txt';
		$this->db->where('file', $file);
		$this->db->or_where('dummy',$file);
		
		$isi_tabel=$this->db->get('transaksi')->row_array();
		$this->model_download->insert($file);
		$this->transaksi_model->update_read($file);
		//print_r($isi_tabel['id_sm']);
		header("Content-Type:  application/vnd.ms-excel, application/rar, application/zip, ");
		force_download("upload_file/".$isi_tabel['id_sm']."/".$file, NULL);
	}

    public function download_arc($file, $id_arc, $jenis) { //$jenis = 1 -> dummy, $jenis = 2 -> file
    	$file = $this->encrypt->decode($file);
    	$id_arc = $this->encrypt->decode($id_arc);
        //load the download helper
    	$this->load->helper('download');
    	$this->load->model('model_read_arc');
    	$this->model_download->insert_arc($file, $id_arc, $jenis);
    	
    	header("Content-Type:  application/vnd.ms-excel, application/rar, application/zip, ");
    	force_download("upload_file/".$file, NULL);
    }

    public function detail_permintaan_kl($id_arc)
    {
    	$id_arc = $this->encrypt->decode($id_arc);
    	
    	$data['data'] =$this->transaksi_model->get_transaksi_id_3($id_arc);
    	$data['data_kl'] =$this->transaksi_model->get_kl_arc_permintaan_kl($id_arc);
    	$data['halaman'] = 'permintaan_kl';
		// print_r($data['data']);
		// exit();
    	$this->load->vars($data);
    	$this->template->load('template/template_diseminasi','diseminasi/detail_permintaan_kl');
    }

    public function delegasi_permintaan_kl2($id_transaksi)
    {

    	$id_transaksi = $this->encrypt->decode($id_transaksi);
    	$this->load->library('form_validation');
    	$this->form_validation->set_rules('id_tabel','id_tabel','max_length[100]|required');
    	
    	if($this->form_validation->run())     
    	{   
    		$params = array(
    			'id_transaksi' => $id_transaksi,
    			'id_tabel' => $this->input->post('id_tabel'),
    		);
    		$this->relasi_transaksi_model->add($params);
    		$this->session->set_flashdata('sukses', "berhasil mendelegasikan permintaan K/L");
    		redirect('diseminasi/permintaan_kl_delegasi');
    		
    	}
    	else
    	{            
    		$data['data'] =$this->transaksi_model->get_transaksi_id_2($id_transaksi);
    		$data['master_tabel']= $this->master_tabel_model->get_all();
    		$data['halaman'] = 'permintaan_kl';
    		$this->load->vars($data);
    		$this->template->load('template/template_diseminasi','diseminasi/delegasi_permintaan_kl2');
    	}

    }
    
    public function detail_permintaan_kl_di_sm($id_transaksi)
    {
    	$id_transaksi = $this->encrypt->decode($id_transaksi);
    	$data['data'] =$this->transaksi_model->get_id($id_transaksi);
    	$data['halaman'] = 'permintaan_kl';
    	$this->load->vars($data);
    	$this->template->load('template/template_diseminasi','diseminasi/detail_permintaan_kl_di_sm');
    }

    
    public function edit_permintaan_kl($id_arc)
    {
    	$id_arc = $this->encrypt->decode($id_arc);
    	$this->load->library('form_validation');
        // $this->form_validation->set_rules('no_induk','No Induk','max_length[40]');
    	
    	
    	$this->form_validation->set_rules('tanggal_arc','tanggal_arc','required');
    	
    	
    	if($this->form_validation->run())     
    	{   
    		
    		$this->load->library('upload');

    		
            //$config['upload_path'] = './upload_file/'.$this->input->post('id_sm').'/'; //path folder untuk di pisah by SM
            $config['upload_path'] = './upload_file/'; //path folder
            
            $config['allowed_types'] = 'rar|xls|xlsx|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '50000'; //maksimum besar file 2M

            $this->upload->initialize($config);

            $params = array(
            	
            	'tahun'            => $this->input->post('tahun'),
            	'periode_arc'       => $this->input->post('periode_arc'),
            	'catatan_ke_kl_arc' => $this->input->post('catatan_ke_kl'),
            	'catatan_ke_sm_arc' => $this->input->post('catatan_ke_sm'),
            	'catatan_dari_sm_arc'  => $this->input->post('catatan_dari_sm'),
            	'tanggal_arc'         =>date('Y-m-d', strtotime($this->input->post('tanggal_arc'))),
            	
            );

            
            if (!empty($_FILES['file']['name'])) {
            //print_r($config);
            	if ($this->upload->do_upload('file'))
            	{
            		$gbr = $this->upload->data();
            		$params['file_arc'] = $gbr['file_name'];
            		
            		
            	} else {
            		$this->session->set_flashdata('gagal', $this->upload->display_errors());
            		$this->load->library('user_agent');

            		redirect($this->agent->referrer());
            		
            		//unset($this->upload->display_errors());
            	} 
            }
            
            	$this->response = $this->upload->display_errors();
            //print_r(date('Y-m-d', strtotime($this->input->post('deadline'))));
            	//$id_transaksi = $this->input->post('id_transaksi');
            	//$this->transaksi_model->update_transaksi($id_transaksi,$params);
            	$cek = $this->arc_model->update($id_arc,$params);
            		
            	$this->session->set_flashdata('sukses', "Data berhasil diupdate");

            	//$arah = ($this->input->post('flag')==1) ? 'permintaan_kl' : 'permintaan_sm' ;
            	redirect('diseminasi/permintaan_kl');
            	
            }
            else
            {            
            	$data['data'] =$this->transaksi_model->get_transaksi_id_3($id_arc);
            	$data['master_sm'] = $this->master_sm_model->get_all_master_sm();
            	$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
            	$data['master_subject'] = $this->subject_model->get_all();
            	$data['master_pks'] = $this->pks_model->get_all();
            	$data['halaman'] =  ($data['data']['flag']==1) ? 'permintaan_kl' : 'permintaan_sm' ;
            	$this->load->vars($data);
            	$this->template->load('template/template_diseminasi','diseminasi/edit_permintaan_kl');
            }
            
        }
        public function edit_permintaan_sm($id_arc)
        {
        	$id_arc = $this->encrypt->decode($id_arc);
        	$this->load->library('form_validation');
        
        	$this->form_validation->set_rules('tanggal_arc','tanggal_arc','required');
        	
        	
        	if($this->form_validation->run())     
        	{   
        		
        		$this->load->library('upload');
            //$config['upload_path'] = './upload_file/'.$this->input->post('id_sm').'/'; //path folder untuk di pisah by SM
            $config['upload_path'] = './upload_file/'; //path folder
            
            $config['allowed_types'] = 'rar|xls|xlsx|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '50000'; //maksimum besar file 2M

            $this->upload->initialize($config);

            $params = array(
            	
            	'tahun'            => $this->input->post('tahun'),
            	'periode_arc'       => $this->input->post('periode_arc'),
            	'catatan_ke_kl_arc' => $this->input->post('catatan_ke_kl'),
            	'catatan_ke_sm_arc' => $this->input->post('catatan_ke_sm'),
            	'catatan_dari_kl_arc'  => $this->input->post('catatan_dari_kl'),
            	'tanggal_arc'         =>date('Y-m-d', strtotime($this->input->post('tanggal_arc'))),
            	
            );

            
            if (!empty($_FILES['file']['name'])) {
            //print_r($config);
            	if ($this->upload->do_upload('file'))
            	{
            		$gbr = $this->upload->data();
            		$params['file_arc'] = $gbr['file_name'];
            		
            		
            	} else {
            		$this->session->set_flashdata('gagal', $this->upload->display_errors());
            		$this->load->library('user_agent');

            		redirect($this->agent->referrer());
            		
            		//unset($this->upload->display_errors());
            	} 
            }
            	
            	$this->response = $this->upload->display_errors();
            	$update = $this->arc_model->update($id_arc,$params);
            	$this->session->set_flashdata('sukses', "Data berhasil diupdate");
            	
            	redirect('diseminasi/permintaan_sm');
            	
            }
            else
            {            
            	$data['data'] =$this->transaksi_model->get_transaksi_id_2($id_arc);
            	$data['master_sm'] = $this->master_sm_model->get_all_master_sm();
            	$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
            	$data['master_subject'] = $this->subject_model->get_all();
            	$data['master_pks'] = $this->pks_model->get_all();
            	$data['halaman'] =  ($data['data']['flag']==1) ? 'permintaan_kl' : 'permintaan_sm' ;
            	$this->load->vars($data);
            	$this->template->load('template/template_diseminasi','diseminasi/edit_permintaan_sm');
            }
            
        }
        
        public function hapus_permintaan()
        {
        	$id_transaksi = $this->input->post('id_transaksi');
        	$this->transaksi_model->hapus_transaksi($id_transaksi);
        	$this->session->set_flashdata('sukses', "Berhasil menghapus permintaan");

        	$arah = ($this->input->post('flag')==1) ? 'permintaan_kl' : 'permintaan_sm' ;
        	redirect('diseminasi/'.$arah);
        }
        public function hapus_permintaan_sm()
        {
        	$id_transaksi = $this->input->post('id_transaksi');
        	$this->transaksi_model->hapus_transaksi($id_transaksi);
        	$this->session->set_flashdata('sukses', "Berhasil menghapus permintaan");

        	$arah = ($this->input->post('flag')==1) ? 'permintaan_kl' : 'permintaan_sm' ;
        	redirect('diseminasi/'.$arah);
        }
        
        public function tolak_permintaan_kl()
        {
        	$id_transaksi = $this->input->post('id_transaksi');
        	$id_transaksi = $this->input->post('id_transaksi');
        	$params = array(
	            	'status_diseminasi' => ($this->input->post('status_diseminasi') == 3) ? $this->input->post('status_diseminasi') : 4 , //4 = permintaan ditolak
	            	'catatan_diseminasi' => $this->input->post('catatan_diseminasi'),
	            );
        	$this->transaksi_model->update_transaksi($id_transaksi,$id_transaksi,$params);
        	$this->session->set_flashdata('sukses', "berhasil mengubah status menjadi tolak");
        	$this->load->library('user_agent');

        	redirect($this->agent->referrer());
        }

        public function kirim_permintaan_kl()
        {
        	$id_arc = $this->input->post('id_arc');
        	$id_transaksi = $this->input->post('id_transaksi');
        	$params_sm = array(
					'status_arc' => ($this->input->post('status') == 3) ? $this->input->post('status') : 4 , //4 = permintaan ditolak
					'status_dis_arc' => ($this->input->post('status') == 3) ? 5 : 7 , //5 = selesai (kirim), 7 = ditolak diseminasi
					'catatan_ke_kl_arc' => (!empty($this->input->post('catatan_ke_kl_arc'))) ? $this->input->post('catatan_ke_kl_arc') : $this->input->post('catatan') ,
					'tgl_dis_arc' =>date('Y-m-d'),
					'read_arc' =>0,
				);
        	$this->arc_model->update($id_arc,$params_sm);
        	$this->session->set_flashdata('sukses', "berhasil mengirim data ke K/L");
        	
        	if ($this->input->post('status') == 3) {
	        	$id_isi_email = 1; //data telah selesai
	        	$flag = 1; // permintaan kl
	        	$tipe = 1; // kirim email ke sm / kl
	        	$this->model_email->kirim_email($id_arc,$id_isi_email, $flag, $tipe);
        	} else {
	        	$id_isi_email = 2; //data telah selesai
	        	$flag = 1; // permintaan kl
	        	$tipe = 1; // kirim email ke sm / kl
	        	$this->model_email->kirim_email($id_arc,$id_isi_email, $flag, $tipe);
        	}
        	redirect('diseminasi/permintaan_kl','refresh');
    	}

	public function kirim_permintaan_sm() // edit bagian ini
	{
		$id_arc = $this->input->post('id_arc');
    	$params_sm = array(
				'status_arc' => ($this->input->post('status') == 3) ? $this->input->post('status') : 4 , //4 = permintaan ditolak
				'status_dis_arc' => ($this->input->post('status') == 3) ? 5 : 7 , //5 = selesai (kirim), 7 = ditolak diseminasi
				'catatan_ke_sm_arc' => (!empty($this->input->post('catatan_ke_sm_arc'))) ? $this->input->post('catatan_ke_sm_arc') : $this->input->post('catatan') ,
				'tgl_dis_arc' =>date('Y-m-d'),
				'read_arc' =>0,
			);
    	$this->arc_model->update($id_arc,$params_sm);
    	$this->session->set_flashdata('sukses', "berhasil mengirim data ke SM");
    	
    	if ($this->input->post('status') == 3) {
        	$id_isi_email = 1; //data telah selesai
        	$flag = 2; // permintaan sm
        	$tipe = 1; // kirim email ke sm / kl
        	$this->model_email->kirim_email($id_arc,$id_isi_email, $flag, $tipe);
    	} else {
        	$id_isi_email = 2; //data ditolak
        	$flag = 2; // permintaan sm
        	$tipe = 1; // kirim email ke sm / kl
        	$this->model_email->kirim_email($id_arc,$id_isi_email, $flag, $tipe);
    	}
    	redirect('diseminasi/permintaan_sm','refresh');

		
	}

	public function kirim_permintaan_katalog()
	{
		$flag = $this->input->post('flag');
		$file = $this->input->post('file');
		$id_transaksi = $this->input->post('id_transaksi');
		$params_sm = array(
			'file' => $file,
			
		);
		$this->transaksi_model->update_transaksi($id_transaksi,$params_sm);
			if ($flag == 1) { // permintaan dari K/L
				$this->kirim_permintaan_kl();
			} else if($flag == 2){ // permintaan dari SM
				$this->kirim_permintaan_sm();
			}
			
			
		}

	public function delegasi_permintaan_sm() // edit bagian ini
	{
		$id_transaksi = $this->input->post('id_transaksi');
		$params_sm = array(
	            	'status' => ($this->input->post('status') == 3) ? 2 : 4 , //4 = permintaan ditolak
	            	'status_dis' => ($this->input->post('status') == 3) ? 1 : '' , //4 = permintaan ditolak
	            	'catatan_ke_kl' => $this->input->post('catatan_ke_kl'),
	            );
		$this->transaksi_model->update_transaksi($id_transaksi,$params_sm);
		
		$this->session->set_flashdata('sukses', "berhasil mendelegasikan permintaan ke K/L");
			// $this->load->library('user_agent');

			// redirect($this->agent->referrer());
		redirect('diseminasi/permintaan_sm','refresh');
	}

	public function meneruskan_data_ke_kl()
	{
		$asli = $this->transaksi_model->get_transaksi_id($this->input->post('id_transaksi'));
		$id_transaksi_update = $this->input->post('id_transaksi_update');
		$params = array(
			'status'     => 3,
			'status_dis'     => 5,
			'file'  => $asli['file'],
			'catatan_ke_kl' => $this->input->post('catatan_diseminasi'),
		);

		$this->transaksi_model->update_transaksi($id_transaksi_update,$params);
		
		$this->load->library('user_agent');

		redirect($this->agent->referrer());
	}

	public function katalog()
	{
		$data['halaman'] = 'katalog';
		$data['transaksi'] = $this->transaksi_model->get_all_menunggu();
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/katalog');
	}

	public function ajax_list() //untuk ajax datatable server side
	{
		$this->load->model('katalog_model');
		$list = $this->katalog_model->get_datatables();
        //print_r($list);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $transaksi) {
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $transaksi->id_transaksi; 
			$row[] = str_replace("Statistik","",$transaksi->nama); 
			$row[] = $transaksi->judul_permintaan;
			$row[] = $transaksi->deadline;
			$row[] = ($transaksi->file != NULL) ? '<a href="'.base_url('diseminasi/download_/').$this->encrypt->encode($transaksi->file).'/'.$this->encrypt->encode($transaksi->id_transaksi).'/2'.'"> <button class="btn btn-success btn-sm">Download</button>'  : '' ;
			
			$row[] = '<a  value="'.$transaksi->id_transaksi.'" data-toggle="modal" data-target="#modal_aksi" data-id_transaksi="'.$transaksi->id_transaksi.'"  data-flag="'.$transaksi->flag.'" data-file="'.$transaksi->file.'"><button class="btn btn-danger btn-sm">Aksi</button></a>';
			
			$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->katalog_model->count_all(),
			"recordsFiltered" => $this->katalog_model->count_filtered(),
			"data" => $data,
		);
        //output to json format
		echo json_encode($output);
	}
	
	public function detail_permintaan_sm($id_arc)
	{
		$id_arc = $this->encrypt->decode($id_arc);
		$data['data'] =$this->transaksi_model->get_transaksi_id_2($id_arc);
		$data['halaman'] = 'permintaan_sm';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/detail_permintaan_sm');
	}
	
	public function detail_permintaan_sm_di_kl($id_transaksi)
	{
		$id_transaksi = $this->encrypt->decode($id_transaksi);
		$data['data'] =$this->transaksi_model->get_transaksi_id_2($id_transaksi);
		$data['halaman'] = 'permintaan_sm';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/detail_permintaan_sm_di_kl');
	}
	
	public function tambah_permintaan_ke_kl(){
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('judul_permintaan','No Induk','max_length[40]');
    		// $this->form_validation->set_rules('judul_permintaan','judul','max_length[100]|required');
    		// // $this->form_validation->set_rules('indikator[]','penulis','max_length[100]|required');
		$this->form_validation->set_rules('deadline','penerbit','required');
		
		
		if($this->form_validation->run())     
		{   

			$judul_permintaan = $this->input->post('judul_permintaan');
			$deadline         = $this->input->post('deadline');
			$catatan          = $this->input->post('catatan');
			
			$params[] = array(
				
				'judul_permintaan' => $judul_permintaan,
				'catatan'          => $catatan,
				'deadline'         =>date('Y-m-d', strtotime($deadline)),
				'status'           => '1',
				// 'tgl'              => date('Y-m-d'),
				'flag'           => '2', //permintaan dari BPS ke K/L
				
			);

			$this->transaksi_model->add_transaksi($params);
			
			
            //print_r($this->upload->display_errors());
            //print_r(date('Y-m-d', strtotime($this->input->post('deadline'))));
			
			$this->session->set_flashdata('sukses', "Data yg anda masukan berhasil");

			redirect('diseminasi/index');
			
		}
		else
		{            
			$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
			$data['halaman'] = 'permintaan_sm';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/form_permintaan_ke_kl');
		}
	}

	function form_tambah_permintaan(){
		$this->load->model('periode_model');
		$data['halaman'] = 'tambah';
		$data['master_sm'] = $this->master_sm_model->get_all_master_sm();
		$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
		$data['daftar_subject']= $this->subject_model->get_all();
		$data['daftar_periode']= $this->periode_model->get_all();
		$data['master_pks'] = $this->pks_model->get_all();
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/form_tambah_permintaan');
	}
	
	function sendEmail()
	{ 
		$subject = 'ini subjek';
		$pesan = 'ini pesan';
		$cek = $this->model_email->kirim_email($subject, $pesan);
		print_r($cek);
	}

	public function tambah_excel(){
			$data = array(); // Buat variabel $data sebagai array
			
			
			  // lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->transaksi_model->upload_file($this->filename);
			
			  if($upload['result'] == "success"){ // Jika proses upload sukses
				// Load plugin PHPExcel nya
			  	include APPPATH.'third_party/PHPExcel/PHPExcel.php';
			  	
			  	$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel_trans/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
				
				// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
				$data = array();
				
				$numrow = 1;
				$flag = $this->input->post('flag');
				foreach($sheet as $row){
				  // Cek $numrow apakah lebih dari 1
				  // Artinya karena baris pertama adalah nama-nama kolom
				  // Jadi dilewat saja, tidak usah diimport
					if($numrow > 1 && !empty($row['A'])){
					// Kita push (add) array data ke variabel data
						if ($flag == 1) {
							array_push($data, array(
					  'id_kl'=>$row['A'], // Insert data nis dari kolom A di excel
					  'judul_permintaan'=>$row['B'], // Insert data jenis kelamin dari kolom C di excel
					  'status'=>'1',
					  'jenis'=>1,
					  'flag'=> 1,
					  'status_dis'=> 8,
					  'no_pks'    => $row['C'],
					  'catatan_dari_kl'    => $row['D'],
					));
						} else {
							array_push($data, array(
					  'id_kl'=>$row['A'], // Insert data nis dari kolom A di excel
					  'id_sm'=>$row['B'], // Insert data nama dari kolom B di excel
					  'judul_permintaan'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
					  'deadline'=>$row['D'], // Insert data alamat dari kolom D di excel
					  'status'=>1,
					  'jenis'=>1,
					  // 'tgl'=>date('Y-m-d'),
					  'flag'=> $this->input->post('flag'),
					  'status_dis'=> $this->input->post('flag'),
					  'catatan_ke_sm'    => ($this->input->post('flag') == 2) ? '' : $row['E'],
					));
						}
					}
					
				  $numrow++; // Tambah 1 setiap kali looping
				}
				
				// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
				$this->db->trans_start(FALSE);
				$this->transaksi_model->add_batch_transaksi($data);
				$this->db->trans_complete();
				$db_error = $this->db->error();
				if (!empty($db_error['code']) || !empty($db_error['message'])) {
					$this->session->set_flashdata('gagal', 'file yang diupload tidak sesuai, pastikan sesuai dengan template yang benar');
					$this->load->library('user_agent');

					redirect($this->agent->referrer());
				}
				
			  }else{ // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
				$this->session->set_flashdata('gagal', $upload['error']);
				$this->load->library('user_agent');

				redirect($this->agent->referrer());
			}
			
			
			$this->session->set_flashdata('sukses', "Data yg anda masukan berhasil");
			$arah = ($this->input->post('flag')==1) ? 'permintaan_kl' : 'permintaan_sm' ;
			redirect('diseminasi/'.$arah);
			
		}
		public function tambah_permintaan()
		{
		//flag 1 -> permintaan dari kl
		//flag 2 -> permintaan dari sm
			$params = array(
				
				'judul_permintaan' => $this->input->post('judul_permintaan'),
				'catatan_ke_kl'    => ($this->input->post('flag') == 1) ? '' : $this->input->post('catatan'),
				'catatan_ke_sm'    => ($this->input->post('flag') == 2) ? '' : $this->input->post('catatan'),
				//'deadline'         => date('Y-m-d', strtotime($this->input->post('deadline'))),
				'status'           => '1',
			// 'tgl'              => date('Y-m-d'),
				'flag'             => $this->input->post('flag'),
				'status_dis'       => ($this->input->post('flag') == 1) ? '1' : '2',
				
			//'id_sm' 		   => $this->input->post('id_sm'),
				'jenis' 		   => 1,
				'no_pks'  		   => $this->input->post('no_pks'),
				'id_tabel'  	   => $this->input->post('id_tabel'),
			);

			$this->load->library('upload');

            //$config['upload_path'] = './upload_file/'.$this->input->post('id_sm').'/'; //path folder untuk di pisah by SM
            $config['upload_path'] = './upload_file/'; //path folder
            
            $config['allowed_types'] = 'rar|xls|xlsx|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '50000'; //maksimum besar file 2M

            $this->upload->initialize($config);

            //print_r($config);
            if ($this->upload->do_upload('file'))
            {
            	$gbr = $this->upload->data();
            	$params['dummy'] = $gbr['file_name'];
            	
            } else {//print_r($this->upload->display_errors());
            	$this->session->set_flashdata('gagal', $this->upload->display_errors());

            	$this->load->library('user_agent');

            	redirect($this->agent->referrer());
			}//error upload data. terima kasih
			
			if ($this->input->post('flag') == 2) {
				$params['id_sm'] = $this->input->post('id_sm');
				$this->transaksi_model->add_transaksi($params);
			}else {
				$params['id_kl'] = $this->input->post('id_kl');
				$this->transaksi_model->add_transaksi($params);
			}

			$this->session->set_flashdata('sukses', "Data yg anda masukan berhasil");
			$arah = ($this->input->post('flag')==1) ? 'permintaan_kl' : 'permintaan_sm' ;
			redirect('diseminasi/'.$arah);
		}
		
		public function master_mou()
		{
			
			$data['halaman'] = 'master_mou';
			$data['data'] = $this->mou_model->get_all();
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/daftar_mou');
		}

		public function master_pks()
		{
			
			$data['halaman'] = 'master_pks';
			$data['data'] = $this->pks_model->get_all();
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/daftar_pks');
		}

		public function download_surat($file) {
		//$file = $this->encrypt->decode($file);
        //load the download helper
			$this->load->helper('download');
        //set the textfile's content 
			$data = 'Hello world! Codeigniter rocks!';
        //set the textfile's name
			$name = 'filedownload.txt';
			
		//print_r($isi_tabel['id_sm']);
			header("Content-Type:  application/pdf, application/rar, application/zip, ");
			force_download("upload_file/surat/".$file, NULL);
		}
		
		public function edit_mou($id_mou)
		{
			$id_mou = $this->encrypt->decode($id_mou);
			$this->load->library('form_validation');
        // $this->form_validation->set_rules('no_induk','No Induk','max_length[40]');
			$this->form_validation->set_rules('no_mou','no mou','max_length[100]|required');
			$this->form_validation->set_rules('id_kl','id kl','max_length[100]|required');
			
			
			if($this->form_validation->run())     
			{   
				
				$this->load->library('upload');

				
            $config['upload_path'] = './upload_file/surat/'; //path folder
            
            $config['allowed_types'] = 'rar|pdf|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '100000'; //maksimum besar file 10M

            $this->upload->initialize($config);

            $params = array(
            	'id_kl'            	=> $this->input->post('id_kl'),
            	'no_mou'            => $this->input->post('no_mou'),
            	'mulai_berlaku' 	=> date('Y-m-d', strtotime($this->input->post('mulai_berlaku'))),
            	'tahun' 			=> $this->input->post('tahun'),
            	'narasi_mou' 		=> $this->input->post('narasi_mou'),
            );
            
            
            //print_r($config);
            if ($this->upload->do_upload('file'))
            {
            	$gbr = $this->upload->data();
            	$params['file_mou'] = $gbr['file_name'];
            	
            	
            } else {//print_r($this->upload->display_errors());
            	
			}//error upload data. terima kasih
			
			$this->response = $this->upload->display_errors();
            //print_r(date('Y-m-d', strtotime($this->input->post('deadline'))));
			$this->mou_model->update($id_mou,$params);
			$this->session->set_flashdata('sukses', "Data berhasil diupdate");

			
			redirect('diseminasi/master_mou');
			
		}
		else
		{            
			$data['data'] =$this->mou_model->get_id($id_mou);
			
			$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
			$data['halaman'] =  'master_mou';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/edit_mou');
		}
		
	}

	public function edit_pks($id_pks)
	{
		$id_pks = $this->encrypt->decode($id_pks);
		$this->load->library('form_validation');
        // $this->form_validation->set_rules('no_induk','No Induk','max_length[40]');
		$this->form_validation->set_rules('no_pks','no pks','max_length[100]|required');
		$this->form_validation->set_rules('id_mou','id mou','max_length[100]|required');
		
		
		if($this->form_validation->run())     
		{   
			
			$this->load->library('upload');

			
            $config['upload_path'] = './upload_file/surat/'; //path folder
            
            $config['allowed_types'] = 'rar|pdf|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '100000'; //maksimum besar file 10M

            $this->upload->initialize($config);

            $params = array(
            	
            	'id_mou'            => $this->input->post('id_mou'),
            	'no_pks'            => $this->input->post('no_pks'),
            	'mulai_berlaku' 	=> date('Y-m-d', strtotime($this->input->post('mulai_berlaku'))),
            	'tahun' 			=> $this->input->post('tahun'),
            	'narasi_pks' 		=> $this->input->post('narasi_pks'),
            	
            );
            
            
            //print_r($config);
            if ($this->upload->do_upload('file'))
            {
            	$gbr = $this->upload->data();
            	$params['file_pks'] = $gbr['file_name'];
            	
            	
            } else {//print_r($this->upload->display_errors());
            	
			}//error upload data. terima kasih
			
			$this->response = $this->upload->display_errors();
            //print_r(date('Y-m-d', strtotime($this->input->post('deadline'))));
			$this->pks_model->update($id_pks,$params);
			$this->session->set_flashdata('sukses', "Data berhasil diupdate");

			
			redirect('diseminasi/master_pks');
			
		}
		else
		{            
			$data['data'] =$this->pks_model->get_id($id_pks);
			
			$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
			$data['master_mou'] = $this->mou_model->get_all();
			$data['halaman'] =  'master_pks';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/edit_pks');
		}
		
	}

	public function tambah_mou()
	{
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('no_mou','no mou','max_length[100]|required');
		$this->form_validation->set_rules('id_kl','id kl','max_length[100]|required');
		
		if($this->form_validation->run())     
		{   
			
			$this->load->library('upload');

            $config['upload_path'] = './upload_file/surat/'; //path folder
            $config['allowed_types'] = 'rar|pdf|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '100000'; //maksimum besar file 10M

            $this->upload->initialize($config);

            $params = array(
            	'id_kl'            	=> $this->input->post('id_kl'),
            	'no_mou'            => $this->input->post('no_mou'),
            	'mulai_berlaku' 	=> date('Y-m-d', strtotime($this->input->post('mulai_berlaku'))),
            	'tahun' 			=> $this->input->post('tahun'),
            	
            );
            
            if ($this->upload->do_upload('file'))
            {
            	$gbr = $this->upload->data();
            	$params['file_mou'] = $gbr['file_name'];
            	
            } else {//print_r($this->upload->display_errors());
            	
			}//error upload data. terima kasih
			
			$this->response = $this->upload->display_errors();
			$this->mou_model->add($params);
			$this->session->set_flashdata('sukses', "Data berhasil diupdate");

			redirect('diseminasi/master_mou');
			
		}
		else
		{            
			
			
			$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
			$data['halaman'] =  'master_mou';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/tambah_mou');
		}
		
	}

	public function tambah_pks()
	{
		
		$this->load->library('form_validation');
        // $this->form_validation->set_rules('no_induk','No Induk','max_length[40]');
		$this->form_validation->set_rules('no_pks','no pks','max_length[100]|required');
		$this->form_validation->set_rules('id_mou','id mou','max_length[100]|required');
		
		
		if($this->form_validation->run())     
		{   
			
			$this->load->library('upload');

			
            $config['upload_path'] = './upload_file/surat/'; //path folder
            
            $config['allowed_types'] = 'rar|pdf|zip'; //type yang dapat diakses bisa anda sesuaikan
            $config['max_size'] = '100000'; //maksimum besar file 10M

            $this->upload->initialize($config);

            $params = array(
            	
            	'id_mou'            => $this->input->post('id_mou'),
            	'no_pks'            => $this->input->post('no_pks'),
            	'mulai_berlaku' 	=> date('Y-m-d', strtotime($this->input->post('mulai_berlaku'))),
            	'tahun' 			=> $this->input->post('tahun'),
            	'narasi_pks' 		=> $this->input->post('narasi_pks'),
            	
            );
            
            
            //print_r($config);
            if ($this->upload->do_upload('file'))
            {
            	$gbr = $this->upload->data();
            	$params['file_pks'] = $gbr['file_name'];
            	
            	
            } else {//print_r($this->upload->display_errors());
            	
			}//error upload data. terima kasih
			
			$this->response = $this->upload->display_errors();
            //print_r(date('Y-m-d', strtotime($this->input->post('deadline'))));
			$this->pks_model->add($params);
			$this->session->set_flashdata('sukses', "Data berhasil diupdate");

			redirect('diseminasi/master_pks');
			
		}
		else
		{            
			
			
			$data['master_kl'] = $this->master_kl_model->get_all_master_kl();
			$data['master_mou'] = $this->mou_model->get_all();
			$data['halaman'] =  'master_pks';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/tambah_pks');
		}
		
	}

	function batas_komplain(){
		$this->load->model('batas_komplain_model');
		$data['halaman'] = 'batas_komplain';
		$data['data'] = $this->batas_komplain_model->get_all();
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/daftar_batas_komplain');
	}

	function edit_batas_komplain(){
		$this->load->model('batas_komplain_model');
		$id_batas_komplain = $this->input->post('id_batas_komplain');
		$params = array(
			'batas_komplain' => $this->input->post('batas_komplain'),
			
		);
		$this->batas_komplain_model->update($id_batas_komplain, $params);
		$this->session->set_flashdata('sukses', "berhasil mengedit batas komplain");
		$this->load->library('user_agent');

		redirect($this->agent->referrer());
		
	}


	function jadwal_notif(){
		$this->load->model('jadwal_notif_model');
		$data['halaman'] = 'jadwal_notif';
		$data['data'] = $this->jadwal_notif_model->get_all();
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/daftar_jadwal_notif');
	}
	function hapus_jadwal_notif($id_notif){
		$this->load->model('jadwal_notif_model');
		
		$this->jadwal_notif_model->delete($id_notif);
		$this->session->set_flashdata('sukses', "berhasil menghapus jadwal notif");
		$this->load->library('user_agent');

		redirect($this->agent->referrer());
		
	}
	function tambah_jadwal_notif(){
		$this->load->model('jadwal_notif_model');
		$params = array('hari' => $this->input->post('hari') );
		$this->jadwal_notif_model->add($params);
		$this->session->set_flashdata('sukses', "berhasil menambahkan jadwal notif");
		$this->load->library('user_agent');

		redirect($this->agent->referrer());
		
	}

	function struktural_layanan(){
		$this->load->model('struktural_layanan_model');
		$data['halaman'] = 'struktural_layanan';
		$data['data'] = $this->struktural_layanan_model->get_all();
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/daftar_struktural_layanan');
	}

	function edit_struktural_layanan(){
		$this->load->model('struktural_layanan_model');
		$id = $this->input->post('id');
		$params = array(
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
		);
		$this->struktural_layanan_model->update($id, $params);
		$this->session->set_flashdata('sukses', "berhasil mengedit struktural_layanan");
		$this->load->library('user_agent');

		redirect($this->agent->referrer());
		
	}

	function master_sm(){
		$this->load->model('master_sm_model');
		$data['halaman'] = 'master_sm';
		$data['data'] = $this->master_sm_model->get_all();
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/daftar_master_sm');
	}

	function edit_data_sm(){
		$this->load->model('master_sm_model');
		$id_data_sm = $this->input->post('id_data_sm');
		$params = array(
			'nama_pegawai' => $this->input->post('nama_pegawai'),
			'email' => $this->input->post('email'),
		);
		$this->master_sm_model->update_data_sm($id_data_sm, $params);
		$this->session->set_flashdata('sukses', "berhasil mengedit data Subject Matter");
		$this->load->library('user_agent');

		redirect($this->agent->referrer());
	}

	function master_kl(){
		$this->load->model('master_kl_model');
		$data['halaman'] = 'master_kl';
		$data['data'] = $this->master_kl_model->get_all();
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/daftar_master_kl');
	}

	
	
	public function tambah_master_kl()
	{
		$this->load->model('master_kl_model');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nama_pusdatin','nama pusdatin','max_length[100]|required');
		$this->form_validation->set_rules('email_pusdatin','email pusdatin','max_length[100]|required');
		
		
		if($this->form_validation->run())     
		{   
			
			$params = array(
				'nama_pusdatin' => $this->input->post('nama_pusdatin'),
				'email_pusdatin' => $this->input->post('email_pusdatin'),
				'bagian_pusdatin' => $this->input->post('bagian_pusdatin'),
				'nama' => $this->input->post('nama'),
				'alamat_pusdatin' => $this->input->post('alamat_pusdatin'),
				'telepon' => $this->input->post('telepon'),
				'fax' => $this->input->post('fax'),
				'link' => $this->input->post('link'),
				
			);
			$this->master_kl_model->add($params);
			$this->session->set_flashdata('sukses', "berhasil menambahkan master K/L");
			

			redirect('diseminasi/master_kl');
			
			
		}
		else
		{            
			
			$data['halaman'] =  'master_kl';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/tambah_master_kl');
		}
		
	}

	public function edit_master_kl($id_kl)
	{
		$this->load->model('master_kl_model');
		
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('nama_pusdatin','nama pusdatin','max_length[100]|required');
		$this->form_validation->set_rules('email_pusdatin','email pusdatin','max_length[100]|required');
		
		
		if($this->form_validation->run())     
		{   
			
			$params = array(
				'nama_pusdatin' => $this->input->post('nama_pusdatin'),
				'email_pusdatin' => $this->input->post('email_pusdatin'),
				'bagian_pusdatin' => $this->input->post('bagian_pusdatin'),
				'nama' => $this->input->post('nama'),
				'alamat_pusdatin' => $this->input->post('alamat_pusdatin'),
				'telepon' => $this->input->post('telepon'),
				'fax' => $this->input->post('fax'),
				'link' => $this->input->post('link'),
				'alias' => $this->input->post('alias'),
			);
			$this->master_kl_model->update($id_kl, $params);
			$this->session->set_flashdata('sukses', "berhasil mengedit master K/L");
			redirect('diseminasi/master_kl');
			
		}
		else
		{            
			$data['data']= $this->master_kl_model->get_id($id_kl);	
			$data['halaman'] =  'master_kl';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/edit_master_kl');
		}
		
	}

	public function cari_judul___()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('keyword','keyword','max_length[100]|required|alpha_numeric');
		if($this->form_validation->run())     
		{ 
			$keyword = $this->input->post('keyword');
			$key = str_replace(' ', ' & ', $keyword);
			
			$data['data_transaksi'] =$this->db->query("select *, master_sm.nama as nama_sm from keranjang.transaksi join master_kl on transaksi.id_kl = master_kl.id_kl join master_sm on transaksi.id_sm = master_sm.id_sm join subject on transaksi.id_subject=subject.id_subject join status on transaksi.status=status.id_status where to_tsvector(id_transaksi|| ' ' ||judul_permintaan) @@ to_tsquery('$key')")->result_array(); // sintaks ini hanya untuk posgre

			$data['halaman'] = 'dashboard';
			$data['halaman_atas'] = 'permintaan_keluar';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/daftar_permintaan');
		} else {
			$this->load->library('user_agent');

			redirect($this->agent->referrer());
		}
	}

	public function daftar_master_tabel()
	{
		$this->load->model('periode_model');

		$data['data_kl']= $this->master_tabel_model->get_all_tabel_kl();
		$data['data_sm']= $this->master_tabel_model->get_all_tabel_sm();
		$data['daftar_sm']= $this->master_sm_model->get_all_master_sm();
		$data['daftar_kl']= $this->master_kl_model->get_all_master_kl();
		$data['daftar_subject']= $this->subject_model->get_all();
		$data['daftar_periode']= $this->periode_model->get_all();
		$data['halaman'] =  'master_tabel';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/daftar_master_tabel');
	}

	public function edit_master_tabel()
	{

		
		$this->load->library('form_validation');
		
		
		$this->form_validation->set_rules('nama_tabel','nama_tabel','max_length[100]|required');
		
		$id_tabel = $this->input->post('id_tabel');
		
		if($this->form_validation->run())     
		{   
			$flag=$this->input->post('flag');
			$params_sm = array(
				'nama_tabel' => $this->input->post('nama_tabel'),
				'id_sm' => $this->input->post('id_sm'),
				'id_subject' => $this->input->post('id_subject'),
				'id_periode' => $this->input->post('id_periode'),
				
			);
			$params_kl = array(
				'nama_tabel' => $this->input->post('nama_tabel'),
				'id_kl' => $this->input->post('id_kl'),
				'id_periode' => $this->input->post('id_periode'),
				
			);

			if ($flag == 'sm') {
				$this->master_tabel_model->update($id_tabel, $params_sm);
				
			} elseif ($flag == 'kl'){
				$this->master_tabel_model->update($id_tabel, $params_kl);
				
			}
			
			
			$this->session->set_flashdata('sukses', "berhasil mengedit master tabel");
			redirect('diseminasi/daftar_master_tabel');
			
		}
		else
		{            
			$data['data']= $this->master_tabel_model->get_all();
			$data['daftar_sm']= $this->master_sm_model->get_all_master_sm();
			$data['daftar_subject']= $this->subject_model->get_all();
			
			
			$this->session->set_flashdata('sukses', "berhasil menambahkan master tabel");
			redirect('diseminasi/daftar_master_tabel');
		}
	}

	public function tambah_master_tabel()
	{
		$this->load->model('periode_model');
		
		$this->load->library('form_validation');
		
		//$this->form_validation->set_rules('id_sm','id_sm','max_length[100]|required');
		//$this->form_validation->set_rules('id_subject','id_subject','max_length[100]|required');
		$this->form_validation->set_rules('nama_tabel','nama_tabel','max_length[100]|required');
		
		
		
		if($this->form_validation->run())     
		{   
			$flag=$this->input->post('flag');
			$params_sm = array(
				'nama_tabel' => $this->input->post('nama_tabel'),
				'id_sm' => $this->input->post('id_sm'),
				'id_subject' => $this->input->post('id_subject'),
				'id_periode' => $this->input->post('id_periode'),
				
			);
			$params_kl = array(
				'nama_tabel' => $this->input->post('nama_tabel'),
				'id_kl' => $this->input->post('id_kl'),
				'id_periode' => $this->input->post('id_periode'),
				
			);

			if ($flag == 'sm') {
				
				$this->master_tabel_model->add($params_sm);
			} elseif ($flag == 'kl'){
				
				$this->master_tabel_model->add($params_kl);
			}
			$this->session->set_flashdata('sukses', "berhasil menambahkan master tabel");
			$asal = $this->input->post('asal');
			if ($asal == 'form_permintaan') {
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			} elseif ($asal == 'form_master_tabel') {
				redirect('diseminasi/daftar_master_tabel');
			}
			
			
			
		}
		else
		{            
			$data['data']= $this->master_tabel_model->get_all();
			$data['daftar_sm']= $this->master_sm_model->get_all_master_sm();
			$data['daftar_kl']= $this->master_kl_model->get_all_master_kl();
			$data['daftar_subject']= $this->subject_model->get_all();
			$data['daftar_periode']= $this->periode_model->get_all();
			$data['halaman'] =  'master_kl';
			$this->load->vars($data);
			$this->template->load('template/template_diseminasi','diseminasi/tambah_master_tabel');
		}
	}

	public function daftar_arc($id_tabel)
	{
		$id_tabel = $this->encrypt->decode($id_tabel);
		$this->load->model('periode_model');
		$data['data_tabel']= $this->master_tabel_model->get_id($id_tabel);
		$data['data_arc']= $this->arc_model->get_id_tabel($id_tabel);
		
		$data['halaman'] =  'master_tabel';
		$this->load->vars($data);
		$this->template->load('template/template_diseminasi','diseminasi/daftar_arc');
	}


	public function generate_arc()
	{
		$id_tabel = $this->input->post('id_tabel');
		$tahun = $this->input->post('tahun');
		$data_generate = $this->master_tabel_model->get_id_generate($id_tabel);
		$data_arc = $this->arc_model->get_id_tabel($id_tabel);
		if (!empty($data_arc) && $data_arc[0]['tahun'] == $tahun) {
			$this->session->set_flashdata('gagal', "ARC telah digenerate");
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
		if (!empty($data_generate[0]['id_kl'])) {
			$status_dis_arc = 2;
		} else if (!empty($data_generate[0]['id_sm'])){
			$status_dis_arc = 1;
		}

		foreach ($data_generate as $key => $value) {
			$params[] = array(
				'tahun' => $tahun ,
				'id_tabel' => $id_tabel,
				'periode_arc' => $value['periode_arc'],
				'status_arc' => 2,
				'status_dis_arc' => $status_dis_arc,
			);
		}
		$cek = $this->model_email->kirim_email_upd_arc($id_tabel,5);
		
		$this->arc_model->add_batch($params);
		$this->session->set_flashdata('sukses', "berhasil generate ARC");
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function sync_kl()
	{
		
		
		$dari_transdata = $this->master_kl_model->get_all();
		$id = array_column($dari_transdata, 'id_kl');
		$tidak_ada_ditransdata = $this->db->select('*')->from('silastik.m_pusdatin')->where_not_in('id',$id)->get()->result_array();

		foreach ($tidak_ada_ditransdata as $key => $value) {
			$insert[] = array(
				'id_kl' => $value['id'],
				'nama_pusdatin' => $value['nama_pusdatin'],
				'email_pusdatin' => $value['email_pusdatin'],
				'nama' => $value['nama'],
				'alamat_pusdatin' => $value['alamat_pusdatin'],
				'telepon' => $value['telepon'],
				'fax' => $value['fax'],
				'email_cadangan' => $value['email_cadangan'],
				'link' => $value['link'],
				'verifikasi' => $value['verifikasi'],

				 );
		}
		if (!empty($tidak_ada_ditransdata)) {
			$this->master_kl_model->insert_batch($insert);
		}
		

		$dari_silastik = $this->master_kl_model->get_all_silastik();

		foreach ($dari_silastik as $key => $value) {
			$update = array(
				//'id_kl' => $value['id'],
				'nama_pusdatin' => $value['nama_pusdatin'],
				'email_pusdatin' => $value['email_pusdatin'],
				'nama' => $value['nama'],
				'alamat_pusdatin' => $value['alamat_pusdatin'],
				'telepon' => $value['telepon'],
				'fax' => $value['fax'],
				'email_cadangan' => $value['email_cadangan'],
				'link' => $value['link'],
				'verifikasi' => $value['verifikasi'],

				 );
			$this->db->update('keranjang.master_kl' , $update , array('id_kl' => $value['id']) );
		}

		//print_r($update[0]['id_kl']);
		//exit();
		//$this->master_kl_model->update_batch($update);
		//$this->db->update_batch('keranjang.master_kl' , $update , 'id_kl' );
		$this->session->set_flashdata('sukses', "berhasil generate ARC");
		$this->master_kl();
	}

}
