<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Disposisi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('Disposisi_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template','disposisi/sipadu_disposisi_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Disposisi_model->json();
    }

    public function read($id) 
    {
        $row = $this->Disposisi_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_disposisi' => $row->id_disposisi,
		'id_surat_masuk' => $row->id_surat_masuk,
		'nip_asal' => $row->nip_asal,
		'unit_kerja_asal' => $row->unit_kerja_asal,
		'nip_tujuan' => $row->nip_tujuan,
		'unit_kerja_tujuan' => $row->unit_kerja_tujuan,
		'catatan' => $row->catatan,
		'timestamp' => $row->timestamp,
	    );
            $this->template->load('template','disposisi/sipadu_disposisi_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('disposisi'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('disposisi/create_action'),
	    'id_disposisi' => set_value('id_disposisi'),
	    'id_surat_masuk' => set_value('id_surat_masuk'),
	    'nip_asal' => set_value('nip_asal'),
	    'unit_kerja_asal' => set_value('unit_kerja_asal'),
	    'nip_tujuan' => set_value('nip_tujuan'),
	    'unit_kerja_tujuan' => set_value('unit_kerja_tujuan'),
	    'catatan' => set_value('catatan'),
	    'timestamp' => set_value('timestamp'),
	);
        $this->template->load('template','disposisi/sipadu_disposisi_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_surat_masuk' => $this->input->post('id_surat_masuk',TRUE),
		'nip_asal' => $this->input->post('nip_asal',TRUE),
		'unit_kerja_asal' => $this->input->post('unit_kerja_asal',TRUE),
		'nip_tujuan' => $this->input->post('nip_tujuan',TRUE),
		'unit_kerja_tujuan' => $this->input->post('unit_kerja_tujuan',TRUE),
		'catatan' => $this->input->post('catatan',TRUE),
		'timestamp' => $this->input->post('timestamp',TRUE),
	    );

            $this->Disposisi_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success 2');
            redirect(site_url('disposisi'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Disposisi_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('disposisi/update_action'),
		'id_disposisi' => set_value('id_disposisi', $row->id_disposisi),
		'id_surat_masuk' => set_value('id_surat_masuk', $row->id_surat_masuk),
		'nip_asal' => set_value('nip_asal', $row->nip_asal),
		'unit_kerja_asal' => set_value('unit_kerja_asal', $row->unit_kerja_asal),
		'nip_tujuan' => set_value('nip_tujuan', $row->nip_tujuan),
		'unit_kerja_tujuan' => set_value('unit_kerja_tujuan', $row->unit_kerja_tujuan),
		'catatan' => set_value('catatan', $row->catatan),
		'timestamp' => set_value('timestamp', $row->timestamp),
	    );
            $this->template->load('template','disposisi/sipadu_disposisi_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('disposisi'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_disposisi', TRUE));
        } else {
            $data = array(
		'id_surat_masuk' => $this->input->post('id_surat_masuk',TRUE),
		'nip_asal' => $this->input->post('nip_asal',TRUE),
		'unit_kerja_asal' => $this->input->post('unit_kerja_asal',TRUE),
		'nip_tujuan' => $this->input->post('nip_tujuan',TRUE),
		'unit_kerja_tujuan' => $this->input->post('unit_kerja_tujuan',TRUE),
		'catatan' => $this->input->post('catatan',TRUE),
		'timestamp' => $this->input->post('timestamp',TRUE),
	    );

            $this->Disposisi_model->update($this->input->post('id_disposisi', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('disposisi'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Disposisi_model->get_by_id($id);

        if ($row) {
            $this->Disposisi_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('disposisi'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('disposisi'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_surat_masuk', 'id surat masuk', 'trim|required');
	$this->form_validation->set_rules('nip_asal', 'nip asal', 'trim|required');
	$this->form_validation->set_rules('unit_kerja_asal', 'unit kerja asal', 'trim|required');
	$this->form_validation->set_rules('nip_tujuan', 'nip tujuan', 'trim|required');
	$this->form_validation->set_rules('unit_kerja_tujuan', 'unit kerja tujuan', 'trim|required');
	$this->form_validation->set_rules('catatan', 'catatan', 'trim|required');
	$this->form_validation->set_rules('timestamp', 'timestamp', 'trim|required');

	$this->form_validation->set_rules('id_disposisi', 'id_disposisi', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Disposisi.php */
/* Location: ./application/controllers/Disposisi.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2021-05-04 10:22:00 */
/* http://harviacode.com */