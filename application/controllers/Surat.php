<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Surat extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }
        $this->load->model('surat_model');
		$this->load->model('master_unit_kerja_model');

	}

	public function index()
	{
		$data['surat'] = $this->surat_model->get_all();
		$data['unit_kerja'] = $this->master_unit_kerja_model->get_all();

		$this->load->vars($data);
		$this->template->load('template/template', 'surat/list');
	}

	// format RB: B-001/RB/BPS/1100/04/2021
	// format umum: B.001/BPS/11560/4/2021 ???

	public function tambah_surat()
	{
		$this->load->library('form_validation');
        $this->form_validation->set_rules('kategori', 'kategori', 'required');


        if ($this->form_validation->run()) {
			$kategori= $this->input->post('kategori');
			$unit_kerja= $this->input->post('unit_kerja');
			$tanggal= $this->input->post('tanggal');
			$tanggal= date('Y-m-d', strtotime($tanggal));
			$perihal= $this->input->post('perihal');
			$tujuan= $this->input->post('tujuan');
			if ($kategori == "RB") {
				$surat = $this->surat_rb($tanggal, $perihal);
			} else {
				$surat = $this->surat_biasa($tanggal, $perihal, $unit_kerja, $kategori);
			}
			$surat['tujuan'] = $tujuan;
			$this->surat_model->add($surat);

			$this->session->set_flashdata('sukses', "Data berhasil ditambahkan");
			$this->session->set_flashdata('no_surat_baru', $surat['no_surat']);
            $this->load->library('user_agent');
            redirect($this->agent->referrer());
		} else {
			$data['surat'] = $this->surat_model->get_all();
			$data['unit_kerja'] = $this->master_unit_kerja_model->get_all();

			$this->session->set_flashdata('gagal', "Gagal membuat no surat");
            $this->load->library('user_agent');
            redirect($this->agent->referrer());

		}

		
		
		$data= null;
		$this->load->vars($data);
		$this->template->load('template/template', 'surat/list');
	}

	public function surat_rb($tanggal, $perihal)
	{
		// format RB: B-001/RB/BPS/1100/04/2021
		$surat_sebelum = $this->db->select('*')->from('sipadu_surat')->where('tanggal <=',$tanggal)->where('kategori','RB')->order_by('id_surat','desc')->get()->row_array(); //ambil surat terakhir sebelum/sama dengan tanggal tersebut
		$no_surat_sebelum_sama = $this->db->select('*')->from('sipadu_surat')->where('kategori','RB')->where('no',$surat_sebelum['no'])->order_by('id_surat','desc')->get()->row_array();//ambil surat terakhir setelah tanggal tersebut
		$surat_sesudah = $this->db->select('*')->from('sipadu_surat')->where('kategori','RB')->where('no',$surat_sebelum['no']+1)->order_by('id_surat','desc')->get()->row_array();//ambil surat terakhir setelah tanggal tersebut
		if (!empty($surat_sesudah)) { 
			$no_sebelum = $surat_sebelum['no'];
			$no_baru = $no_sebelum;
			if (empty($no_surat_sebelum_sama['sub_no'])) {
				//ascii A = 65
				$sub_no = 65;
			} else {
				$sub_no = $no_surat_sebelum_sama['sub_no'] + 1;
			}
			$bulan = date('m', strtotime($tanggal));
			$tahun = date('Y', strtotime($tanggal));
			$unit_kerja = 1100;
			$kategori = "RB";
			$awalan = "B-";
			$no_surat= $awalan.sprintf("%03d",$no_baru).chr($sub_no)."/".$kategori."/"."BPS"."/".$unit_kerja."/".sprintf("%02d",$bulan)."/".$tahun;
			$surat = array(
				'bulan' => $bulan,
				'tahun' => $tahun,
				'no' => $no_baru,
				'unit_kerja' => $unit_kerja,
				'kategori' => $kategori, 
				'no_surat' => $no_surat,
				'sub_no' => $sub_no,
				'perihal' => $perihal,
				'awalan' => $awalan,
				'bps' => "BPS",
				'nip' => $this->session->userdata('nip_pegawai'),
				'tanggal' => $tanggal,
				);
			return $surat;
		} else {
			$no_sebelum = $surat_sebelum['no'];
			$no_baru = $no_sebelum + 1;
			
			$bulan = date('m', strtotime($tanggal));
			$tahun = date('Y', strtotime($tanggal));
			$unit_kerja = 1100;
			$kategori = "RB";
			$awalan = "B-";
			$no_surat= $awalan.sprintf("%03d",$no_baru)."/".$kategori."/"."BPS"."/".$unit_kerja."/".sprintf("%02d",$bulan)."/".$tahun;
			$surat = array(
				'bulan' => $bulan,
				'tahun' => $tahun,
				'no' => $no_baru,
				'unit_kerja' => $unit_kerja,
				'kategori' => $kategori, 
				'no_surat' => $no_surat,
				// 'sub_no' => NULL,
				'perihal' => $perihal,
				'awalan' => $awalan,
				'bps' => "BPS",
				'nip' => $this->session->userdata('nip_pegawai'),
				'tanggal' => $tanggal,
				);
			return $surat;
		}
	}

	public function surat_biasa($tanggal, $perihal, $unit_kerja, $kategori)
	{
		// format RB: B-001/RB/BPS/1100/04/2021
		
		$surat_sebelum = $this->db->select('*')->from('sipadu_surat')->where('tanggal <=',$tanggal)->where('kategori !=','RB')->like('unit_kerja',substr($unit_kerja,0,4))->order_by('id_surat','desc')->get()->row_array(); //ambil surat terakhir sebelum/sama dengan tanggal tersebut
		$no_surat_sebelum_sama = $this->db->select('*')->from('sipadu_surat')->where('kategori !=','RB')->like('unit_kerja',substr($unit_kerja,0,4))->where('no',$surat_sebelum['no'])->order_by('id_surat','desc')->get()->row_array();//ambil surat terakhir setelah tanggal tersebut
		$surat_sesudah = $this->db->select('*')->from('sipadu_surat')->where('kategori !=','RB')->like('unit_kerja',substr($unit_kerja,0,4))->where('no',$surat_sebelum['no']+1)->order_by('id_surat','desc')->get()->row_array();//ambil surat terakhir setelah tanggal tersebut
		if (!empty($surat_sesudah)) { 
			$no_sebelum = $surat_sebelum['no'];
			$no_baru = $no_sebelum;
			if (empty($no_surat_sebelum_sama['sub_no'])) {
				//ascii A = 65
				$sub_no = 65;
			} else {
				$sub_no = $no_surat_sebelum_sama['sub_no'] + 1;
			}
			$bulan = date('m', strtotime($tanggal));
			$tahun = date('Y', strtotime($tanggal));
			
			$awalan = "B-";
			$no_surat= $awalan.sprintf("%03d",$no_baru).chr($sub_no)."/"."BPS"."/".$unit_kerja."/".sprintf("%02d",$bulan)."/".$tahun;
			$surat = array(
				'bulan' => $bulan,
				'tahun' => $tahun,
				'no' => $no_baru,
				'unit_kerja' => $unit_kerja,
				'kategori' => $kategori, 
				'no_surat' => $no_surat,
				'sub_no' => $sub_no,
				'perihal' => $perihal,
				'awalan' => $awalan,
				'bps' => "BPS",
				'nip' => $this->session->userdata('nip_pegawai'),
				'tanggal' => $tanggal,
				);
			return $surat;
		} else {
			$no_sebelum = $surat_sebelum['no'];
			$no_baru = $no_sebelum + 1;
			
			$bulan = date('m', strtotime($tanggal));
			$tahun = date('Y', strtotime($tanggal));
			
			$awalan = "B-";
			$no_surat= $awalan.sprintf("%03d",$no_baru)."/"."BPS"."/".$unit_kerja."/".sprintf("%02d",$bulan)."/".$tahun;
			$surat = array(
				'bulan' => $bulan,
				'tahun' => $tahun,
				'no' => $no_baru,
				'unit_kerja' => $unit_kerja,
				'kategori' => $kategori, 
				'no_surat' => $no_surat,
				// 'sub_no' => NULL,
				'perihal' => $perihal,
				'awalan' => $awalan,
				'bps' => "BPS",
				'nip' => $this->session->userdata('nip_pegawai'),
				'tanggal' => $tanggal,
				);
			return $surat;
		}
	}

	public function Upload()
    {

        $this->load->library('upload');
        
        $config['upload_path'] = './upload_file/surat/'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '100000'; //maksimum besar file 2M

        $this->upload->initialize($config);

        // $files = $_FILES['template'];
        // $this->upload->do_upload('template');

        if ($this->upload->do_upload('file_surat')) {
                $gbr[] = $this->upload->data();
                $nama_file= $gbr['0']['file_name'];
                // print_r($gbr);
            } else {
                echo 'gagal upload file';
                echo  $this->upload->display_errors('<p>', '</p>');
				$this->session->set_flashdata('gagal', "Gagal Upload Surat: ".$this->upload->display_errors('<p>', '</p>'));
            	$this->load->library('user_agent');
            	redirect($this->agent->referrer());
                //return false;
            }

		$id_surat = $this->input->post('id_surat');

		$params = array(
		'file_surat' =>$nama_file ,
		'tanggal_upload' => date('Y-m-d H:i:s'),
		);

		
		$update = $this->surat_model->update($id_surat, $params);
		if ($update['error']) { //jika gagal
			$this->session->set_flashdata('gagal', "Gagal Upload Surat");
            $this->load->library('user_agent');
            redirect($this->agent->referrer());
		} else {
			$this->session->set_flashdata('sukses', "Berhasil Upload Surat");
            $this->load->library('user_agent');
            redirect($this->agent->referrer());
		}
        
    }

	public function download($id_surat, $file_surat) { 
		
        //load the download helper
		$this->load->helper('download');
		$params = array(
			'id_surat' =>$id_surat,
			'nip'=> $this->session->userdata('nip_pegawai'));
		$this->surat_model->add_download($params);
		//$this->transaksi_model->update_read($id_transaksi);

		header("Content-Type:  application/pdf ");
		force_download("upload_file/surat/".$file_surat, NULL);
	}
	
	public function masuk()
	{
		
	}

	
}