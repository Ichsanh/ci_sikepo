<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Butir_fungsional extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }
        $this->load->model('master_butir_fungsional_model');
        $this->load->model('master_jenis_jabatan_fungsional_model');
        $this->load->model('master_bidang_model');
    }
    public function index()
    {
        // $butir_fungsional = $this->master_butir_fungsional_model->get_all();
        $jenis_jabatan_fungsional = $this->master_jenis_jabatan_fungsional_model->get_all();

        // $data['butir_fungsional'] = $butir_fungsional;
        $data['jenis_jabatan_fungsional'] = $jenis_jabatan_fungsional;

        $this->load->vars($data);
        $this->template->load('template/template', 'butir_fungsional/list');
    }

    public function ajax_list() //untuk ajax datatable server side
    {
        // $list = $this->master_pekerjaan_model->get_datatables($jenis_pekerjaan);
        $list = $this->master_pekerjaan_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $isi) {
            $no++;

            $row = array();
            $row[] = $no;
            $row[] = $isi->jenis_pekerjaan;
            $row[] = $isi->rincian_pekerjaan;
            $row[] = $isi->keterangan_pekerjaan;
            if ($isi->status_approval == 0) {
                $row[] = '<td><center><span class="badge badge-info">' . $isi->keterangan_approval . '</span> </td>';
            }
            if ($isi->status_approval == 1) {
                $row[] = '<td><center><span class="badge badge-success">' . $isi->keterangan_approval . '</span> </td>';
            }
            if ($isi->status_approval == 2) {
                $row[] = '<td><center><span class="badge badge-danger">' . $isi->keterangan_approval . '</span> </td>';
            }
            // $row[] = $isi->status_approval;
            $row[] = $isi->pegawai_created;
            $row[] = $isi->pegawai_approval;
            if ($this->session->userdata('level_user') <> 'user' and $this->session->userdata('level_user') <> 'pejabat') {
                $row[] = '<a onclick="return confirm_approve()" href="' . base_url('pekerjaan/approve/') . $isi->id_master_pekerjaan . '"> <span class="badge badge-success"><i class="fa fa-check-circle"></i> approve</span></a> <a  ></a>    
            <a onclick="return confirm_reject()"
            href="' . base_url('pekerjaan/reject/') . $isi->id_master_pekerjaan . '" > <span class="badge badge-danger"><i class="fa fa-close"></i> reject </span> </a>';
            }

            // $row[] = '<ahref="' . site_url('indikator/view/') . $isi->mfd . '/' . $isi->tipe . '/' . $isi->id_api . '/' . $isi->kode_kelompok . '">'. $isi->judul . '</ahref=>';

            // $row[] = '<a value="'.$data->id_data.'" data-toggle="modal" data-target="#modal_aksi"data-id_data="'.$data->id_data.'" data-flag="'.$data->flag.'" data-file="'.$data->file.'"><button class="btn btn-danger btn-sm">Aksi</button></a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->master_pekerjaan_model->count_all(),
            "recordsFiltered" => $this->master_pekerjaan_model->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function lihat_butir()
    {
        $kode_jenis_jabatan_fungsional = $this->input->get('kode_jenis_jabatan_fungsional');
        $butir_fungsional = $this->master_butir_fungsional_model->get_fungsional($kode_jenis_jabatan_fungsional);
        $jenis_jabatan_fungsional_terpilih = $this->master_jenis_jabatan_fungsional_model->get_id($kode_jenis_jabatan_fungsional);
        $jenis_jabatan_fungsional = $this->master_jenis_jabatan_fungsional_model->get_all();

        $data['jenis_jabatan_fungsional'] = $jenis_jabatan_fungsional;
        $data['jenis_jabatan_fungsional_terpilih'] = $jenis_jabatan_fungsional_terpilih;
        $data['butir_fungsional'] = $butir_fungsional;

        $this->load->vars($data);
        $this->template->load('template/template', 'butir_fungsional/list');
    }

    public function download_template()
    {
        $this->load->helper('download');
        force_download('kumpulan_template/template_master_butir_fungsional.xlsx',NULL);
    }

    public function Upload()
    {

        $this->load->library('upload');
        $new_name = 'fungsional_'.$this->session->userdata('nip_pegawai').'_'.$_FILES["template"]['name'] ;
        $config['file_name'] = $new_name;
        $config['upload_path'] = './upload_file/'; //path folder
        $config['allowed_types'] = 'xlsx'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '50000'; //maksimum besar file 2M

        $this->upload->initialize($config);

        // $files = $_FILES['template'];
        // $this->upload->do_upload('template');

        if ($this->upload->do_upload('template')) {
                $gbr[] = $this->upload->data();
                $nama_file= $gbr['0']['file_name'];
                // print_r($gbr);
            } else {
                echo 'gagal upload file';
                echo  $this->upload->display_errors('<p>', '</p>');
                //return false;
            }

        $inputFileName = "upload_file/".$nama_file;

        /**  Identify the type of $inputFileName  **/
        // $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

        // /**  Create a new Reader of the type that has been identified  **/
        // $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        // /**  Load $inputFileName to a Spreadsheet Object  **/
        // $spreadsheet = $reader->load($inputFileName);

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $data = $spreadsheet->getActiveSheet()->toArray();
        // print_r($data);
        unset($data[0]);
        foreach ($data as $key => $value) {
            if (is_null($value['0']) ) {
                break;
            }
            $params = array(
                'kode_jenis_jabatan_fungsional' => $value['0'],
                'nama_jenis_jabatan_fungsional' =>  $value['1'],
                'butir_fungsional_grouping' => $value['2'],
                'butir_fungsional' => $value['3'],
                'pekerjaan_rincian' => $value['4'],
                'pekerjaan_volume' => $value['5'],
                'pekerjaan_pelaksana' => $value['6'],
                'ak' => $value['8'],
                'filter'=> 1,
            );

            try {
                $this->master_butir_fungsional_model->tambah_dummy($params);
                
                
            } catch (Exception $error) {
                echo 'ERROR:'.$error->getMessage();
            }
            
        }
        $data_duplikat = $this->master_butir_fungsional_model->cek_duplikat();
        $this->db->query('TRUNCATE TABLE dummy_master_butir_fungsional');
        if (empty($data_duplikat)) {
            $this->session->set_flashdata('sukses', "Data berhasil ditambahkan");
            $this->load->library('user_agent');
            redirect($this->agent->referrer());
        } else {
            $this->download_error_upload($data_duplikat);
        }
        
    }

    public function download_error_upload($data_duplikat)
    {
        
        $spreadsheet2 = new Spreadsheet();
        
        // $spreadsheet->getDefaultStyle()
        //     ->getFont()
        //     ->setName('Segoe UI')
        //     ->setSize(11);


        $sheet = $spreadsheet2->getActiveSheet();
        
        $sheet->setCellValue('A1', 'kode_jenis_jabatan_fungsional');
        $sheet->setCellValue('B1', 'nama_jenis_jabatan_fungsional');
        $sheet->setCellValue('C1', 'butir_fungsional_grouping');
        $sheet->setCellValue('D1', 'butir_fungsional');
        $sheet->setCellValue('E1', 'pekerjaan_rincian');
        $sheet->setCellValue('F1', 'pekerjaan_volume');
        $sheet->setCellValue('G1', 'pekerjaan_pelaksana');
        $sheet->setCellValue('H1', 'ak');
        $sheet->setCellValue('I1', 'status');
        
        foreach ($data_duplikat as $key => $value) {
            
            $sheet->setCellValue('A'.($key+2), $value['kode_jenis_jabatan_fungsional']);
            $sheet->setCellValue('B'.($key+2), $value['nama_jenis_jabatan_fungsional']);
            $sheet->setCellValue('C'.($key+2), $value['butir_fungsional_grouping']);
            $sheet->setCellValue('D'.($key+2), $value['butir_fungsional']);
            $sheet->setCellValue('E'.($key+2), $value['pekerjaan_rincian']);
            $sheet->setCellValue('F'.($key+2), $value['pekerjaan_volume']);
            $sheet->setCellValue('G'.($key+2), $value['pekerjaan_pelaksana']);
            $sheet->setCellValue('H'.($key+2), $value['ak']);
            $sheet->setCellValue('I'.($key+2), 'Duplikat');
            
        }

        $styleGaris = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
        ];
        $sheet->getStyle('A1:E'.(count($data_duplikat)+1))->applyFromArray($styleGaris);

        $writer = new Xlsx($spreadsheet2);
        
        $filename = "error upload master butir fungsional";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}