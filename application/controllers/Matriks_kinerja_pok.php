<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Matriks_kinerja_pok extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
        } else {
            redirect('login');
        }

        $this->load->model('pegawai_model');
        $this->load->model('kegiatan_model');
        $this->load->model('master_bidang_model');

        $this->load->model('master_pekerjaan_model');
        $this->load->model('waktu_deskripsi_model');
        $this->load->model('master_pok_model');
        $this->load->model('matriks_kinerja_model');
    }
    public function index()
    {
        // $matriks_kinerja = $this->matriks_kinerja_model->get_matriks_kinerja();
        $bidang = $this->master_bidang_model->get_all();
        $data['bidang'] = $bidang;
        $data['bidang_terpilih'] = null;
        $data['matriks_kinerja'] = null;
        $this->load->vars($data);
        $this->template->load('template/template', 'matriks_kinerja_pok/list');
    }

    public function lihat_matriks_kinerja()
    {

        $kode_bidang = $this->input->get('kode_bidang');
        $id_pok = $this->input->get('id_pok');
        $matriks_kinerja_pok = $this->matriks_kinerja_model->get_matriks_kinerja_pok($kode_bidang, $id_pok);
        $bidang_terpilih = $this->master_bidang_model->get_id($kode_bidang);
        $bidang = $this->master_bidang_model->get_all();
        $data['bidang'] = $bidang;
        $data['bidang_terpilih'] = $bidang_terpilih;
        $data['pok_terpilih'] = $this->master_pok_model->get_id($id_pok);
        $data['pok'] = $this->master_pok_model->get_pok_bidang($kode_bidang);
        $data['matriks_kinerja_pok'] = $matriks_kinerja_pok;
        $this->load->vars($data);
        $this->template->load('template/template', 'matriks_kinerja_pok/list');
    }

    public function detail_pekerjaan($id_matriks)
    {
        $detail_pekerjaan = $this->matriks_kinerja_model->get_detail_pekerjaan($id_matriks);
        $target_pekerjaan = $this->matriks_kinerja_model->get_target_pekerjaan($id_matriks);
        $total_target = $this->matriks_kinerja_model->get_total_target($id_matriks);
        $jumlah_alokasi = $this->matriks_kinerja_model->get_jumlah_alokasi($id_matriks);

        $data['target_pekerjaan'] = $target_pekerjaan;
        $data['detail_pekerjaan'] = $detail_pekerjaan;
        $data['total_target'] = $total_target;
        $data['jumlah_alokasi'] = $jumlah_alokasi;
        $this->load->vars($data);
        $this->template->load('template/template', 'matriks_kinerja_pok/detail_pekerjaan');
    }

    public function edit_kinerja_individu($id_matriks, $id_skp)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nip_pegawai', 'nip pegawai', 'required');

        if ($this->form_validation->run()) {

            $params = array(

                'nip_pegawai' => $this->input->post('nip_pegawai'),
                'keterangan' => $this->input->post('keterangan'),
                'target' => $this->input->post('target'),

            );

            $update = $this->matriks_kinerja_model->edit_kinerja_individu($id_skp, $params);
            $this->session->set_flashdata('sukses', "Data berhasil diupdate");
            $this->load->library('user_agent');

            redirect('matriks_kinerja_pok/detail_pekerjaan/' . $id_matriks);
        } else {
            $detail_pekerjaan = $this->matriks_kinerja_model->get_detail_pekerjaan($id_matriks);
            $kinerja_individu = $this->matriks_kinerja_model->get_kinerja_individu($id_skp);
            $pegawai = $this->pegawai_model->get_all();

            $data['detail_pekerjaan'] = $detail_pekerjaan;
            $data['kinerja_individu'] = $kinerja_individu;
            $data['pegawai'] = $pegawai;
            $this->load->vars($data);
            $this->template->load('template/template', 'matriks_kinerja_pok/edit_kinerja_individu');
        }
    }

    public function alokasi_pegawai_tahunan($id_matriks)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nip_pegawai', 'nip pegawai', 'required');

        if ($this->form_validation->run()) {


            $params = array(

                'nip_pegawai' => $this->input->post('nip_pegawai'),
                'keterangan' => $this->input->post('keterangan'),
                // 'target' => $target,
                'id_matriks' => $id_matriks,
                'kode_jabatan_fungsional' => $this->session->userdata('kode_jabatan_fungsional'),
                'tahun' => $this->session->userdata('tahun_anggaran'),

            );

            if (!empty($this->input->post("target_bulan_01"))) {
                $params['target'] = $this->input->post("target_bulan_01");
                $params['bulan'] = 1;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_02"))) {
                $params['target'] = $this->input->post("target_bulan_02");
                $params['bulan'] = 2;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_03"))) {
                $params['target'] = $this->input->post("target_bulan_03");
                $params['bulan'] = 3;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_04"))) {
                $params['target'] = $this->input->post("target_bulan_04");
                $params['bulan'] = 4;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_05"))) {
                $params['target'] = $this->input->post("target_bulan_05");
                $params['bulan'] = 5;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_06"))) {
                $params['target'] = $this->input->post("target_bulan_06");
                $params['bulan'] = 6;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_07"))) {
                $params['target'] = $this->input->post("target_bulan_07");
                $params['bulan'] = 7;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_08"))) {
                $params['target'] = $this->input->post("target_bulan_08");
                $params['bulan'] = 8;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_09"))) {
                $params['target'] = $this->input->post("target_bulan_09");
                $params['bulan'] = 9;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_10"))) {
                $params['target'] = $this->input->post("target_bulan_10");
                $params['bulan'] = 10;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_11"))) {
                $params['target'] = $this->input->post("target_bulan_11");
                $params['bulan'] = 11;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }
            if (!empty($this->input->post("target_bulan_12"))) {
                $params['target'] = $this->input->post("target_bulan_12");
                $params['bulan'] = 12;
                $tambah = $this->matriks_kinerja_model->tambah_kinerja_individu($params);
            }

            $this->session->set_flashdata('sukses', "Data berhasil diupdate");
            $this->load->library('user_agent');

            redirect('matriks_kinerja_pok/detail_pekerjaan/' . $id_matriks);
        } else {
            $detail_pekerjaan = $this->matriks_kinerja_model->get_detail_pekerjaan($id_matriks);
            $pegawai = $this->pegawai_model->get_all();

            $data['detail_pekerjaan'] = $detail_pekerjaan;
            $data['pegawai'] = $pegawai;
            $this->load->vars($data);
            $this->template->load('template/template', 'matriks_kinerja_pok/alokasi_pegawai_tahunan');
        }
    }

    public function tambah_pekerjaan($kode_pok)
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_master_pekerjaan', 'nama id_master_pekerjaan', 'required');


        if ($this->form_validation->run()) {

            $params = array(
                'tahun' => $this->session->userdata('tahun_anggaran'),
                'id_master_pekerjaan' => $this->input->post("id_master_pekerjaan"),
                'id_pok' => $kode_pok,
                'target_bulan_01' => $this->input->post("target_bulan_01"),
                'target_bulan_02' => $this->input->post("target_bulan_02"),
                'target_bulan_03' => $this->input->post("target_bulan_03"),
                'target_bulan_04' => $this->input->post("target_bulan_04"),
                'target_bulan_05' => $this->input->post("target_bulan_05"),
                'target_bulan_06' => $this->input->post("target_bulan_06"),
                'target_bulan_07' => $this->input->post("target_bulan_07"),
                'target_bulan_08' => $this->input->post("target_bulan_08"),
                'target_bulan_09' => $this->input->post("target_bulan_09"),
                'target_bulan_10' => $this->input->post("target_bulan_10"),
                'target_bulan_11' => $this->input->post("target_bulan_11"),
                'target_bulan_12' => $this->input->post("target_bulan_12"),
                'satuan' => $this->input->post("satuan"),
                'waktu' => $this->input->post("waktu"),
                'id_waktu_deskripsi' => $this->input->post("id_waktu_deskripsi"),
                'flag_jenis_matriks' => 1,
                'id_kegiatan' => $this->input->post("id_kegiatan"),

            );

            $insert = $this->matriks_kinerja_model->tambah($params);
            $this->session->set_flashdata('sukses', "Data berhasil ditambahkan");

            redirect('matriks_kinerja_pok');
        } else {

            $data['kegiatan'] = $this->kegiatan_model->get_kegiatan_pok($kode_pok);
            $data['jenis_pekerjaan'] = $this->master_pekerjaan_model->get_all_jenis_pekerjaan();
            $data['pekerjaan'] = $this->master_pekerjaan_model->get_all();
            $data['waktu_deskripsi'] = $this->waktu_deskripsi_model->get_all();
            $data['kode_pok'] = $kode_pok;
            $this->load->vars($data);
            $this->template->load('template/template', 'matriks_kinerja_pok/tambah_pekerjaan');
        }
    }
    public function download_template()
    {
        $this->load->helper('download');
        force_download('kumpulan_template/template_tabel_matriks_kinerja.xlsx',NULL);
    }

    public function Upload()
    {

        $this->load->library('upload');
        $new_name = 'fungsional_'.$this->session->userdata('nip_pegawai').'_'.$_FILES["template"]['name'] ;
        $config['file_name'] = $new_name;
        $config['upload_path'] = './upload_file/'; //path folder
        $config['allowed_types'] = 'xlsx'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '50000'; //maksimum besar file 2M

        $this->upload->initialize($config);

        // $files = $_FILES['template'];
        // $this->upload->do_upload('template');

        if ($this->upload->do_upload('template')) {
                $gbr[] = $this->upload->data();
                $nama_file= $gbr['0']['file_name'];
                // print_r($gbr);
            } else {
                echo 'gagal upload file';
                echo  $this->upload->display_errors('<p>', '</p>');
                //return false;
            }

        $inputFileName = "upload_file/".$nama_file;

        /**  Identify the type of $inputFileName  **/
        // $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

        // /**  Create a new Reader of the type that has been identified  **/
        // $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);

        // /**  Load $inputFileName to a Spreadsheet Object  **/
        // $spreadsheet = $reader->load($inputFileName);

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($inputFileName);

        /**  Convert Spreadsheet Object to an Array for ease of use  **/
        $data = $spreadsheet->getActiveSheet()->toArray();
        // print_r($data);
        unset($data[0]);
        foreach ($data as $key => $value) {
            if (is_null($value['0']) ) {
                break;
            }
            $params = array(
                
                'id_pok' => $value['0'],
                'pok' => $value['1'],
                'id_kegiatan' => $value['2'],
                'kegiatan' => $value['3'],
                'id_master_pekerjaan' => $value['4'], //id_master_pekerjaan == id_master_uraian
                'uraian_kegiatan' => $value['5'],
                'satuan' => $value['6'],
                'waktu' => $value['7'],
                'tahun' => $value['8'],
                'id_waktu_deskripsi' => $value['9'],
                'waktu_deskripsi' => $value['10'],
                'biaya' => $value['11'],
                'jenis_fungsional' => $value['12'],
                'nama_jenis_jabatan_fungsional' => $value['13'],
                'target_bulan_01' => $value['14'],
                'target_bulan_02' => $value['15'],
                'target_bulan_03' => $value['16'],
                'target_bulan_04' => $value['17'],
                'target_bulan_05' => $value['18'],
                'target_bulan_06' => $value['19'],
                'target_bulan_07' => $value['20'],
                'target_bulan_08' => $value['21'],
                'target_bulan_09' => $value['22'],
                'target_bulan_10' => $value['23'],
                'target_bulan_11' => $value['24'],
                'target_bulan_12' => $value['25'],

            );

            try {
                $this->matriks_kinerja_model->tambah_dummy($params);
                
                
            } catch (Exception $error) {
                echo 'ERROR:'.$error->getMessage();
            }
            
        }
        $data_duplikat = $this->matriks_kinerja_model->cek_duplikat();
        $this->db->query('TRUNCATE TABLE dummy_tabel_matriks_kinerja');
        if (empty($data_duplikat)) {
            $this->session->set_flashdata('sukses', "Data berhasil ditambahkan");
            $this->load->library('user_agent');
            redirect($this->agent->referrer());
        } else {
            $this->download_error_upload($data_duplikat);
        }
        
    }

    public function download_error_upload($data_duplikat)
    {
        
        $spreadsheet2 = new Spreadsheet();
        
        // $spreadsheet->getDefaultStyle()
        //     ->getFont()
        //     ->setName('Segoe UI')
        //     ->setSize(11);


        $sheet = $spreadsheet2->getActiveSheet();

        $sheet->setCellValue('A1','id_pok');
        $sheet->setCellValue('B1','pok');
        $sheet->setCellValue('C1','id_kegiatan');
        $sheet->setCellValue('D1','kegiatan');
        $sheet->setCellValue('E1','id_master_uraian');
        $sheet->setCellValue('F1','uraian_kegiatan');
        $sheet->setCellValue('G1','satuan');
        $sheet->setCellValue('H1','tahun');
        $sheet->setCellValue('I1','waktu');
        $sheet->setCellValue('J1','id_waktu_deskripsi');
        $sheet->setCellValue('K1','waktu_deskripsi');
        $sheet->setCellValue('L1','biaya');
        $sheet->setCellValue('M1','jenis_fungsional');
        $sheet->setCellValue('N1','nama_jenis_jabatan_fungsional');
        $sheet->setCellValue('O1','target_bulan_01');
        $sheet->setCellValue('P1','target_bulan_02');
        $sheet->setCellValue('Q1','target_bulan_03');
        $sheet->setCellValue('R1','target_bulan_04');
        $sheet->setCellValue('S1','target_bulan_05');
        $sheet->setCellValue('T1','target_bulan_06');
        $sheet->setCellValue('U1','target_bulan_07');
        $sheet->setCellValue('V1','target_bulan_08');
        $sheet->setCellValue('W1','target_bulan_09');
        $sheet->setCellValue('X1','target_bulan_10');
        $sheet->setCellValue('Y1','target_bulan_11');
        $sheet->setCellValue('Z1','target_bulan_12');
        $sheet->setCellValue('AA1', 'status');

        
        foreach ($data_duplikat as $key => $value) {
            
            $sheet->setCellValue('A'.($key+2), $value['id_pok']);
            $sheet->setCellValue('B'.($key+2), $value['pok']);
            $sheet->setCellValue('C'.($key+2), $value['id_kegiatan']);
            $sheet->setCellValue('D'.($key+2), $value['kegiatan']);
            $sheet->setCellValue('E'.($key+2), $value['id_master_pekerjaan']);
            $sheet->setCellValue('F'.($key+2), $value['uraian_kegiatan']);
            $sheet->setCellValue('G'.($key+2), $value['satuan']);
            $sheet->setCellValue('H'.($key+2), $value['tahun']);
            $sheet->setCellValue('I'.($key+2), $value['waktu']);
            $sheet->setCellValue('J'.($key+2), $value['id_waktu_deskripsi']);
            $sheet->setCellValue('K'.($key+2), $value['waktu_deskripsi']);
            $sheet->setCellValue('L'.($key+2), $value['biaya']);
            $sheet->setCellValue('M'.($key+2), $value['jenis_fungsional']);
            $sheet->setCellValue('N'.($key+2), $value['nama_jenis_jabatan_fungsional']);
            $sheet->setCellValue('O'.($key+2), $value['target_bulan_01']);
            $sheet->setCellValue('P'.($key+2), $value['target_bulan_02']);
            $sheet->setCellValue('Q'.($key+2), $value['target_bulan_03']);
            $sheet->setCellValue('R'.($key+2), $value['target_bulan_04']);
            $sheet->setCellValue('S'.($key+2), $value['target_bulan_05']);
            $sheet->setCellValue('T'.($key+2), $value['target_bulan_06']);
            $sheet->setCellValue('U'.($key+2), $value['target_bulan_07']);
            $sheet->setCellValue('V'.($key+2), $value['target_bulan_08']);
            $sheet->setCellValue('W'.($key+2), $value['target_bulan_09']);
            $sheet->setCellValue('X'.($key+2), $value['target_bulan_10']);
            $sheet->setCellValue('Y'.($key+2), $value['target_bulan_11']);
            $sheet->setCellValue('Z'.($key+2), $value['target_bulan_12']);
            $sheet->setCellValue('AA'.($key+2), 'Duplikat');
            
        }

        $styleGaris = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    // 'color' => ['argb' => 'FFFF0000'],
                ],
            ],
        ];
        $sheet->getStyle('A1:AA'.(count($data_duplikat)+1))->applyFromArray($styleGaris);

        $writer = new Xlsx($spreadsheet2);
        
        $filename = "error upload matriks kinerja pok";

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }
}