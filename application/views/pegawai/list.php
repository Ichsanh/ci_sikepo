<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        if ($this->session->flashdata('sukses') <> '') {
        ?>
            <div class="alert alert-success alert-dismissible " role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <p><?php echo $this->session->flashdata('sukses'); ?></p>
            </div>
        <?php
        }
        ?>
        <?php
        if ($this->session->flashdata('gagal') <> '') {
        ?>
            <div class="alert alert-danger alert-dismissible " role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <p><?php echo $this->session->flashdata('gagal'); ?></p>
            </div>
        <?php
        }
        ?>
    </div>

</div>



<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>List Pegawai</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">


            <div class="col-sm-12">
                <!-- <div class="card-box table-responsive"> -->
                <div class="card-box ">
                    <p class="text-muted font-10 m-b-10">
                    </p>

                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%;">
                        <!-- <table class="table table-striped table-bordered" style="width:100%; height:10px;"> -->
                        <thead>
                            <tr style="background-color: #2a3f54; height:5px; color: #ffffff;">
                                <th width="1%">
                                    <center> ID
                                </th>
                                <th width="10%">
                                    <center> NAMA
                                </th>
                                <th width="1%">
                                    <center> PANGKAT
                                </th>
                                <th width="5%">
                                    <center> SATKER
                                </th>
                                <th width="15%">
                                    <center> UNIT KERJA
                                </th>
                                <th width="1%">
                                    <center> JABATAN STRUKTUR
                                </th>
                                <th width="1%">
                                    <center> JABATAN FUNGSIONAL
                                </th>
                                <th width="5%">
                                    <center> STATUS
                                </th>
                                <th width="1%">
                                    <center> AKSI
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php


                            foreach ($pegawai as $key => $q) {

                            ?>
                                <tr>
                                    <td>
                                        <center><?php echo $q['id_pegawai'] ?>
                                    </td>
                                    <td>
                                        <left><?php echo $q['nama_pegawai'] ?> (<?php echo $q['nip_pegawai'] ?>)
                                    </td>
                                    <td>
                                        <center><?php echo $q['pangkat'] ?>
                                    </td>
                                    <td>
                                        <center><?php echo $q['nama_satker'] ?>
                                    </td>
                                    <td>
                                        <center><?php echo $q['nama_unit_kerja'] ?>
                                    </td>
                                    <td>
                                        <center><?php echo $q['nama_jabatan_struktur'] ?>
                                    </td>
                                    <td>
                                        <center><?php echo $q['nama_jabatan_fungsional'] ?>
                                    </td>
                                    <td>
                                        <center><?php echo $q['status_pegawai'] ?>
                                    </td>

                                    <td>
                                        <center>
                                            <a title="UPDATE" href="pegawai/edit/<?php echo $q['id_pegawai']; ?>"><i class="fa fa-edit fa-lg"></i></a>
                                    </td>

                                </tr>
                            <?php } ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

        $('#custom-filter').appendTo(".dataTables_filter label");
        $('#custom-filter').on('change', function() {
            $('#datatable-buttons').DataTable().column(1).search(
                $('.fungsi').val(),
            ).draw();
        });

    });
</script>