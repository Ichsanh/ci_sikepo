<link href="<?= base_url(); ?>/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

<div class="row">

    <div class="x_panel">
        <div class="x_title">
            <h2>Form Edit Pegawai</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            <form action="" method="post" enctype="multipart/form-data" role="form">
                <input type="hidden" value="<?php echo $data['id_pegawai'] ?>" name="id_pegawai" />


                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NAMA PEGAWAI <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="nama_pegawai" name="nama_pegawai" required="" class="form-control " value="<?php echo $data['nama_pegawai']; ?>">
                    </div>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NIP<span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="nip_pegawai" name="nip_pegawai" required="" class="form-control " value="<?php echo $data['nip_pegawai']; ?>">
                    </div>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">PANGKAT <span class=""></span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                        <select class="form-control select2 select2-danger" name="pangkat" id="pangkat" data-dropdown-css-class="select2-danger" style="width: 100%;">
                            <option value="<?php echo $data['pangkat']; ?>"><?php echo $data['nama_pangkat']; ?></option>
                            <?php
                            foreach ($master_pangkat as $key => $value) {

                                echo "<option value='$value[gol_pangkat]'> $value[nama_pangkat] </option>";
                            } ?>
                        </select>
                    </div>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">SATKER <span class=""></span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                        <select class="form-control select2 select2-danger" name="kode_satker" id="kode_satker" data-dropdown-css-class="select2-danger" style="width: 100%;">
                            <option value="<?php echo $data['kode_satker']; ?>"><?php echo $data['nama_satker']; ?></option>
                            <?php
                            foreach ($master_satker as $key => $value) {
                            ?>
                                <option value='<?= $value['kode_satker']; ?>'> <?= $value['nama_satker']; ?> </option>";
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">UNIT KERJA <span class=""></span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                        <select class="form-control select2 select2-danger" name="kode_unit_kerja" id="kode_unit_kerja" data-dropdown-css-class="select2-danger" style="width: 100%;">
                            <option value="<?php echo $data['kode_unit_kerja']; ?>"><?php echo $data['nama_unit_kerja']; ?></option>

                            <?php
                            foreach ($master_unit_kerja as $key => $value) {
                            ?>
                                <option value='<?= $value['kode_unit_kerja']; ?>'> <?= $value['nama_unit_kerja']; ?> </option>";
                            <?php } ?>
                        </select>
                    </div>
                </div>


                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">JABATAN STRUKTUR <span class=""></span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                        <select class="form-control select2 select2-danger" name="kode_jabatan_struktur" id="kode_jabatan_struktur" data-dropdown-css-class="select2-danger" style="width: 100%;">
                            <option value="<?php echo $data['kode_jabatan_struktur']; ?>"><?php echo $data['nama_jabatan_struktur']; ?></option>

                            <?php
                            foreach ($master_jabatan_struktur as $key => $value) {
                            ?>
                                <option value='<?= $value['kode_jabatan_struktur']; ?>'> <?= $value['nama_jabatan_struktur']; ?> </option>";
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">JABATAN FUNGSIONAL <span class=""></span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                        <select class="form-control select2 select2-danger" name="kode_jabatan_fungsional" id="kode_jabatan_fungsional" data-dropdown-css-class="select2-danger" style="width: 100%;">
                            <option value="<?php echo $data['kode_jabatan_fungsional']; ?>"><?php echo $data['nama_jabatan_fungsional']; ?></option>

                            <?php
                            foreach ($master_jabatan_fungsional as $key => $value) {
                            ?>
                                <option value='<?= $value['kode_jabatan_fungsional']; ?>'> <?= $value['nama_jabatan_fungsional']; ?> </option>";
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="item form-group">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">STATUS PEGAWAI <span class=""></span>
                    </label>
                    <div class="col-md-6 col-sm-6 ">
                        <select class="form-control select2 select2-danger" name="status_pegawai" id="status_pegawai" data-dropdown-css-class="select2-danger" style="width: 100%;">
                            <option value="<?php echo $data['status_pegawai']; ?>"><?php echo $data['status_pegawai']; ?></option>
                            <option value="PNS">PNS</option>
                            <option value="CPNS">CPNS</option>
                            <option value="TUGAS BELAJAR">TUGAS BELAJAR</option>
                        </select>
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="item form-group">
                    <div class="col-md-6 col-sm-6 offset-md-3">
                        <a href='./?page=crud_master_pegawai' class="btn btn-danger"> <i class="fa fa-times"></i> Batal</a>
                        <button type="submit" name="simpan" class="btn btn-success">Submit</button>


                    </div>
                </div>

            </form>
        </div>
    </div>

</div>


<script src="<?= base_url(); ?>/vendors/select2/dist/js/select2.full.min.js"></script>
<script>
    $(".select2").select2();
</script>