<div class="x_panel">
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">


                <div class="item ">
                    <div class="col-md-2 col-sm-2 ">PEKERJAAN </div>
                    <div class="col-md-10 col-sm-10 ">: <?php echo $detail_pekerjaan['rincian_pekerjaan']; ?></div>
                </div>
                <div class="item ">
                    <div class="col-md-2 col-sm-2 ">KEGIATAN </div>
                    <div class="col-md-10 col-sm-10 ">: <?php echo $detail_pekerjaan['nama_kegiatan']; ?></div>
                </div>
                <div class="item ">
                    <div class="col-md-2 col-sm-2 ">BULAN</div>
                    <div class="col-md-10 col-sm-10 ">: <?php echo $kinerja_individu['bulan']; ?></div>
                </div>

                <div class="item ">
                    <div class="col-md-2 col-sm-2 "> SATUAN</div>
                    <div class="col-md-10 col-sm-10 ">: <?php echo $detail_pekerjaan['satuan']; ?></div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="x_panel">
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
                <form
                    action="<?=base_url('matriks_kinerja_pok/edit_kinerja_individu/').$kinerja_individu['id_matriks'].'/'.$kinerja_individu['id_skp'];?>"
                    method="post" enctype="multipart/form-data" role="form">


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NAMA PEGAWAI <span
                                class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <select class="form-control select2 select2-danger" name="nip_pegawai" id="nip_pegawai"
                                data-dropdown-css-class="select2-danger" style="width: 100%;">
                                <option value="<?php echo $kinerja_individu['nip_pegawai'] ?>">
                                    <?php echo $kinerja_individu['nama_pegawai'] ?>
                                </option>
                                <?php 
                                foreach ($pegawai as $key => $aa) {
                                    echo "<option value='$aa[nip_pegawai]'> $aa[nama_pegawai] </option>";}?>
                            </select>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            KETERANGAN <span class="required"></span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" id="keterangan" name="keterangan" class="form-control "
                                value="<?php echo $kinerja_individu['keterangan'] ?>">
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET <span class="required"></span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" id="target" name="target" class="form-control "
                                value="<?php echo $kinerja_individu['target'] ?>">
                        </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="item form-group">
                        <div class="col-md-6 col-sm-6 offset-md-3">
                            <button onclick="history.back(-1)" class="btn btn-danger"> <i class="fa fa-times"></i>
                                Batal</button>
                            <button type="submit" name="simpan" class="btn btn-success">Submit</button>


                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>