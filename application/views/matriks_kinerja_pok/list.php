<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        if ($this->session->flashdata('sukses') <> '') {
        ?>
        <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('sukses'); ?></p>
        </div>
        <?php
        }
        ?>
        <?php
        if ($this->session->flashdata('gagal') <> '') {
        ?>
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('gagal'); ?></p>
        </div>
        <?php
        }
        ?>
    </div>

</div>
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>Matriks Kinerja</h2>
            <ul class="nav navbar-right panel_toolbox">

                <a href="<?= base_url() ?>matriks_kinerja_pok/download_template">
                    <button class="btn btn-sm btn-block btn-success"><i class="fa fa-download"></i>
                        Template</button>
                </a>
                &nbsp;
                <a>
                    <button class="btn btn-sm btn-block btn-primary " data-toggle="modal" data-target=".modal_upload"><i
                            class="fa fa-upload"></i> Template</button>
                </a>

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <div class="col-lg-12 col-12">
                <div class="card card-grey">
                    <div class="card-body">
                        <div class="box-header">
                            <form action="<?= base_url() ?>matriks_kinerja_pok/lihat_matriks_kinerja" method="get"
                                role="form">
                                <div class="form-group row">


                                    <label class="col-sm-2 col-form-label">FUNGSI</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="kode_bidang"
                                            id="bidang" data-dropdown-css-class="select2-danger" style="width: 100%;">
                                            <?php
                                            if (empty($bidang_terpilih)) {
                                                echo '<option value="">Pilih Fungsi</option>';
                                            } else { ?>
                                            <option value="<?php echo $bidang_terpilih['kode_bidang']; ?>">
                                                <?php echo $bidang_terpilih['nama_bidang']; ?></option>
                                            <?php }
                                            ?>
                                            <?php
                                            foreach ($bidang as $key => $value) {
                                            ?>
                                            <option value="<?= $value['kode_bidang']; ?>">
                                                <?= $value['nama_bidang']; ?>
                                            </option>
                                            <?php

                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">POK</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="id_pok" id="id_pok"
                                            data-dropdown-css-class="select2-danger" style="width: 100%;">
                                            <?php
                                            if (empty($bidang_terpilih)) {
                                                echo '<option value="">Pilih POK</option>';
                                                echo '<option value="">Pilih Fungsi terlebih dahulu</option>';
                                            } else { ?>
                                            <option value="<?php echo $pok_terpilih['id_pok']; ?>">
                                                <?php echo $pok_terpilih['rincian_pok']; ?></option>
                                            <?php
                                                foreach ($pok as $key => $value) {
                                                ?>
                                            <option value="<?= $value['id_pok']; ?>">
                                                <?= $value['rincian_pok']; ?>
                                            </option>
                                            <?php

                                                }
                                                ?>
                                            <?php }
                                            ?>

                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-block btn-primary"><i
                                                class="fa fa-refresh"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($matriks_kinerja_pok)) {  ?>
<div class="row">
    <div class="col-lg-12 col-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <?php if ($this->session->userdata('level_user') == "user") { ?>
                    <?php } else { ?>

                    <td style="background-color: #d6f5d6; text-align:right;">
                        <a href="<?= base_url('matriks_kinerja_pok/tambah_pekerjaan/') . $pok_terpilih['id_pok'] ?>"
                            style="margin-bottom: 10px;" class="btn btn-md btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Tambah Data </a>
                    </td>
                    <?php  } ?>


                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <p class="text-muted font-13 m-b-30">
                            </p>
                            <table id="datatable-buttons-fix" class="table table-striped table-bordered"
                                style="width:100%">
                                <thead>
                                    <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                        <th width="1%">
                                            <center> NO
                                        </th>
                                        <th width="10%">
                                            <center> PEKERJAAN
                                        </th>
                                        <th width="1%">
                                            <center> TOTAL
                                        </th>
                                        <th width="1%">
                                            <center> ALOKASI
                                        </th>
                                        <th width="1%">
                                            <center> 01
                                        </th>
                                        <th width="1%">
                                            <center> 02
                                        </th>
                                        <th width="1%">
                                            <center> 03
                                        </th>
                                        <th width="1%">
                                            <center> 04
                                        </th>
                                        <th width="1%">
                                            <center> 05
                                        </th>
                                        <th width="1%">
                                            <center> 06
                                        </th>
                                        <th width="1%">
                                            <center> 07
                                        </th>
                                        <th width="1%">
                                            <center> 08
                                        </th>
                                        <th width="1%">
                                            <center> 09
                                        </th>
                                        <th width="1%">
                                            <center> 10
                                        </th>
                                        <th width="1%">
                                            <center> 11
                                        </th>
                                        <th width="1%">
                                            <center> 12
                                        </th>

                                        <?php if ($this->session->userdata('level_user') == "user") { ?>
                                        <?php } else { ?>

                                        <th width="3%">
                                            <center> AKSI
                                        </th>
                                        <?php  } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($matriks_kinerja_pok as $key => $q) {

                                        ?>
                                    <tr>
                                        <td>
                                            <center><?php echo $q['id_matriks_kinerja'] ?>
                                        </td>
                                        <td>
                                            <left><?php echo $q['rincian_pekerjaan'] ?>
                                                <?php echo $q['nama_kegiatan'] ?>
                                        </td>
                                        <td>
                                            <center><?php echo $q['sum_all'] ?><br><?php echo $q['satuan'] ?>
                                        </td>


                                        <td style="background-color: #d6f5d6; text-align:right;">

                                            <a title="list"
                                                href="<?= base_url('matriks_kinerja_pok/detail_pekerjaan/') . $q['id_matriks_kinerja']; ?>"
                                                style="margin-bottom: 10px;" class="btn btn-info btn-sm"> <i
                                                    class=""></i> Alokasi </a>
                                        </td>

                                        <?php if ($q['sum_01'] == $q['target_bulan_01'] & $q['count_01'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_01'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_01'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_01'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_01'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_02'] == $q['target_bulan_02'] & $q['count_02'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_02'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_02'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_02'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_02'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_03'] == $q['target_bulan_03'] & $q['count_03'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_03'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_03'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_03'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_03'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_04'] == $q['target_bulan_04'] & $q['count_04'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_04'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_04'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_04'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_04'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_05'] == $q['target_bulan_05'] & $q['count_05'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_05'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_05'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_05'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_05'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_06'] == $q['target_bulan_06'] & $q['count_06'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_06'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_06'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_06'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_06'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_07'] == $q['target_bulan_07'] & $q['count_07'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_07'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_07'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_07'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_07'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_08'] == $q['target_bulan_08'] & $q['count_08'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_08'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_08'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_08'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_08'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_09'] == $q['target_bulan_09'] & $q['count_09'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_09'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_09'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_09'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_09'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_10'] == $q['target_bulan_10'] & $q['count_10'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_10'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_10'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_10'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_10'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_11'] == $q['target_bulan_11'] & $q['count_11'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_11'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_11'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_11'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_11'] ?>)
                                                <?php  } ?>
                                        </td>

                                        <?php if ($q['sum_12'] == $q['target_bulan_12'] & $q['count_12'] > 0) {  ?>
                                        <td style="background-color: #d6f5d6; height:10px; ">
                                            <center>
                                                <?php  } else { ?>
                                        <td style="background-color: #ffe6e6; height:10px; ">
                                            <center>
                                                <?php  } ?>
                                                <?php if ($q['target_bulan_12'] == 0) {  ?>
                                                <?php  } else { ?>
                                                <b><?php echo $q['target_bulan_12'] ?></b></a>
                                                <p></p>
                                                <?php  } ?>
                                                <?php if ($q['count_12'] == 0) {  ?>
                                                <?php  } else { ?>
                                                (<?php echo $q['count_12'] ?>)
                                                <?php  } ?>
                                        </td>


                                        <?php if ($this->session->userdata('level_user') == "user") { ?>
                                        <?php } else { ?>

                                        <td style=" text-align:center;">

                                            <a href="<?= base_url('kegiatan/edit_pekerjaan/') . $q['id_matriks_kinerja']; ?>"
                                                class=" btn btn-primary btn-sm"> <i class="fa fa-pencil"></i> </a>

                                            <!-- <?php //if ($q['count_all'] == 0) {  
                                                                                                                ?>
                                                                                                            <a onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" href="hapus.php?hapus=&id=<?php echo $q['id_matriks_kinerja']; ?>" style="" class="btn btn-danger btn-sm"> <i class="fa fa-trash"></i> </a>
                                                                                                        <?php  //} else { 
                                                                                                        ?>
                                                                                                            (Terdapat Kinerja Individu)
                                                                                                            (Jika Ingin Delete Data maka Delete Kinerja Individu terlebih Dahulu) 
                                                                                                        <?php // } 
                                                                                                        ?> -->
                                        </td>
                                        <?php  } ?>

                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<!-- Modal Upload matriks kinerja pok-->
<div class="modal fade modal_upload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Upload Matriks Kinerja POK</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url(); ?>matriks_kinerja_pok/upload" method="post" enctype="multipart/form-data"
                    role="form">
                    <input type="hidden" value="" name="id_master_pekerjaan" />


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Upload Template
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="file" id="template" name="template" required="" class="form-control" value=""
                                accept=".xlsx">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">
var htmlobjek;
$(document).ready(function() {

    $("#bidang").change(function() {
        var kode_bidang = $("#bidang").val();
        $.ajax({
            url: "<?= base_url() ?>pok/ambil_pok_bidang",
            data: "kode_bidang=" + kode_bidang,
            cache: false,
            success: function(msg) {


                $("#id_pok").html(msg);
            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {

    $('.modal_edit').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#edit_nama_kegiatan').attr("value", div.data('nama_kegiatan'));
        modal.find('#edit_id_kegiatan').attr("value", div.data('id_kegiatan'));
        //modal.find('#email_to').attr("value",div.data(''));

    });

});
</script>