<div class="col-lg-12 col-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>List POK</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <p class="text-muted font-13 m-b-30">
                        </p>
                        <label style="margin-left: 10px;" id="custom-filter">
                            <select class="fungsi pilih select2">
                                <option value=""><b>Fungsi</b>
                                </option>
                                <?php
                                $i = 1;
                                foreach ($bidang as $key => $value) {
                                ?>
                                    <option value="<?= $value['nama_bidang']; ?>">
                                        <?= $value['nama_bidang']; ?>
                                    </option>
                                <?php

                                }
                                ?>

                            </select>
                        </label>
                        <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                    <th width="1%">
                                        <center> NO
                                    </th>
                                    <th width="1%">
                                        <center> TAHUN
                                    </th>
                                    <th width="1%">
                                        <center> FUNGSI
                                    </th>
                                    <th width="1%">
                                        <center> MAK
                                    </th>
                                    <th width="20%">
                                        <center> OUTPUT
                                    </th>
                                    <th width="1%">
                                        <center> KEGIATAN
                                    </th>

                                </tr>
                            </thead>


                            <tbody>
                                <?php
                                foreach ($pok as $key => $value) {


                                ?>
                                    <tr>
                                        <td>
                                            <center><?php echo $i++ ?>
                                        </td>
                                        <td>
                                            <center><?php echo $value['tahun'] ?>
                                        </td>
                                        <td>
                                            <center><?php echo $value['nama_bidang'] ?>
                                        </td>
                                        <td>
                                            <center><?php echo $value['kode'] ?>
                                        </td>
                                        <td>
                                            <left><?php echo $value['rincian_pok'] ?>
                                        </td>
                                        <td>
                                            <center><?php echo $value['jumlah_pekerjaan'] ?>
                                        </td>

                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        $('#custom-filter').appendTo(".dataTables_filter label");
        $('#custom-filter').on('change', function() {
            $('#datatable-buttons').DataTable().column(2).search(
                $('.fungsi').val(),
            ).draw();
        });

    });
</script>