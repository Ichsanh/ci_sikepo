<div class="x_panel">
    <div class="row">
        <div class="col-sm-12">


            <div class="x_title">
                <h2><b>Matrik Kerja Tahun Anggaran <?php echo  $_SESSION['tahun_anggaran']; ?></b> </h2>


                <div class="card-box table-responsive">
                    <p class="text-muted font-10 m-b-10">
                    </p>
                    <table id="datatable-buttons-fix" class="table table-striped table-bordered" style="width:100%; height:10px;">
                        <thead>
                            <tr style="background-color: #2a3f54; height:5px; color: #ffffff;">
                                <th width="1%">
                                    <center> ID POK
                                </th>

                                <th width="20%">
                                    <center> KEGIATAN/PEKERJAAN
                                </th>
                                <th width="20%">
                                    <center> NAMA PEGAWAI
                                </th>
                                <th width="1%">
                                    <center> 01
                                </th>
                                <th width="1%">
                                    <center> 02
                                </th>
                                <th width="1%">
                                    <center> 03
                                </th>
                                <th width="1%">
                                    <center> 04
                                </th>
                                <th width="1%">
                                    <center> 05
                                </th>
                                <th width="1%">
                                    <center> 06
                                </th>
                                <th width="1%">
                                    <center> 07
                                </th>
                                <th width="1%">
                                    <center> 08
                                </th>
                                <th width="1%">
                                    <center> 09
                                </th>
                                <th width="1%">
                                    <center> 10
                                </th>
                                <th width="1%">
                                    <center> 11
                                </th>
                                <th width="1%">
                                    <center> 12
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            foreach ($data as $key => $q) {

                            ?>
                                <tr>
                                    <td>
                                        <center><?php echo $q['id_matriks_kinerja'] ?>
                                    </td>
                                    <td>
                                        <left><?php echo $q['nama_pekerjaan_non_pok'] ?> <?php echo $q['rincian_pekerjaan'] ?> <?php echo $q['nama_kegiatan'] ?> (<?php echo $q['satuan'] ?>)
                                    </td>
                                    <td>
                                        <center><?php echo $q['nama_pegawai'] ?>
                                    </td>


                                    <?php
                                    $bulan = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12',);
                                    foreach ($bulan as $key => $i) {
                                        if ($q['target_' . $i] > 0) {  ?>
                                            <td style="background-color: #d6f5d6; height:10px; ">
                                                <center>
                                                    <?php echo $q['target_' . $i] ?>
                                                <?php  } else { ?>
                                            <td style="background-color: #ffe6e6; height:10px; ">
                                                <center>
                                                <?php  } ?>
                                            </td>
                                        <?php  } ?>

                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>