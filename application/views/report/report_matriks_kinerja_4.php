<div class="x_panel">
    <div class="row">
        <div class="col-sm-12">


            <div class="x_title">
                <h2><b>JUMLAH PEKERJAAN <?php echo  $_SESSION['tahun_anggaran']; ?></b> </h2>


                <div class="card-box table-responsive">
                    <p class="text-muted font-10 m-b-10">
                    </p>
                    <table id="datatable-buttons-fix" class="table table-striped table-bordered" style="width:100%; height:10px;">
                        <thead>
                            <tr style="background-color: #2a3f54; height:5px; color: #ffffff;">
                                <th width="30%">
                                    <center> NAMA
                                </th>
                                <th width="1%">
                                    <center> TAHUN
                                </th>
                                <th width="1%">
                                    <center> 01
                                </th>
                                <th width="1%">
                                    <center> 02
                                </th>
                                <th width="1%">
                                    <center> 03
                                </th>
                                <th width="1%">
                                    <center> 04
                                </th>
                                <th width="1%">
                                    <center> 05
                                </th>
                                <th width="1%">
                                    <center> 06
                                </th>
                                <th width="1%">
                                    <center> 07
                                </th>
                                <th width="1%">
                                    <center> 08
                                </th>
                                <th width="1%">
                                    <center> 09
                                </th>
                                <th width="1%">
                                    <center> 10
                                </th>
                                <th width="1%">
                                    <center> 11
                                </th>
                                <th width="1%">
                                    <center> 12
                                </th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            foreach ($data as $q) {

                            ?>
                                <tr>
                                    <td>
                                        <left><?php echo $q['nama_pegawai'] ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_all']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_01']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_02']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_03']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_04']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_05']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_06']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_07']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_08']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_09']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_10']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_11']), 0, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['count_12']), 0, '.', '.') . ''; ?>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>