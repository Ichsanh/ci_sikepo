<div class="x_panel">
    <div class="row">
        <div class="col-sm-12">


            <div class="x_title">
                <h2><b>POTENSI AK <?php echo  $_SESSION['tahun_anggaran']; ?></b> </h2>


                <div class="card-box table-responsive">
                    <p class="text-muted font-10 m-b-10">
                    </p>
                    <table id="datatable-buttons-fix" class="table table-striped table-bordered" style="width:100%; height:10px;">
                        <thead>
                            <tr style="background-color: #2a3f54; height:5px; color: #ffffff;">
                                <th width="30%">
                                    <center> NAMA
                                </th>
                                <th width="1%">
                                    <center> TAHUN
                                </th>
                                <th width="1%">
                                    <center> 01
                                </th>
                                <th width="1%">
                                    <center> 02
                                </th>
                                <th width="1%">
                                    <center> 03
                                </th>
                                <th width="1%">
                                    <center> 04
                                </th>
                                <th width="1%">
                                    <center> 05
                                </th>
                                <th width="1%">
                                    <center> 06
                                </th>
                                <th width="1%">
                                    <center> 07
                                </th>
                                <th width="1%">
                                    <center> 08
                                </th>
                                <th width="1%">
                                    <center> 09
                                </th>
                                <th width="1%">
                                    <center> 10
                                </th>
                                <th width="1%">
                                    <center> 11
                                </th>
                                <th width="1%">
                                    <center> 12
                                </th>


                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            foreach ($data as $key => $q) {

                            ?>
                                <tr>
                                    <td>
                                        <left><?php echo $q['nama_pegawai'] ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_all']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_01']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_02']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_03']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_04']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_05']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_06']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_07']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_08']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_09']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_10']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_11']), 2, '.', '.') . ''; ?>
                                    </td>
                                    <td>
                                        <center><?php echo number_format(($q['ak_12']), 2, '.', '.') . ''; ?>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>