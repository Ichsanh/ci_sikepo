<div class="row">
    <div class="col-md-12 col-sm-12  profile_details">
        <div class="well profile_view">
            <div class="col-sm-12">

                <div class="left col-md-8 col-sm-8">
                    <h2><?php echo $this->session->userdata('nama_pegawai');?></h2>
                    <p><strong>NAMA: </strong><?php echo $this->session->userdata('nama_pegawai');?></p>
                    <p><strong>NIP LAMA: </strong><?php echo $this->session->userdata('nip_pegawai_lama');?></p>
                    <p><strong>NIP BARU: </strong><?php echo $this->session->userdata('nip_pegawai');?></p>
                    <p><strong>PANGKAT: </strong><?php echo $this->session->userdata('kode_pangkat');?></p>
                    <p><strong>SATKER: </strong><?php echo $this->session->userdata('kode_satker');?></p>
                    <p><strong>BIDANG/BAGIAN: </strong><?php echo $this->session->userdata('kode_bidang');?></p>
                    <p><strong>SEKSI/SUBBAG: </strong><?php echo $this->session->userdata('kode_seksi');?></p>
                    <p><strong>UNIT KERJA: </strong><?php echo $this->session->userdata('kode_unit_kerja');?></p>
                    <p><strong>JABATAN: </strong><?php echo $this->session->userdata('kode_jabatan');?></p>
                    <ul class="list-unstyled">
                    </ul>
                </div>
                <div class="right col-md-4 col-sm-4 text-center">
                    <img src="images/img.jpg" alt="" class="img-circle img-fluid">
                </div>
            </div>
            <div class=" profile-bottom text-center">

            </div>
        </div>
    </div>


</div>