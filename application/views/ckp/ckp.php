<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        if ($this->session->flashdata('sukses') <> '') {
        ?>
        <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('sukses'); ?></p>
        </div>
        <?php
        }
        ?>
        <?php
        if ($this->session->flashdata('gagal') <> '') {
        ?>
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('gagal'); ?></p>
        </div>
        <?php
        }
        ?>
    </div>

</div>
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>List Kegiatan</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <div class="col-lg-12 col-12">
                <div class="card card-grey">
                    <div class="card-body">
                        <div class="box-header">
                            <form action="<?= base_url() ?>ckp/lihat_ckp" method="get" role="form">
                                <div class="form-group row">


                                    <label class="col-sm-2 col-form-label">PEGAWAI</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="nip_pegawai"
                                            id="nip_pegawai" data-dropdown-css-class="select2-danger"
                                            style="width: 100%;">
                                            <?php
                                            if (empty($pegawai_terpilih)) {
                                                echo '<option value="">Pilih Pegawai</option>';
                                            } else { ?>
                                            <option value="<?php echo $pegawai_terpilih['nip_pegawai']; ?>">
                                                <?php echo $pegawai_terpilih['nama_pegawai']; ?></option>
                                            <?php }
                                            ?>
                                            <?php
                                            foreach ($pegawai as $key => $value) {
                                            ?>
                                            <option value="<?= $value['nip_pegawai']; ?>">
                                                <?= $value['nama_pegawai']; ?>
                                            </option>
                                            <?php

                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">BULAN</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="bulan" id="bulan"
                                            data-dropdown-css-class="select2-danger" style="width: 100%;">
                                            <?php
                                            if (empty($pegawai_terpilih)) {
                                                echo '<option value="">Pilih Bulan</option>';
                                                for ($a = 1; $a < 13; $a++) { ?>
                                            <option value="<?= $a; ?>">
                                                <?= $a; ?>
                                            </option>
                                            <?php


                                                }
                                            } else { ?>
                                            <option value="<?php echo $bulan; ?>">
                                                <?php echo $bulan; ?></option>
                                            <?php
                                                for ($a = 1; $a < 13; $a++) { ?>
                                            <option value="<?= $a; ?>">
                                                <?= $a; ?>
                                            </option>
                                            <?php

                                                }
                                                ?>
                                            <?php }
                                            ?>

                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-block btn-primary"><i
                                                class="fa fa-refresh"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if (!empty($pegawai_terpilih)) { ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form action="<?= base_url(); ?>ckp/update_ckp" method="post" enctype="multipart/form-data" role="form">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">



                        <div class="col-xs-1">

                            <?php echo "<input type='submit' class='btn btn-sm   btn-primary' value='SIMPAN'>"; ?>
                            <?php echo "<a href='".base_url()."ckp/download_ckp/".$nip_pegawai."/".$bulan."' class='btn btn-sm   btn-success'>DOWNLOAD</a>"; ?>
                        </div>

                        <div class="col-sm-12">
                            <div class="card-box ">
                                <h3>

                                    <p class="text-muted ">
                                        Kegiatan Utama
                                    </p>
                                </h3>
                                <table class="table table-striped  table-bordered table-responsive" style="width:100%">
                                    <thead>
                                        <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                            <th width="1%">
                                                <center> NO
                                            </th>

                                            <th width="10%">
                                                <center> RINCIAN
                                            </th>
                                            <th width="1%">
                                                <center> SATUAN
                                            </th>
                                            <!-- <th width="1%">
                                            <center> BULAN
                                        </th> -->
                                            <th width="1%">
                                                <center> TARGET
                                            </th>
                                            <th width="1%">
                                                <center> REALISASI
                                            </th>
                                            <th width="1%">
                                                <center> KUALITAS
                                            </th>
                                            <th width="1%">
                                                <center> BUTIR
                                            </th>
                                            <th width="1%">
                                                <center> AK
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                    $no = 0;
                                    $i = 0;
                                    if (!empty($data)) {


                                        foreach ($data as $q) {

                                            $no++;
                                    ?>
                                        <tr>
                                            <td><?= $no; ?></td>

                                            <td>
                                                <LEFT>
                                                    <?= $q['nama_pekerjaan_non_pok']; ?><?= $q['rincian_pekerjaan']; ?>
                                                    <?= $q['nama_kegiatan']; ?>
                                            </td>
                                            <td>
                                                <LEFT><?= $q['satuan']; ?>
                                            </td>



                                            <!-- <td>
                                                    <center><input style="width:75px; text-align:right;" readonly="" name="<?php echo "bulan[$i]"; ?>" value="<?php echo number_format(($q['bulan']), 0, '.', '.') . ''; ?>" />
                                                </td> -->
                                            <td>
                                                <center><input style="width:75px; text-align:right;" readonly=""
                                                        name="<?php echo "target[$i]"; ?>"
                                                        value="<?php echo number_format(($q['target']), 2, '.', '.') . ''; ?>" />
                                            </td>



                                            <input style="width:75px; text-align:right;" hidden="true"
                                                name="<?php echo "nip_pegawai[$i]"; ?>"
                                                value="<?php echo $q['nip_pegawai']; ?>" />
                                            <input style="width:75px; text-align:right;" hidden="true"
                                                name="<?php echo "id_skp[$i]"; ?>"
                                                value="<?php echo $q['id_skp']; ?>" />


                                            <?php if ($this->session->userdata('level_user') == "user") { ?>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "realisasi[$i]"; ?>"
                                                        value="<?php echo number_format(($q['realisasi']), 2, '.', '.') . ''; ?>" />
                                            </td>
                                            <td>
                                                <center><input readonly=""
                                                        style="width:75px; text-align:right; background-color: #;"
                                                        class="inputs" name="<?php echo "kualitas[$i]"; ?>"
                                                        value="<?php echo number_format(($q['kualitas']), 0, '.', '.') . ''; ?>" />
                                            </td> <?php  }  ?>

                                            <?php if ($this->session->userdata('level_user') == "admin_bidang" or $this->session->userdata('level_user') == "superadmin" or $this->session->userdata('level_user') == "admin_prov") { ?>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "realisasi[$i]"; ?>"
                                                        value="<?php echo number_format(($q['realisasi']), 2, '.', '.') . ''; ?>" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "kualitas[$i]"; ?>"
                                                        value="<?php echo number_format(($q['kualitas']), 0, '.', '.') . ''; ?>" />
                                            </td>

                                            <?php  }  ?>
                                            <td>
                                                <center><?=$q['butir_fungsional'] ?>
                                                    <span data-toggle='modal' data-target='.modal_link_ak'
                                                        data-uraian_kegiatan="<?= $q['nama_pekerjaan_non_pok']; ?><?= $q['rincian_pekerjaan']; ?>
                                            <?= $q['nama_kegiatan']; ?>" data-id_skp="<?php echo $q['id_skp']; ?>"
                                                        data-nip_pegawai="<?php echo $nip_pegawai; ?>">
                                                        <a data-toggle="tooltip" data-placement="top" title="link AK"><i
                                                                class="fa fa-edit fa-lg"></i></a>
                                                    </span>
                                            </td>
                                            <td style="text-align: right;">
                                                <?=$q['nilai_ak'] ?>
                                            </td>

                                        </tr>
                                        <?php $i++;
                                        }
                                    }   ?>
                                        <?php 
                                $a=55;
                                for ($i=50; $i < $a ; $i++) { 
                                  $no++; 
                                ?>
                                        <tr>
                                            <input style="width:75px; text-align:right;" hidden="true"
                                                name="<?php echo "nip_pegawai[$i]"; ?>"
                                                value="<?php echo $nip_pegawai; ?>" />
                                            <input style="width:75px; text-align:right;" hidden="true"
                                                name="<?php echo "bulan[$i]"; ?>" value="<?php echo $bulan; ?>" />
                                            <td><?= $no; ?></td>
                                            <td><input style="width:300px; text-align:left; background-color: #ffffcc;"
                                                    class="inputs" name="<?php echo "nama_pekerjaan_non_pok[$i]"; ?>"
                                                    value="" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:left; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "satuan[$i]"; ?>" value="" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "target[$i]"; ?>" value="" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "realisasi[$i]"; ?>" value="" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "kualitas[$i]"; ?>" value="" />
                                            </td>
                                            <td>
                                                <!-- <center><input
                                                    style="width:75px; text-align:right; background-color: #ffffcc;"
                                                    class="inputs" name="<?php echo "butir[$i]"; ?>" value="" /> -->
                                            </td>
                                            <td>
                                                <!-- <center><input
                                                    style="width:75px; text-align:right; background-color: #ffffcc;"
                                                    class="inputs" name="<?php echo "ak[$i]"; ?>" value="" /> -->
                                            </td>

                                        </tr>
                                        <?php  }  ?>
                                    </tbody>
                                </table>

                                <h3>

                                    <p class="text-muted ">
                                        Kegiatan Tambahan
                                    </p>
                                </h3>
                                <table class="table table-striped  table-bordered" style="width:100%">
                                    <thead>
                                        <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                            <th width="1%">
                                                <center> NO
                                            </th>

                                            <th width="10%">
                                                <center> RINCIAN
                                            </th>
                                            <th width="1%">
                                                <center> SATUAN
                                            </th>
                                            <!-- <th width="1%">
                                            <center> BULAN
                                        </th> -->
                                            <th width="1%">
                                                <center> TARGET
                                            </th>
                                            <th width="1%">
                                                <center> REALISASI
                                            </th>
                                            <th width="1%">
                                                <center> KUALITAS
                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                    $no = 0;
                                    $i = 0;
                                    if (!empty($data_tambahan)) {


                                        foreach ($data_tambahan as $q) {

                                            $no++;
                                    ?>
                                        <tr>
                                            <td><?= $no; ?></td>

                                            <td>
                                                <LEFT>
                                                    <?= $q['nama_pekerjaan_non_pok']; ?><?= $q['rincian_pekerjaan']; ?>
                                                    <?= $q['nama_kegiatan']; ?>
                                            </td>
                                            <td>
                                                <LEFT><?= $q['satuan']; ?>
                                            </td>



                                            <!-- <td>
                                                    <center><input style="width:75px; text-align:right;" readonly="" name="<?php echo "bulan[$i]"; ?>" value="<?php echo number_format(($q['bulan']), 0, '.', '.') . ''; ?>" />
                                                </td> -->
                                            <td>
                                                <center><input style="width:75px; text-align:right;" readonly=""
                                                        name="<?php echo "target[$i]"; ?>"
                                                        value="<?php echo number_format(($q['target']), 2, '.', '.') . ''; ?>" />
                                            </td>



                                            <input style="width:75px; text-align:right;" hidden="true"
                                                name="<?php echo "nip_pegawai[$i]"; ?>"
                                                value="<?php echo $q['nip_pegawai']; ?>" />
                                            <input style="width:75px; text-align:right;" hidden="true"
                                                name="<?php echo "id_skp[$i]"; ?>"
                                                value="<?php echo $q['id_skp']; ?>" />


                                            <?php if ($this->session->userdata('level_user') == "user") { ?>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "realisasi[$i]"; ?>"
                                                        value="<?php echo number_format(($q['realisasi']), 2, '.', '.') . ''; ?>" />
                                            </td>
                                            <td>
                                                <center><input readonly=""
                                                        style="width:75px; text-align:right; background-color: #;"
                                                        class="inputs" name="<?php echo "kualitas[$i]"; ?>"
                                                        value="<?php echo number_format(($q['kualitas']), 0, '.', '.') . ''; ?>" />
                                            </td> <?php  }  ?>

                                            <?php if ($this->session->userdata('level_user') == "admin_bidang" or $this->session->userdata('level_user') == "superadmin" or $this->session->userdata('level_user') == "admin_prov") { ?>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "realisasi[$i]"; ?>"
                                                        value="<?php echo number_format(($q['realisasi']), 2, '.', '.') . ''; ?>" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "kualitas[$i]"; ?>"
                                                        value="<?php echo number_format(($q['kualitas']), 0, '.', '.') . ''; ?>" />
                                            </td>

                                            <?php  }  ?>

                                        </tr>
                                        <?php $i++;
                                        }
                                    }   ?>
                                        <?php 
                                $a=75; //untuk tambahan
                                for ($i=70; $i < $a ; $i++) { 
                                  $no++; 
                                ?>
                                        <tr>
                                            <input style="width:75px; text-align:right;" hidden="true"
                                                name="<?php echo "nip_pegawai[$i]"; ?>"
                                                value="<?php echo $nip_pegawai; ?>" />
                                            <input style="width:75px; text-align:right;" hidden="true"
                                                name="<?php echo "bulan[$i]"; ?>" value="<?php echo $bulan; ?>" />
                                            <td><?= $no; ?></td>
                                            <td><input style="width:300px; text-align:left; background-color: #ffffcc;"
                                                    class="inputs" name="<?php echo "nama_pekerjaan_non_pok[$i]"; ?>"
                                                    value="" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:left; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "satuan[$i]"; ?>" value="" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "target[$i]"; ?>" value="" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "realisasi[$i]"; ?>" value="" />
                                            </td>
                                            <td>
                                                <center><input
                                                        style="width:75px; text-align:right; background-color: #ffffcc;"
                                                        class="inputs" name="<?php echo "kualitas[$i]"; ?>" value="" />
                                            </td>


                                        </tr>
                                        <?php  }  ?>

                                    </tbody>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
}
?>



<!-- Modal edit Kegiatan-->
<div class="modal fade modal_link_ak" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Link Angka Kredit</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url(); ?>ckp/link_ak" method="post" enctype="multipart/form-data" role="form">
                    <input type="hidden" value="" name="id_skp" id="id_skp" />
                    <input type="hidden" value="" name="nip_pegawai" id="nip_pegawai" />

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Uraian kegiatan

                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="uraian_kegiatan" name="uraian_kegiatan" required=""
                                class="form-control" value="" disabled>
                        </div>
                    </div>


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">AK
                            <span class="">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">

                            <select class="kategori pilih select2 form-control" name="id_butir" style="width: 100%;">

                                <option value="">Pilih Butir Fungsional</option>
                                <?php
                                    foreach ($butir_fungsional as $key => $value) {
                                    ?>
                                <option value="<?= $value['id_butir']; ?>">
                                    <?= '['.$value['butir_fungsional'].'] '.$value['pekerjaan_rincian'].' ('.$value['jenjang_fungsional'].')'; ?>
                                </option>
                                <?php

                                    }
                                    ?>
                            </select>
                        </div>

                    </div>
                    <!-- <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">KETERANGAN <span class=""></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="keterangan_pekerjaan" name="keterangan_pekerjaan" class="form-control">
                        </div>
                    </div> -->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {

    $('.modal_link_ak').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#uraian_kegiatan').attr("value", div.data('uraian_kegiatan'));
        modal.find('#id_skp').attr("value", div.data('id_skp'));
        modal.find('#nip_pegawai').attr("value", div.data('nip_pegawai'));
        //modal.find('#email_to').attr("value",div.data(''));

    });

});
</script>