<div class="row">
    <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
            <div class="x_title">
                <h2>FORM EDIT : <?php echo $kegiatan['nama_kegiatan']; ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                </ul>

                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-12 ">
                    <div class="x_content">

                        <br />
                        <form action="" method="post" enctype="multipart/form-data" role="form">
                            <input type="hidden" name="id_pok" value="<?= $kegiatan['id_pok'] ?>">
                            <input type="hidden" name="url" value="<?= $url ?>">
                            <input type="hidden" name="id_kegiatan" value="<?= $kegiatan['id_kegiatan'] ?>">
                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> JENIS PEKERJAAN <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                    <select required="true" class="form-control select2 select2-danger" name="id_jenis_pekerjaan" id="id_jenis_pekerjaan" data-dropdown-css-class="select2-danger" style="width: 100%;">
                                        <?php

                                        echo "<option value='$jenis_pekerjaan_terpilih[id_jenis_pekerjaan]'> $jenis_pekerjaan_terpilih[jenis_pekerjaan] </option>";
                                        ?>
                                        <?php
                                        foreach ($jenis_pekerjaan as $key => $aa) {

                                            echo "<option value='$aa[id_jenis_pekerjaan]'> $aa[jenis_pekerjaan] </option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>


                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> PEKERJAAN <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                    <select required="true" class="form-control select2 select2-danger" name="id_master_pekerjaan" id="id_master_pekerjaan" data-dropdown-css-class="select2-danger" style="width: 100%;">
                                        <?php


                                        echo "<option value='$pekerjaan_terpilih[id_master_pekerjaan]'> $pekerjaan_terpilih[rincian_pekerjaan] </option>";
                                        ?>
                                        <?php
                                        foreach ($pekerjaan as $key => $aa) {

                                            echo "<option value='$aa[id_master_pekerjaan]'> $aa[rincian_pekerjaan] </option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>



                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                                    SATUAN <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                    <input required="true" type="text" id="satuan" name="satuan" class="form-control " value="<?php echo $data['satuan']; ?>">
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                                    WAKTU <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                    <input required="true" type="text" id="waktu" name="waktu" class="form-control " value="<?php echo $data['waktu']; ?>">
                                </div>
                            </div>

                            <div class="item form-group">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                                    WAKTU DESKRIPSI <span class="required">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 ">
                                    <select required="true" class="form-control select2 select2-danger" name="id_waktu_deskripsi" id="id_waktu_deskripsi" data-dropdown-css-class="select2-danger" style="width: 100%;">
                                        <?php

                                        echo "<option value='$waktu_deskripsi_terpilih[id_waktu_deskripsi]'> $waktu_deskripsi_terpilih[waktu_deskripsi] </option>";
                                        ?>
                                        <?php
                                        foreach ($waktu_deskripsi as $aa) {

                                            echo "<option value='$aa[id_waktu_deskripsi]'> $aa[waktu_deskripsi] </option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>


                    </div>
                </div>


                <div class="col-md-3 col-sm-12 ">
                    <div class="x_content">

                        <br />
                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 01 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_01" name="target_bulan_01" class="form-control" value="<?php echo $data['target_bulan_01']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 02 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_02" name="target_bulan_02" class="form-control " value="<?php echo $data['target_bulan_02']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 03 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_03" name="target_bulan_03" class="form-control " value="<?php echo $data['target_bulan_03']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 04 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_04" name="target_bulan_04" class="form-control " value="<?php echo $data['target_bulan_04']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 05 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_05" name="target_bulan_05" class="form-control " value="<?php echo $data['target_bulan_05']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 06 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_06" name="target_bulan_06" class="form-control " value="<?php echo $data['target_bulan_06']; ?>">
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 col-sm-12 ">
                    <div class="x_content">
                        <br />
                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 07 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_07" name="target_bulan_07" class="form-control " value="<?php echo $data['target_bulan_07']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 08 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_08" name="target_bulan_08" class="form-control " value="<?php echo $data['target_bulan_08']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 09 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_09" name="target_bulan_09" class="form-control " value="<?php echo $data['target_bulan_09']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 10 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_10" name="target_bulan_10" class="form-control " value="<?php echo $data['target_bulan_10']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 11 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_11" name="target_bulan_11" class="form-control " value="<?php echo $data['target_bulan_11']; ?>">
                            </div>
                        </div>

                        <div class="item form-group">
                            <label class="col-form-label col-md-6 col-sm-6 label-align" for="first-name">
                                TARGET 12 <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 ">
                                <input type="text" id="target_bulan_12" name="target_bulan_12" class="form-control " value="<?php echo $data['target_bulan_12']; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>






            <div class="ln_solid"></div>
            <div class="item form-group">
                <div class="col-md-12 col-sm-12 offset-md-12">
                    <button class="btn btn-danger" onclick="history.back(-1)"> <i class="fa fa-times"></i> Batal</button>
                    <button type="submit" name="simpan" class="btn btn-success">Submit</button>


                </div>
            </div>

            </form>
        </div>
    </div>
</div>
<script>
    function goBack() {
        window.history.back();
    }
</script>