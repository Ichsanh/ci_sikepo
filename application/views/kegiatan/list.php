<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        if ($this->session->flashdata('sukses') <> '') {
        ?>
        <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('sukses'); ?></p>
        </div>
        <?php
        }
        ?>
        <?php
        if ($this->session->flashdata('gagal') <> '') {
        ?>
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('gagal'); ?></p>
        </div>
        <?php
        }
        ?>
    </div>

</div>
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>List Kegiatan</h2>
            <ul class="nav navbar-right panel_toolbox">

                <a href="<?= base_url() ?>kegiatan/download_master">
                    <button class="btn btn-sm btn-block btn-success"><i class="fa fa-download"></i> Master</button>
                </a>
                &nbsp;
                <a href="<?= base_url() ?>kegiatan/download_template">
                    <button class="btn btn-sm btn-block btn-success"><i class="fa fa-download"></i> Template</button>
                </a>
                &nbsp;
                <a>
                    <button class="btn btn-sm btn-block btn-primary " data-toggle="modal" data-target=".modal_upload"><i
                            class="fa fa-upload"></i> Template</button>
                </a>

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <div class="col-lg-12 col-12">
                <div class="card card-grey">
                    <div class="card-body">
                        <div class="box-header">
                            <form action="<?= base_url() ?>kegiatan/lihat_kegiatan" method="get" role="form">
                                <div class="form-group row">


                                    <label class="col-sm-2 col-form-label">FUNGSI</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="kode_bidang"
                                            id="bidang" data-dropdown-css-class="select2-danger" style="width: 100%;">
                                            <?php
                                            if (empty($bidang_terpilih)) {
                                                echo '<option value="">Pilih Fungsi</option>';
                                            } else { ?>
                                            <option value="<?php echo $bidang_terpilih['kode_bidang']; ?>">
                                                <?php echo $bidang_terpilih['nama_bidang']; ?></option>
                                            <?php }
                                            ?>
                                            <?php
                                            foreach ($bidang as $key => $value) {
                                            ?>
                                            <option value="<?= $value['kode_bidang']; ?>">
                                                <?= $value['nama_bidang']; ?>
                                            </option>
                                            <?php

                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">POK</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="id_pok" id="id_pok"
                                            data-dropdown-css-class="select2-danger" style="width: 100%;">
                                            <?php
                                            if (empty($bidang_terpilih)) {
                                                echo '<option value="">Pilih POK</option>';
                                                echo '<option value="">Pilih Fungsi terlebih dahulu</option>';
                                            } else { ?>
                                            <option value="<?php echo $pok_terpilih['id_pok']; ?>">
                                                <?php echo $pok_terpilih['rincian_pok']; ?></option>
                                            <?php
                                                foreach ($pok as $key => $value) {
                                                ?>
                                            <option value="<?= $value['id_pok']; ?>">
                                                <?= $value['rincian_pok']; ?>
                                            </option>
                                            <?php

                                                }
                                                ?>
                                            <?php }
                                            ?>

                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-block btn-primary"><i
                                                class="fa fa-refresh"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (empty($kegiatan) and !empty($bidang_terpilih)) {  ?>
<div class="row">
    <div class="x_panel">
        <div class="x_content">

            <div style="text-align:center;">
                <button type="button" class="btn btn-primary btn-sm " class="title_right" data-toggle="modal"
                    data-target=".modal_tambah">+ Kegiatan</button>
            </div>

        </div>
    </div>
</div>
<?php } ?>
<?php if (!empty($kegiatan)) {  ?>
<div class="row">
    <div class="x_panel">
        <div class="x_content">
            <div class="row">
                <?php if (!empty($kegiatan)) {  ?>
                <td style="text-align:right;">
                    <button type="button" class="btn btn-primary btn-sm " class="title_right" data-toggle="modal"
                        data-target=".modal_tambah">+ Kegiatan</button>
                </td>
                <?php } ?>

                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <p class="text-muted font-13 m-b-30">
                        </p>
                        <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                    <th width="1%">
                                        <center> NO
                                    </th>
                                    <th width="8%">
                                        <center> KEGIATAN
                                    </th>
                                    <th width="10%">
                                        <center> PEKERJAAN
                                    </th>

                                    <th width="3%">
                                        <center> AKSI
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 1;
                                    foreach ($kegiatan as $key => $q) {

                                    ?>
                                <tr>
                                    <td>
                                        <center><?php echo $no++ ?>
                                    </td>
                                    <td>
                                        <left><?php echo $q['nama_kegiatan'] ?>
                                    </td>

                                    <td>
                                        <?php
                                                $query = "SELECT rincian_pekerjaan,id_matriks_kinerja,
                                        count(d.target) as jumlah_target
                                        FROM tabel_matriks_kinerja a
                                        left join tabel_master_kegiatan b on a.id_kegiatan=b.id_kegiatan
                                        left join tabel_master_pekerjaan c on
                                        a.id_master_pekerjaan=c.id_master_pekerjaan
                                        left join tabel_matriks_kinerja_individu d on a.id_matriks_kinerja=d.id_matriks
                                        where a.id_kegiatan=$q[id_kegiatan]
                                        group by a.id_matriks_kinerja ";
                                                $hasil_query = $this->db->query($query)->result_array();
                                                foreach ($hasil_query as $key => $value) {
                                                    echo $value['rincian_pekerjaan']; ?>
                                        <a href="<?= base_url('kegiatan/edit_pekerjaan/') . $value['id_matriks_kinerja'] ?>"
                                            data-toggle="tooltip" data-placement="top" title="edit pekerjaan"><i
                                                class=" fa fa-edit fa-lg"></i></a>


                                        <?php if ($value['jumlah_target'] > 0) { ?>
                                        <?php } else { ?>
                                        <a onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"
                                            href="<?= base_url('kegiatan/hapus_pekerjaan/') . $value['id_matriks_kinerja'] ?>"
                                            data-toggle="tooltip" data-placement="top" title="hapus pekerjaan"><i
                                                class="  fa fa-trash fa-lg"></i></a>
                                        <?php } ?>
                                        <br>
                                        <?php
                                                } ?>
                                    </td>


                                    <td style="text-align:right;">
                                        <center>
                                            <a href="<?= base_url('kegiatan/tambah_pekerjaan/') . $q['id_kegiatan'] ?>"
                                                data-toggle="tooltip" data-placement="top" title="tambah pekerjaan"> <i
                                                    class="fa fa-plus"></i>
                                            </a>
                                            <span data-toggle='modal' data-target='.modal_edit'
                                                data-id_kegiatan="<?php echo $q['id_kegiatan']; ?>"
                                                data-nama_kegiatan="<?php echo $q['nama_kegiatan']; ?>">
                                                <a data-toggle="tooltip" data-placement="top" title="edit kegiatan"><i
                                                        class="fa fa-edit fa-lg"></i></a>
                                            </span>
                                            <?php if ($q['jumlah_kegiatan_matriks'] <= 0) { ?>
                                            <a onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"
                                                href="<?= base_url('kegiatan/hapus/') . $q['id_kegiatan'] ?>"
                                                data-toggle="tooltip" data-placement="top" title="hapus kegiatan"><i
                                                    class="  fa fa-trash fa-lg"></i></a>
                                            <?php } ?>
                                    </td>


                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<!-- Modal tambah Kegiatan-->
<div class="modal fade modal_tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah Kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url(); ?>kegiatan/tambah" method="post" enctype="multipart/form-data"
                    role="form">
                    <input type="hidden" value="" name="id_master_pekerjaan" />


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NAMA KEGIATAN
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="nama_kegiatan" name="nama_kegiatan" required="" class="form-control"
                                value="">
                        </div>
                    </div>


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">POK
                            <span class="">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">

                            <select class="kategori pilih select2 form-control" name="id_pok" style="width: 100%;">
                                <?php
                                if (empty($bidang_terpilih)) {
                                    echo '<option value="">Pilih POK</option>';
                                    echo '<option value="">Pilih Fungsi terlebih dahulu</option>';
                                } else { ?>
                                <option value="<?php echo $pok_terpilih['id_pok']; ?>">
                                    <?php echo $pok_terpilih['rincian_pok']; ?></option>
                                <?php
                                    foreach ($pok as $key => $value) {
                                    ?>
                                <option value="<?= $value['id_pok']; ?>">
                                    <?= $value['rincian_pok']; ?>
                                </option>
                                <?php

                                    }
                                    ?>
                                <?php }
                                ?>

                            </select>
                        </div>

                    </div>
                    <!-- <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">KETERANGAN <span class=""></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="keterangan_pekerjaan" name="keterangan_pekerjaan" class="form-control">
                        </div>
                    </div> -->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>

<!-- Modal Upload Kegiatan-->
<div class="modal fade modal_upload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Upload Kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url(); ?>kegiatan/upload" method="post" enctype="multipart/form-data"
                    role="form">
                    <input type="hidden" value="" name="id_master_pekerjaan" />


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Upload Template
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="file" id="template" name="template" required="" class="form-control" value=""
                                accept=".xlsx">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>

<!-- Modal edit Kegiatan-->
<div class="modal fade modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Edit Kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url(); ?>kegiatan/edit" method="post" enctype="multipart/form-data" role="form">
                    <input type="hidden" value="" name="id_kegiatan" id="edit_id_kegiatan" />


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NAMA KEGIATAN
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="edit_nama_kegiatan" name="nama_kegiatan" required=""
                                class="form-control" value="">
                        </div>
                    </div>


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">POK
                            <span class="">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">

                            <select class="kategori pilih select2 form-control" name="id_pok" style="width: 100%;">
                                <?php
                                if (empty($bidang_terpilih)) {
                                    echo '<option value="">Pilih POK</option>';
                                    echo '<option value="">Pilih Fungsi terlebih dahulu</option>';
                                } else { ?>
                                <option value="<?php echo $pok_terpilih['id_pok']; ?>">
                                    <?php echo $pok_terpilih['rincian_pok']; ?></option>
                                <?php
                                    foreach ($pok as $key => $value) {
                                    ?>
                                <option value="<?= $value['id_pok']; ?>">
                                    <?= $value['rincian_pok']; ?>
                                </option>
                                <?php

                                    }
                                    ?>
                                <?php }
                                ?>

                            </select>
                        </div>

                    </div>
                    <!-- <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">KETERANGAN <span class=""></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="keterangan_pekerjaan" name="keterangan_pekerjaan" class="form-control">
                        </div>
                    </div> -->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {

    $("#bidang").change(function() {
        var kode_bidang = $("#bidang").val();
        $.ajax({
            url: "<?= base_url() ?>pok/ambil_pok_bidang",
            data: "kode_bidang=" + kode_bidang,
            cache: false,
            success: function(msg) {


                $("#id_pok").html(msg);
            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {

    $('.modal_edit').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#edit_nama_kegiatan').attr("value", div.data('nama_kegiatan'));
        modal.find('#edit_id_kegiatan').attr("value", div.data('id_kegiatan'));
        //modal.find('#email_to').attr("value",div.data(''));

    });

});
</script>