<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2> List Pekerjaan</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li> <a>
                        <button class="btn btn-primary btn-sm " class="title_right" data-toggle="modal"
                            data-target=".modal_tambah">+ Tambah Pekerjaan</button>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>pekerjaan/download_master">
                        <button class="btn btn-sm btn-block btn-success"><i class="fa fa-download"></i> Master</button>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>pekerjaan/download_template">
                        <button class="btn btn-sm btn-block btn-success"><i class="fa fa-download"></i>
                            Template</button>
                    </a>
                </li>
                <li>
                    <a>
                        <button class="btn btn-sm btn-block btn-primary " data-toggle="modal"
                            data-target=".modal_upload"><i class="fa fa-upload"></i> Template</button>
                    </a>
                </li>

            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <p class="text-muted font-13 m-b-30">
                        </p>


                        <label style="margin-left: 10px;" id="custom-filter">
                            <select class="kategori pilih select2">
                                <option value=""><b>Jenis Pekerjaan</b>
                                </option>
                                <?php
                                foreach ($jenis_pekerjaan as $key => $value) {
                                ?>
                                <option value="<?= $value['jenis_pekerjaan']; ?>">
                                    <?= $value['jenis_pekerjaan']; ?>
                                </option>
                                <?php

                                }
                                ?>

                            </select>
                        </label>
                        <table id="table" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                    <th class="text-center">No</th>
                                    <th class="text-center">Kategori</th>
                                    <th class="text-center">Pekerjaan</th>
                                    <th class="text-center">Keterangan</th>
                                    <th class="text-center">status</th>
                                    <th class="text-center">creater by</th>
                                    <th class="text-center">approval by</th>
                                    <?php
                                    if ($this->session->userdata('level_user') <> 'user' and $this->session->userdata('level_user') <> 'pejabat') {
                                    ?>
                                    <th class="text-center">approval</th>
                                    <?php
                                    }

                                    ?>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <!-- <tfoot>
                                    <tr>
                                        <th>id</th>
                                        <th>Judul</th>
                                        <th>Level</th>
                                        <th>Jenis</th>
                                        <th>Aksi</th>
                                        
                                    </tr>
                                </tfoot> -->
                        </table>
                        <!-- <table id="example" class="table table-bordered table-condensed" width="100%"></table> -->
                    </div>
                </div>
            </div>
            <!-- Individual Column Searching (Select Inputs) end -->

        </div>
    </div>
</div>

<!-- Modal tambah -->
<div class="modal fade modal_tambah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tambah Pekerjaan</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="pekerjaan/tambah" method="post" enctype="multipart/form-data" role="form">
                    <input type="hidden" value="" name="id_master_pekerjaan" />


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NAMA PEKERJAAN
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="rincian_pekerjaan" name="rincian_pekerjaan" required=""
                                class="form-control " value="">
                        </div>
                    </div>


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">JENIS PEKERJAAN
                            <span class="">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">

                            <select class=" select2 form-control" name="id_jenis_pekerjaan" style="width: 100%;"
                                required="">
                                <option value=""><b>Jenis Pekerjaan</b>
                                </option>
                                <?php
                                foreach ($jenis_pekerjaan as $key => $value) {
                                ?>
                                <option value="<?= $value['id_jenis_pekerjaan']; ?>">
                                    <?= $value['jenis_pekerjaan']; ?>
                                </option>
                                <?php

                                }
                                ?>

                            </select>
                        </div>

                    </div>
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">KETERANGAN <span
                                class=""></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="keterangan_pekerjaan" name="keterangan_pekerjaan"
                                class="form-control">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>

<!-- Modal Upload uraian Kegiatan-->
<div class="modal fade modal_upload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Upload Uraian Kegiatan</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url(); ?>pekerjaan/upload" method="post" enctype="multipart/form-data"
                    role="form">
                    <input type="hidden" value="" name="id_master_pekerjaan" />


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Upload Template
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="file" id="template" name="template" required="" class="form-control" value=""
                                accept=".xlsx">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>



<script type="text/javascript">
var table;
var pathArray = window.location.pathname.split('/');

$(document).ready(function() {
    console.log(pathArray[4]);
    //datatables
    table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        // "order": [], //Initial no order.
        "ordering": false,

        // Load data for the table's content from an Ajax source
        "ajax": {

            "url": "<?php echo site_url('pekerjaan/ajax_list/') ?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [{
            // "targets": [ 0,1,2,3 ], //first column / numbering column
            // "orderable": false, //set not orderable
        }, ],

        initComplete: function() {



        }

    });



});
</script>
<script type="text/javascript">
$(document).ready(function() {

    $(".nooo select").addClass('hidden');
    //Loading.hide(); 

});


$(document).ready(function() {
    $('.pilih').select2();
});
$(document).ready(function() {

    // $('#custom-filter').appendTo("#table_filter label");
    // $('#custom-filter').appendTo(".dataTables_filter label");

});
$(".kategori").change(function() {
    console.log('ganti');
    var value = $('option:selected', this).attr('value');
    console.log(value);
    $("a.kategori_3").each(function() {
        url = $(this).attr("href");
        var last_url = url.substring(url.lastIndexOf('/') + 1);

        if (last_url === '') {
            var $this = $(this);
            var _href = $this.attr("href");
            $this.attr("href", _href + value);
        } else {

            url = url.replace(last_url, value);
            var $this = $(this);
            $this.attr("href", url);
        }

    });
    var val = $.fn.dataTable.util.escapeRegex(
        $(this).val()
    );

    // column
    //     .search( val ? val : '', true, false )
    //     .draw();
    table.columns([1]).search(this.value).draw();

});
</script>


<script>
$(document).ready(function() {
    $('.select2').select2();
});
</script>

<script type="text/javascript">
function confirm_reject() {
    return confirm('Apakah anda yakin akan Reject data ini?');
}

function confirm_approve() {
    return confirm('Apakah anda yakin akan Setujui data ini?');
}
</script>