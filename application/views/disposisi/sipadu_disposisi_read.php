<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Sipadu_disposisi Read</h2>
        <table class="table">
	    <tr><td>Id Surat Masuk</td><td><?php echo $id_surat_masuk; ?></td></tr>
	    <tr><td>Nip Asal</td><td><?php echo $nip_asal; ?></td></tr>
	    <tr><td>Unit Kerja Asal</td><td><?php echo $unit_kerja_asal; ?></td></tr>
	    <tr><td>Nip Tujuan</td><td><?php echo $nip_tujuan; ?></td></tr>
	    <tr><td>Unit Kerja Tujuan</td><td><?php echo $unit_kerja_tujuan; ?></td></tr>
	    <tr><td>Catatan</td><td><?php echo $catatan; ?></td></tr>
	    <tr><td>Timestamp</td><td><?php echo $timestamp; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('disposisi') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>