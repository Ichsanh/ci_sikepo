<div class="content-wrapper">
    
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA SIPADU_DISPOSISI</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">
            
<table class='table table-bordered>'        

	    <tr><td width='200'>Id Surat Masuk <?php echo form_error('id_surat_masuk') ?></td><td><input type="text" class="form-control" name="id_surat_masuk" id="id_surat_masuk" placeholder="Id Surat Masuk" value="<?php echo $id_surat_masuk; ?>" /></td></tr>
	    <tr><td width='200'>Nip Asal <?php echo form_error('nip_asal') ?></td><td><input type="text" class="form-control" name="nip_asal" id="nip_asal" placeholder="Nip Asal" value="<?php echo $nip_asal; ?>" /></td></tr>
	    <tr><td width='200'>Unit Kerja Asal <?php echo form_error('unit_kerja_asal') ?></td><td><input type="text" class="form-control" name="unit_kerja_asal" id="unit_kerja_asal" placeholder="Unit Kerja Asal" value="<?php echo $unit_kerja_asal; ?>" /></td></tr>
	    <tr><td width='200'>Nip Tujuan <?php echo form_error('nip_tujuan') ?></td><td><input type="text" class="form-control" name="nip_tujuan" id="nip_tujuan" placeholder="Nip Tujuan" value="<?php echo $nip_tujuan; ?>" /></td></tr>
	    <tr><td width='200'>Unit Kerja Tujuan <?php echo form_error('unit_kerja_tujuan') ?></td><td><input type="text" class="form-control" name="unit_kerja_tujuan" id="unit_kerja_tujuan" placeholder="Unit Kerja Tujuan" value="<?php echo $unit_kerja_tujuan; ?>" /></td></tr>
	    
        <tr><td width='200'>Catatan <?php echo form_error('catatan') ?></td><td> <textarea class="form-control" rows="3" name="catatan" id="catatan" placeholder="Catatan"><?php echo $catatan; ?></textarea></td></tr>
	    <tr><td width='200'>Timestamp <?php echo form_error('timestamp') ?></td><td><input type="text" class="form-control" name="timestamp" id="timestamp" placeholder="Timestamp" value="<?php echo $timestamp; ?>" /></td></tr>
	    <tr><td></td><td><input type="hidden" name="id_disposisi" value="<?php echo $id_disposisi; ?>" /> 
	    <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i> <?php echo $button ?></button> 
	    <a href="<?php echo site_url('disposisi') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a></td></tr>
	</table></form>        </div>
</div>
</div>