<div class="x_panel">
    <div class="x_content">


        <div class="item ">
            <div class="col-md-2 col-sm-2 ">ID </div>
            <div class="col-md-10 col-sm-10 ">: <?php echo $detail_pekerjaan['id_matriks_kinerja']; ?></div>
        </div>
        <div class="item ">
            <div class="col-md-2 col-sm-2 ">KEGIATAN </div>
            <div class="col-md-10 col-sm-10 ">: <?php echo $detail_pekerjaan['nama_pekerjaan_non_pok']; ?></div>
        </div>
        <div class="item ">
            <div class="col-md-2 col-sm-2 "> SATUAN</div>
            <div class="col-md-10 col-sm-10 ">: <?php echo $detail_pekerjaan['satuan']; ?></div>
        </div>

    </div>
</div>

<div class="x_panel">
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
                <form
                    action="<?= base_url('matriks_kinerja_pok/alokasi_pegawai_tahunan/') . $detail_pekerjaan['id_matriks_kinerja'] ?>"
                    method="post" enctype="multipart/form-data" role="form">


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NAMA PEGAWAI <span
                                class="required">*</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <select class="form-control select2 select2-danger" name="nip_pegawai" id="nip_pegawai"
                                data-dropdown-css-class="select2-danger" style="width: 100%;">
                                <option value=""></option>
                                <?php
                                foreach ($pegawai as $key => $aa) {
                                    echo "<option value='$aa[nip_pegawai]'> $aa[nama_pegawai] </option>";
                                } ?>
                            </select>
                        </div>
                    </div>



                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            KETERANGAN <span class="required"></span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="type" id="keterangan" name="keterangan" class="form-control ">
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-5 col-12">
    <div class="x_panel">
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 01 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_01']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_01'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_01" name="target_bulan_01"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_01"
                                name="target_bulan_01" class="form-control">
                            <?php } ?>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 02 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_02']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_02'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_02" name="target_bulan_02"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_02"
                                name="target_bulan_02" class="form-control">
                            <?php } ?>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 03 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_03']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_03'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_03" name="target_bulan_03"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_03"
                                name="target_bulan_03" class="form-control">
                            <?php } ?>
                        </div>
                    </div>


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 04 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_04']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_04'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_04" name="target_bulan_04"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_04"
                                name="target_bulan_04" class="form-control">
                            <?php } ?>
                        </div>
                    </div>



                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 05 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_05']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_05'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_05" name="target_bulan_05"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_05"
                                name="target_bulan_05" class="form-control">
                            <?php } ?>
                        </div>
                    </div>



                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 06 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_06']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_06'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_06" name="target_bulan_06"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_06"
                                name="target_bulan_06" class="form-control">
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-5 col-12">
    <div class="x_panel">
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 07 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_07']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_07'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_07" name="target_bulan_07"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_07"
                                name="target_bulan_07" class="form-control">
                            <?php } ?>
                        </div>
                    </div>



                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 08 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_08']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_08'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_08" name="target_bulan_08"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_08"
                                name="target_bulan_08" class="form-control">
                            <?php } ?>
                        </div>
                    </div>



                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 09 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_09']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_09'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_09" name="target_bulan_09"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_09"
                                name="target_bulan_09" class="form-control">
                            <?php } ?>
                        </div>
                    </div>



                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 10 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_10']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_10'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_10" name="target_bulan_10"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_10"
                                name="target_bulan_10" class="form-control">
                            <?php } ?>
                        </div>
                    </div>



                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 11 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_11']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_11'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_11" name="target_bulan_11"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_11"
                                name="target_bulan_11" class="form-control">
                            <?php } ?>
                        </div>
                    </div>



                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
                            TARGET 12 <span
                                class="required">(<?php echo $detail_pekerjaan['target_bulan_12']; ?>)</span>
                        </label>
                        <div class="col-md-9 col-sm-9 ">
                            <?php if ($detail_pekerjaan['target_bulan_12'] > 0) { ?>
                            <input type="number" step="0.01" min="0" id="target_bulan_12" name="target_bulan_12"
                                class="form-control">
                            <?php } else { ?>
                            <input disabled="true" type="number" step="0.01" min="0" id="target_bulan_12"
                                name="target_bulan_12" class="form-control">
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-lg-2 col-12">
    <div class="x_panel">
        <div class="x_content">
            <div class="row">
                <div class="col-sm-12">

                    <button onclick="history.go(-1); return false;" class=" btn btn-block btn-danger"> <i
                            class="fa fa-times"></i> Batal</button>
                    <button type="submit" name="simpan" class="btn btn-block btn-success">Submit</button></form>
                </div>
            </div>
        </div>
    </div>


</div>
</form>