<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        if ($this->session->flashdata('sukses') <> '') {
        ?>
        <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('sukses'); ?></p>
        </div>
        <?php
        }
        ?>
        <?php
        if ($this->session->flashdata('gagal') <> '') {
        ?>
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('gagal'); ?></p>
        </div>
        <?php
        }
        ?>
    </div>

</div>

<div class="x_panel">
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">


                <div class="item ">
                    <div class="col-md-2 col-sm-2 ">KEGIATAN </div>
                    <div class="col-md-10 col-sm-10 ">: <?php echo $detail_pekerjaan['nama_pekerjaan_non_pok']; ?></div>
                </div>

                <div class="item ">
                    <div class="col-md-2 col-sm-2 "> SATUAN</div>
                    <div class="col-md-10 col-sm-10 ">: <?php echo $detail_pekerjaan['satuan']; ?></div>
                </div>


                <div class="item ">
                    <div class="col-md-2 col-sm-2 "> TARGET TOTAL</div>
                    <div class="col-md-10 col-sm-10 ">: <?php echo $total_target['sum_total']; ?></div>
                </div>

                <div class="item ">
                    <div class="col-md-2 col-sm-2 "> TARGET PEGAWAI</div>
                    <div class="col-md-10 col-sm-10 ">: <?php echo $jumlah_alokasi['jumlah_alokasi']; ?></div>
                </div>

                <?php if($jumlah_alokasi['jumlah_alokasi']==$total_target['sum_total']){ ?>
                <div class="col-md-2 col-sm-2 "> CATATAN</div>
                <div class="col-md-10 col-sm-10 ">:<?php echo ' Alokasi Sesuai '?></div>
                <?php }else{ ?>
                <div class="item ">
                    <div class="col-md-2 col-sm-2 "> CATATAN</div>
                    <div class="col-md-10 col-sm-10 " style="color:red">

                        : Alokasi Target Belum Sesuai dengan Total Alokasi Pegawai

                    </div>
                </div>

                <?php } ?>

            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-12">
    <td style=" text-align:right;">
        <a>
            <button onclick="history.back(-1)" class="btn  btn-danger btn-sm"> <i class="fa fa-back"></i> back</button>
        </a>
    </td>

    <td style=" text-align:right;">
        <a
            href="<?=base_url('matriks_kinerja_non_pok/alokasi_pegawai_tahunan/').$detail_pekerjaan['id_matriks_kinerja']; ?>">
            <button class="btn btn-info btn-sm"> <i class="fa fa-pencil"></i> Alokasi
            </button>
        </a>
    </td>
</div>


<div class="x_panel">
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <p class="text-muted font-13 m-b-30">
                    </p>
                    <table id="datatable-buttons-fix" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                <th width="1%">
                                    <center> NO
                                </th>
                                <th width="20%">
                                    <center> NAMA PEGAWAI
                                </th>
                                <th width="1%">
                                    <center> 01
                                </th>
                                <th width="1%">
                                    <center> 02
                                </th>
                                <th width="1%">
                                    <center> 03
                                </th>
                                <th width="1%">
                                    <center> 04
                                </th>
                                <th width="1%">
                                    <center> 05
                                </th>
                                <th width="1%">
                                    <center> 06
                                </th>
                                <th width="1%">
                                    <center> 07
                                </th>
                                <th width="1%">
                                    <center> 08
                                </th>
                                <th width="1%">
                                    <center> 09
                                </th>
                                <th width="1%">
                                    <center> 10
                                </th>
                                <th width="1%">
                                    <center> 11
                                </th>
                                <th width="1%">
                                    <center> 12
                                </th>


                            </tr>
                        </thead>


                        <tbody>
                            <?php
                                        $no=1;
                                        foreach ($target_pekerjaan as $key => $q) {
                                        ?>
                            <tr>
                                <td>
                                    <center><?php echo $no++?>
                                </td>
                                <td>
                                    <center><?php echo $q['nama_pegawai']?>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_01']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_02']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_03']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_04']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_05']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_06']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_07']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_08']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_09']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_10']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_11']?></b></a>
                                </td>

                                <td>
                                    <center><a title="alokasi"
                                            href="<?=base_url('matriks_kinerja_non_pok/edit_kinerja_individu/').$q['id_matriks'].'/'.$q['id_skp'];?>">
                                            <b><?php echo $q['target_12']?></b></a>
                                </td>

                            </tr>
                            <?php
                                    }
                                    ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>