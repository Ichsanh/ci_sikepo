<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>List Surat Masuk</h2>


            <ul class="nav navbar-right panel_toolbox">
                <li> <a href="<?=base_url('surat_masuk/create')?>">
                        <button class="btn btn-primary btn-sm " class="title_right" data-toggle="modal"
                            data-target=".modal_tambah">+ Tambah</button>
                    </a>

                </li>


            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <?php if (!empty($surat)) {  ?>
                <td style="text-align:right;">
                    <!-- <button type="button" class="btn btn-primary btn-sm " class="title_right" data-toggle="modal" data-target=".modal_tambah">+ butir</button> -->
                </td>
                <?php } ?>

                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <p class="text-muted font-13 m-b-30">
                        </p>


                    </div>

                    <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>No Surat</th>
                                <th>Pengirim</th>
                                <th>Perihal</th>
                                <th>Tujuan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
            foreach ($surat_masuk_data as $surat_masuk)
            {
                ?>
                            <tr>
                                <td width="10px"><?php echo ++$start ?></td>
                                <td><?php echo $surat_masuk->kategori ?></td>
                                <td><?php echo $surat_masuk->unit_kerja ?></td>
                                <td><?php echo $surat_masuk->perihal ?></td>
                                <td><?php echo $surat_masuk->tujuan ?></td>
                                <td><?php echo $surat_masuk->nip ?></td>
                                <td><?php echo $surat_masuk->timestamp ?></td>
                                <td><?php echo $surat_masuk->file_surat ?></td>
                                <td><?php echo $surat_masuk->tanggal ?></td>
                                <td><?php echo $surat_masuk->no_surat ?></td>
                                <td><?php echo $surat_masuk->tanggal_upload ?></td>
                                <td><?php echo $surat_masuk->pengirim ?></td>
                                <td style="text-align:center">
                                    <?php 
				echo anchor(site_url('surat_masuk/read/'.$surat_masuk->id_surat_masuk),'<i class="fa fa-eye" aria-hidden="true"></i>','class="btn btn-danger btn-sm"'); 
				echo '  '; 
				echo anchor(site_url('surat_masuk/update/'.$surat_masuk->id_surat_masuk),'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>','class="btn btn-danger btn-sm"'); 
				echo '  '; 
				echo anchor(site_url('surat_masuk/delete/'.$surat_masuk->id_surat_masuk),'<i class="fa fa-trash-o" aria-hidden="true"></i>','class="btn btn-danger btn-sm" Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
                                </td>
                            </tr>
                            <?php
            }
            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>