<div class="content-wrapper">
    
    <section class="content">
        <div class="box box-warning box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">INPUT DATA SURAT_MASUK</h3>
            </div>
            <form action="<?php echo $action; ?>" method="post">
            
<table class='table table-bordered>'        

	    <tr><td width='200'>Kategori <?php echo form_error('kategori') ?></td><td><input type="text" class="form-control" name="kategori" id="kategori" placeholder="Kategori" value="<?php echo $kategori; ?>" /></td></tr>
	    <tr><td width='200'>Unit Kerja <?php echo form_error('unit_kerja') ?></td><td><input type="text" class="form-control" name="unit_kerja" id="unit_kerja" placeholder="Unit Kerja" value="<?php echo $unit_kerja; ?>" /></td></tr>
	    
        <tr><td width='200'>Perihal <?php echo form_error('perihal') ?></td><td> <textarea class="form-control" rows="3" name="perihal" id="perihal" placeholder="Perihal"><?php echo $perihal; ?></textarea></td></tr>
	    <tr><td width='200'>Tujuan <?php echo form_error('tujuan') ?></td><td><input type="text" class="form-control" name="tujuan" id="tujuan" placeholder="Tujuan" value="<?php echo $tujuan; ?>" /></td></tr>
	    <tr><td width='200'>Nip <?php echo form_error('nip') ?></td><td><input type="text" class="form-control" name="nip" id="nip" placeholder="Nip" value="<?php echo $nip; ?>" /></td></tr>
	    <tr><td width='200'>Timestamp <?php echo form_error('timestamp') ?></td><td><input type="text" class="form-control" name="timestamp" id="timestamp" placeholder="Timestamp" value="<?php echo $timestamp; ?>" /></td></tr>
	    <tr><td width='200'>File Surat <?php echo form_error('file_surat') ?></td><td><input type="text" class="form-control" name="file_surat" id="file_surat" placeholder="File Surat" value="<?php echo $file_surat; ?>" /></td></tr>
	    <tr><td width='200'>Tanggal <?php echo form_error('tanggal') ?></td><td><input type="date" class="form-control" name="tanggal" id="tanggal" placeholder="Tanggal" value="<?php echo $tanggal; ?>" /></td></tr>
	    <tr><td width='200'>No Surat <?php echo form_error('no_surat') ?></td><td><input type="text" class="form-control" name="no_surat" id="no_surat" placeholder="No Surat" value="<?php echo $no_surat; ?>" /></td></tr>
	    <tr><td width='200'>Tanggal Upload <?php echo form_error('tanggal_upload') ?></td><td><input type="text" class="form-control" name="tanggal_upload" id="tanggal_upload" placeholder="Tanggal Upload" value="<?php echo $tanggal_upload; ?>" /></td></tr>
	    <tr><td width='200'>Pengirim <?php echo form_error('pengirim') ?></td><td><input type="text" class="form-control" name="pengirim" id="pengirim" placeholder="Pengirim" value="<?php echo $pengirim; ?>" /></td></tr>
	    <tr><td></td><td><input type="hidden" name="id_surat_masuk" value="<?php echo $id_surat_masuk; ?>" /> 
	    <button type="submit" class="btn btn-danger"><i class="fa fa-floppy-o"></i> <?php echo $button ?></button> 
	    <a href="<?php echo site_url('surat_masuk') ?>" class="btn btn-info"><i class="fa fa-sign-out"></i> Kembali</a></td></tr>
	</table></form>        </div>
</div>
</div>