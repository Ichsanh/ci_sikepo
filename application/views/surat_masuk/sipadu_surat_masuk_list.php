<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>List Surat Masuk</h2>


            <ul class="nav navbar-right panel_toolbox">
                <li> <a href="<?=base_url('surat_masuk/create')?>">
                        <button class="btn btn-primary btn-sm " class="title_right" data-toggle="modal"
                            data-target=".modal_tambah">+ Tambah</button>
                    </a>

                </li>


            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <?php if (!empty($surat)) {  ?>
                <td style="text-align:right;">
                    <!-- <button type="button" class="btn btn-primary btn-sm " class="title_right" data-toggle="modal" data-target=".modal_tambah">+ butir</button> -->
                </td>
                <?php } ?>

                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <p class="text-muted font-13 m-b-30">
                        </p>


                    </div>

                    <table class="table table-striped table-bordered" style="width:100%" id="mytable">
                        <thead>
                            <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                <th width="30px">No</th>
                                <th>Kategori</th>
                                <th>Unit Kerja Tujuan</th>
                                <th>Perihal</th>
                                <th>Nip</th>
                                <th>Timestamp</th>
                                <th>File Surat</th>
                                <th>Tanggal Surat</th>
                                <th>No Surat</th>
                                <th>Tanggal Upload</th>
                                <th>Instansi</th>
                                <th width="200px">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script> -->
<script type="text/javascript">
$(document).ready(function() {
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var t = $("#mytable").dataTable({
        initComplete: function() {
            var api = this.api();
            $(' #mytable_filter input').off('.DT').on('keyup.DT', function(e) {
                if (e.keyCode == 13) {
                    api.search(this.value).draw();
                }
            });
        },
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        ajax: {
            "url": "<?php echo base_url('surat_masuk/json') ?>",
            "type": "POST"
        },
        columns: [{
            "data": "id_surat_masuk",
            "orderable": false
        }, {
            "data": "kategori"
        }, {
            "data": "unit_kerja_tujuan"
        }, {
            "data": "perihal"
        }, {
            "data": "nip"
        }, {
            "data": "timestamp"
        }, {
            "data": "file_surat"
        }, {
            "data": "tanggal_surat"
        }, {
            "data": "no_surat"
        }, {
            "data": "tanggal_upload"
        }, {
            "data": "instansi"
        }, {
            "data": "action",
            "orderable": false,
            "className": "text-center"
        }],
        order: [
            [0, 'desc']
        ],
        rowCallback: function(row,
            data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var
                length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)',
                row).html(index);
        }
    });
});
</script>