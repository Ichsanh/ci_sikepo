<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        if ($this->session->flashdata('sukses') <> '') {
        ?>
        <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('sukses'); ?></p>
        </div>
        <?php
        }
        ?>
        <?php
        if ($this->session->flashdata('gagal') <> '') {
        ?>
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('gagal'); ?></p>
        </div>
        <?php
        }
        ?>
    </div>

</div>
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>Membuat No Surat</h2>
            <ul class="nav navbar-right panel_toolbox">
                <!-- <a href="<?= base_url() ?>surat/download_template">
                    <button class="btn btn-sm btn-block btn-success"><i class="fa fa-download"></i> Template</button>
                </a>
                &nbsp;
                <a>
                    <button class="btn btn-sm btn-block btn-primary " data-toggle="modal" data-target=".modal_upload"><i
                            class="fa fa-upload"></i> Template</button>
                </a> -->
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form action="<?= base_url(); ?>surat/tambah_surat" method="post" enctype="multipart/form-data" role="form">
                <div class="col-lg-12 col-12">
                    <div class="card card-grey">
                        <div class="card-body">
                            <div class="box-header">

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kategori</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="kategori"
                                            id="kategori" require>
                                            <option value=""> Pilih Kategori
                                            </option>
                                            <option value="RB"> RB
                                            </option>
                                            <option value="Internal"> Internal
                                            </option>
                                            <option value="Eksternal"> Eksternal
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal</label>
                                    <div class="col-sm-8">
                                        <input type="date" id="tanggal" name="tanggal" required="" class="form-control"
                                            value="">
                                    </div>

                                </div>

                                <div class="form-group row" id="unit_kerja">

                                    <label class="col-sm-2 col-form-label">Unit Kerja</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="unit_kerja"
                                            id="unit_kerja" data-dropdown-css-class="select2-danger"
                                            style="width: 100%;">

                                            <?php
                                            foreach ($unit_kerja as $key => $value) {
                                            ?>
                                            <option value="<?= $value['kode_unit_kerja_internal']; ?>">
                                                <?= $value['nama_unit_kerja']; ?>
                                            </option>
                                            <?php

                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Perihal</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="perihal" name="perihal" required="" class="form-control"
                                            value="">
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tujuan</label>
                                    <div class="col-sm-8">
                                        <input type="text" id="tujuan" name="tujuan" required="" class="form-control"
                                            value="">
                                    </div>

                                </div>

                                <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-refresh"></i>
                                    Submit</button>
                            </div>
                        </div>
                        <?php
                        if ($this->session->flashdata('no_surat_baru') <> '') {
                        ?>
                        <div class="card card-grey" id="no_surat_baru">
                            <div class="card-body">
                                ini no surat yang anda buat: <b>
                                    <h2><?= $this->session->flashdata('no_surat_baru'); ?></h2>
                                </b>
                            </div>
                        </div>
                        <?php
                        }
                        ?>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<?php if (!empty($surat)) {  ?>
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>List Surat</h2>

            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <?php if (!empty($surat)) {  ?>
                <td style="text-align:right;">
                    <!-- <button type="button" class="btn btn-primary btn-sm " class="title_right" data-toggle="modal" data-target=".modal_tambah">+ butir</button> -->
                </td>
                <?php } ?>

                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <p class="text-muted font-13 m-b-30">
                        </p>
                        <table id="datatable-buttons" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                    <th width="1%">
                                        <center> NO
                                    </th>
                                    <th width="1%">
                                        <center> TANGGAL
                                    </th>
                                    <th width="1%">
                                        <center> NO SURAT
                                    </th>
                                    <th width="1%">
                                        <center> KATEGORI
                                    </th>
                                    <th width="50%">
                                        <center> PERIHAL
                                    </th>
                                    <th width="3%">
                                        <center> TUJUAN
                                    </th>
                                    <th width="3%">
                                        <center> PEMBUAT
                                    </th>
                                    <th width="3%">
                                        <center> AKSI
                                    </th>

                                    <!-- <th width="3%">
                                            <center> AKSI
                                        </th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 1;

                                    foreach ($surat as $key => $q) {

                                    ?>
                                <tr>
                                    <td>
                                        <center><?php echo $no++ ?>
                                    </td>
                                    <td>
                                        <left><?php echo $q['tanggal'] ?>
                                    </td>
                                    <td>
                                        <!--  format RB: B-001/RB/BPS/1100/04/2021
	                                          format umum: B.001/BPS/11560/4/2021 -->
                                        <left>
                                            <?php echo $q['no_surat']; ?>
                                    </td>
                                    <td>

                                        <left><?php echo $q['kategori'] ?>
                                    </td>
                                    <td>
                                        <left><?php echo $q['perihal'] ?>
                                    </td>
                                    <td>
                                        <left><?php echo $q['tujuan'] ?>
                                    </td>
                                    <td>
                                        <left><?php echo $q['nama_pegawai'] ?>
                                    </td>
                                    <td>

                                        <span data-toggle='modal' data-target='.modal_upload'
                                            data-id_surat="<?php echo $q['id_surat']; ?>"
                                            data-no_surat="<?php echo $q['no_surat']; ?>">
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top"
                                                title="upload surat"><i class="fa fa-upload fa-lg"
                                                    style="color: blue"></i></a>
                                        </span>
                                        <?php
                                        if (!empty($q['file_surat'])) {
                                        ?>
                                        <span>
                                            <a href="<?= base_url(); ?>surat/download/<?=$q['id_surat']."/".$q['file_surat']?>"
                                                data-toggle="tooltip" data-placement="top" title="download surat"><i
                                                    class="fa fa-download fa-lg" style="color: green"></i></a>
                                        </span>

                                        <?php
                                        }
                                        ?>

                                    </td>

                                    <!-- <td style="text-align:right;">
                                                <center>

                                                    <span data-toggle='modal' data-target='.modal_edit' data-id_butir_fungsional="<?php echo $q['id_butir']; ?>" data-nama_butir_fungsional="<?php echo $q['pekerjaan_rincian']; ?>">
                                                        <a data-toggle="tooltip" data-placement="top" title="edit surat"><i class="fa fa-edit fa-lg"></i></a>
                                                    </span>

                                                    <a onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" href="<?= base_url('surat/hapus/') . $q['id_butir'] ?>" data-toggle="tooltip" data-placement="top" title="hapus surat"><i class="  fa fa-trash fa-lg"></i></a>

                                            </td> -->


                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>


<!-- Modal Upload Kegiatan-->
<div class="modal fade modal_upload" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Upload Surat</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url(); ?>surat/upload" method="post" enctype="multipart/form-data" role="form">
                    <input type="hidden" value="" name="id_surat" id="id_surat" />
                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">No Surat
                            <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="no_surat" name="no_surat" class="form-control" value="" readonly>
                        </div>
                    </div>

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Upload Surat
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="file" id="file_surat" name="file_surat" required="" class="form-control"
                                value="" accept=".pdf">
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>



<script type="text/javascript">
var htmlobjek;
$(document).ready(function() {

    $("#kategori").change(function() {
        var kategori = $("#kategori").val();
        if (kategori != "RB") {
            $('#unit_kerja').show();
        } else {
            $('#unit_kerja').hide();
        }
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {

    $('.modal_upload').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#id_surat').attr("value", div.data('id_surat'));
        modal.find('#no_surat').attr("value", div.data('no_surat'));
        //modal.find('#email_to').attr("value",div.data(''));

    });

});
</script>