<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">

        <ul class="nav side-menu">
            <li><a href="<?= base_url('home'); ?>"><i class="fa fa-home"></i> Home </a></li>

            <li><a href="<?= base_url('ckp'); ?>"><i class="fa fa-table"></i> CKP</a></li>
            <li><a><i class="fa fa-gears"></i> Master <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?= base_url('butir_fungsional'); ?>"><i class="fa fa-bookmark"></i> Butir
                            Fungsional</a></li>
                    <li><a href="<?= base_url('kegiatan'); ?>"><i class="fa fa-bookmark"></i> Kegiatan </a></li>
                    <li><a href="<?= base_url('pekerjaan'); ?>"><i class="fa fa-bookmark"></i> Uraian Kegiatan </a></li>
                    <li><a href="<?= base_url('pegawai'); ?>"><i class="fa fa-users"></i> Pegawai </a></li>
                    <li><a href="<?= base_url('pok'); ?>"><i class="fa fa-bookmark"></i> POK </a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-desktop"></i> Report <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?= base_url('matriks_kinerja_pok'); ?>"><i class="fa fa-table"></i> Matriks Kinerja
                        </a></li>
                    <li><a href="<?= base_url('matriks_kinerja_non_pok'); ?>"><i class="fa fa-table"></i> Matriks
                            Kinerja Non POK
                        </a></li>
                    <li><a href="<?= base_url('report/matriks_kinerja_1'); ?>"><i class="fa fa-table"></i> Report
                            Matriks
                            Kinerja (1)</a></li>
                    <li><a href="<?= base_url('report/matriks_kinerja_2'); ?>"><i class="fa fa-table"></i> Report
                            Matriks
                            Kinerja (2)</a></li>
                    <li><a href="<?= base_url('report/matriks_kinerja_3'); ?>"><i class="fa fa-table"></i> Report
                            Matriks
                            Kinerja (3)</a></li>
                    <li><a href="<?= base_url('report/matriks_kinerja_4'); ?>"><i class="fa fa-table"></i> Report
                            Matriks
                            Kinerja (4)</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-envelope"></i> Surat <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="<?= base_url('surat'); ?>">Surat</a></li>
                    <li><a href="<?= base_url('surat_masuk'); ?>">Surat Masuk</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="tables.html">Tables</a></li>
                    <li><a href="tables_dynamic.html">Table Dynamic</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="chartjs.html">Chart JS</a></li>
                    <li><a href="chartjs2.html">Chart JS2</a></li>

                </ul>
            </li>
            <li><a><i class="fa fa-clone"></i>Layouts <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="fixed_sidebar.html">Fixed Sidebar</a></li>
                    <li><a href="fixed_footer.html">Fixed Footer</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="menu_section">
        <h3>Live On</h3>
        <ul class="nav side-menu">
            <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="e_commerce.html">E-commerce</a></li>
                    <li><a href="projects.html">Projects</a></li>

                </ul>
            </li>
            <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="page_403.html">403 Error</a></li>
                    <li><a href="page_404.html">404 Error</a></li>

                </ul>
            </li>
            <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#level1_1">Level One</a>
                    <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>

                        </ul>
                    </li>
                    <li><a href="#level1_2">Level One</a>
                    </li>
                </ul>
            </li>
            <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span
                        class="label label-success pull-right">Coming Soon</span></a></li>
        </ul>
    </div>

</div>