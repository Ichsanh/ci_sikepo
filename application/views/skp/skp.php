<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        if ($this->session->flashdata('sukses') <> '') {
        ?>
        <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('sukses'); ?></p>
        </div>
        <?php
        }
        ?>
        <?php
        if ($this->session->flashdata('gagal') <> '') {
        ?>
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span>
            </button>
            <p><?php echo $this->session->flashdata('gagal'); ?></p>
        </div>
        <?php
        }
        ?>
    </div>

</div>
<div class="row">
    <div class="x_panel">
        <div class="x_title">
            <h2>List Kegiatan</h2>
            <ul class="nav navbar-right panel_toolbox">
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <div class="col-lg-12 col-12">
                <div class="card card-grey">
                    <div class="card-body">
                        <div class="box-header">
                            <form action="<?= base_url() ?>skp/lihat_skp" method="get" role="form">
                                <div class="form-group row">


                                    <label class="col-sm-2 col-form-label">PEGAWAI</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="nip_pegawai"
                                            id="nip_pegawai" data-dropdown-css-class="select2-danger"
                                            style="width: 100%;">
                                            <?php
                                            if (empty($pegawai_terpilih)) {
                                                echo '<option value="">Pilih Pegawai</option>';
                                            } else { ?>
                                            <option value="<?php echo $pegawai_terpilih['nip_pegawai']; ?>">
                                                <?php echo $pegawai_terpilih['nama_pegawai']; ?></option>
                                            <?php }
                                            ?>
                                            <?php
                                            foreach ($pegawai as $key => $value) {
                                            ?>
                                            <option value="<?= $value['nip_pegawai']; ?>">
                                                <?= $value['nama_pegawai']; ?>
                                            </option>
                                            <?php

                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">TAHUN</label>
                                    <div class="col-sm-8">
                                        <select class="form-control select2 select2-danger" name="tahun" id="tahun"
                                            data-dropdown-css-class="select2-danger" style="width: 100%;">
                                            <?php
                                            if (empty($pegawai_terpilih)) {
                                                echo '<option value="">Pilih Tahun</option>';
                                                for ($a = 2021; $a < 2025; $a++) { ?>
                                            <option value="<?= $a; ?>">
                                                <?= $a; ?>
                                            </option>
                                            <?php


                                                }
                                            } else { ?>
                                            <option value="<?php echo $tahun; ?>">
                                                <?php echo $tahun; ?></option>
                                            <?php
                                                for ($a = 2021; $a < 2025; $a++) { ?>
                                            <option value="<?= $a; ?>">
                                                <?= $a; ?>
                                            </option>
                                            <?php

                                                }
                                                ?>
                                            <?php }
                                            ?>

                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-block btn-primary"><i
                                                class="fa fa-refresh"></i> Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if (!empty($pegawai_terpilih)) { ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form action="<?= base_url(); ?>skp/update_ckp" method="post" enctype="multipart/form-data" role="form">
            <div class="x_panel">
                <div class="x_content">
                    <div class="row">



                        <div class="col-xs-1">

                            <!-- <?php echo "<input type='submit' class='btn btn-sm   btn-primary' value='SIMPAN'>"; ?> -->
                            <?php echo "<a href='".base_url()."ckp/download_ckp/".$nip_pegawai."/".$tahun."' class='btn btn-sm   btn-success'>DOWNLOAD</a>"; ?>
                        </div>

                        <div class="col-sm-12">
                            <div class="card-box ">
                                <h3>

                                    <p class="text-muted ">
                                        <!-- Kegiatan Utama -->
                                    </p>
                                </h3>
                                <table class="table table-striped  table-bordered table-responsive" style="width:100%">
                                    <thead>
                                        <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                            <th width="1%" rowspan="2" style="vertical-align: middle;">
                                                <center> NO
                                            </th>

                                            <th width="10%" rowspan="2" style="vertical-align: middle;">
                                                <center> KEGIATAN TUGAS JABATAN
                                            </th>
                                            <th width="1%" rowspan="2" style="vertical-align: middle;">
                                                <center> AK
                                            </th>
                                            <!-- <th width="1%">
                                            <center> BULAN
                                        </th> -->
                                            <th colspan="6">
                                                <center> TARGET
                                            </th>

                                        </tr>
                                        <tr style="background-color: #2a3f54; height:10px; color: #ffffff;">
                                            <th colspan="2" width="1%">
                                                <center> KUANT/OUTPUT
                                            </th>
                                            <th width="1%">
                                                <center> KUAL/MUTU
                                            </th>
                                            <th colspan="2" width="1%">
                                                <center> WAKTU
                                            </th>
                                            <th width="1%">
                                                <center> BIAYA
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                    $no = 0;
                                    $i = 0;
                                    if (!empty($data)) {


                                        foreach ($data as $q) {

                                            $no++;
                                    ?>
                                        <tr>
                                            <td><?= $no; ?></td>

                                            <td>
                                                <LEFT>
                                                    <?= $q['nama_pekerjaan_non_pok']; ?><?= $q['rincian_pekerjaan']; ?>
                                                    <?= $q['nama_kegiatan']; ?>
                                            </td>
                                            <td>
                                                <LEFT><?= $q['ak']; ?>
                                            </td>
                                            <td>
                                                <LEFT><?= $q['sum_target']; ?>
                                            </td>
                                            <td>
                                                <LEFT><?= $q['satuan']; ?>
                                            </td>
                                            <td>
                                                <LEFT><?= $q['kualitas']; ?>
                                            </td>
                                            <td>
                                                <LEFT><?= $q['waktu']; ?>
                                            </td>
                                            <td>
                                                <LEFT>Bulan
                                            </td>
                                            <td>
                                                <LEFT>-
                                            </td>
                                        </tr>
                                        <?php $i++;
                                        }
                                    }   ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
}
?>



<!-- Modal edit Kegiatan-->
<div class="modal fade modal_link_ak" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Link Angka Kredit</h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url(); ?>ckp/link_ak" method="post" enctype="multipart/form-data" role="form">
                    <input type="hidden" value="" name="id_skp" id="id_skp" />
                    <input type="hidden" value="" name="nip_pegawai" id="nip_pegawai" />

                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Uraian kegiatan

                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="uraian_kegiatan" name="uraian_kegiatan" required=""
                                class="form-control" value="" disabled>
                        </div>
                    </div>


                    <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">AK
                            <span class="">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">

                            <select class="kategori pilih select2 form-control" name="id_butir" style="width: 100%;">

                                <option value="">Pilih Butir Fungsional</option>
                                <?php
                                    foreach ($butir_fungsional as $key => $value) {
                                    ?>
                                <option value="<?= $value['id_butir']; ?>">
                                    <?= '['.$value['butir_fungsional'].'] '.$value['pekerjaan_rincian'].' ('.$value['jenjang_fungsional'].')'; ?>
                                </option>
                                <?php

                                    }
                                    ?>
                            </select>
                        </div>

                    </div>
                    <!-- <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">KETERANGAN <span class=""></span>
                        </label>
                        <div class="col-md-6 col-sm-6 ">
                            <input type="text" id="keterangan_pekerjaan" name="keterangan_pekerjaan" class="form-control">
                        </div>
                    </div> -->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {

    $('.modal_link_ak').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#uraian_kegiatan').attr("value", div.data('uraian_kegiatan'));
        modal.find('#id_skp').attr("value", div.data('id_skp'));
        modal.find('#nip_pegawai').attr("value", div.data('nip_pegawai'));
        //modal.find('#email_to').attr("value",div.data(''));

    });

});
</script>