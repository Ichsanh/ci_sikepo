<?php

// extends class Model
class Master_butir_fungsional_model extends CI_Model
{

    public function get_all()
    {
        return  $this->db->select('*')->from("master_butir_fungsional as a")->join('master_jabatan_fungsional as b', 'a.pekerjaan_pelaksana=b.kode_jabatan_fungsional')->get()->result_array();
    }

    public function get_fungsional($kode_jenis_jabatan_fungsional)
    {
        return  $this->db->select('*')->from("master_butir_fungsional as a")->join('master_jabatan_fungsional as b', 'a.pekerjaan_pelaksana=b.kode_jabatan_fungsional')->where('a.kode_jenis_jabatan_fungsional', $kode_jenis_jabatan_fungsional)->get()->result_array();
    }

    public function get_butir_tingkat_jabatan($kode_jenis_jabatan_fungsional, $tingkat_jabatan)
    {
        return  $this->db->select('*')->from("master_butir_fungsional as a")->join('master_jabatan_fungsional as b', 'a.pekerjaan_pelaksana=b.kode_jabatan_fungsional')->where('a.kode_jenis_jabatan_fungsional', $kode_jenis_jabatan_fungsional)->where('b.tingkat_jabatan', $tingkat_jabatan)->get()->result_array();
    }

    public function get_id($id_butir)
    {
        return  $this->db->select('*')->from("master_butir_fungsional as a")->join('master_jabatan_fungsional as b', 'a.pekerjaan_pelaksana=b.kode_jabatan_fungsional')->where('a.id_butir',$id_butir)->get()->row_array();
    }

    function tambah_dummy($params)
    {
        return $this->db->insert('dummy_master_butir_fungsional', $params);
    }
    function cek_duplikat()
    {
        $this->db->query("UPDATE dummy_master_butir_fungsional
        INNER JOIN master_butir_fungsional ON master_butir_fungsional.pekerjaan_rincian = dummy_master_butir_fungsional.pekerjaan_rincian 
        and master_butir_fungsional.pekerjaan_pelaksana = dummy_master_butir_fungsional.pekerjaan_pelaksana 
        SET dummy_master_butir_fungsional.status = 1");

        $this->db->query('INSERT INTO master_butir_fungsional (kode_jenis_jabatan_fungsional,butir_fungsional_grouping,butir_fungsional,pekerjaan_rincian,pekerjaan_volume,pekerjaan_pelaksana,ak,filter)  
        SELECT kode_jenis_jabatan_fungsional,butir_fungsional_grouping,butir_fungsional,pekerjaan_rincian,pekerjaan_volume,pekerjaan_pelaksana,ak,filter
        FROM dummy_master_butir_fungsional WHERE status != 1');
        
        return $this->db->select('*')->from('dummy_master_butir_fungsional')->where('status',1)->get()->result_array();
    }
}