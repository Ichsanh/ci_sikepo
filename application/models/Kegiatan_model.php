<?php

// extends class Model
class Kegiatan_model extends CI_Model
{

    public function get_all()
    {
        return  $this->db->select('*')->from("tabel_master_kegiatan")
        ->join('tabel_pok','tabel_pok.id_pok=tabel_master_kegiatan.id_pok')->get()->result_array();
    }

    public function get_kegiatan_pok($kode_pok)
    {
        return  $this->db->select('*,a.id_kegiatan as id_kegiatan,a.id_pok as id_pok, count(b.id_kegiatan) as jumlah_kegiatan_matriks')->from("tabel_master_kegiatan as a")->join('tabel_matriks_kinerja as b', 'a.id_kegiatan=b.id_kegiatan', 'left')->where('a.id_pok', $kode_pok)->group_by('a.id_kegiatan')->get()->result_array();
    }

    function tambah($params)
    {

        return $this->db->insert('tabel_master_kegiatan', $params);
    }
    function tambah_dummy($params)
    {
        return $this->db->insert('dummy_tabel_master_kegiatan', $params);
    }
    function cek_duplikat()
    {
        $this->db->query("UPDATE dummy_tabel_master_kegiatan
        INNER JOIN tabel_master_kegiatan ON tabel_master_kegiatan.nama_kegiatan = dummy_tabel_master_kegiatan.nama_kegiatan
        SET dummy_tabel_master_kegiatan.status = 1");

        $this->db->query('INSERT INTO tabel_master_kegiatan (id_pok, nama_kegiatan, flag_jenis_kegiatan, jenis_kegiatan)  
        SELECT id_pok, nama_kegiatan, flag_jenis_kegiatan, jenis_kegiatan
        FROM dummy_tabel_master_kegiatan WHERE status != 1');
        
        return $this->db->select('*')->from('dummy_tabel_master_kegiatan')->where('status',1)->get()->result_array();
    }
    public function get_kegiatan_id($id_kegiatan)
    {
        return  $this->db->select('*')->from("tabel_master_kegiatan")->where('id_kegiatan', $id_kegiatan)->get()->row_array();
    }
    function update($id, $params)
    {
        $this->db->where('id_kegiatan', $id);
        return $this->db->update('tabel_master_kegiatan', $params);
    }

    function hapus($id)
    {
        $this->db->where('id_kegiatan', $id);
        return $this->db->delete('tabel_master_kegiatan');
    }
}