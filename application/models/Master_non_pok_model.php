<?php

// extends class Model
class Master_non_pok_model extends CI_Model
{

    public function get_all()
    {
        return $this->db->select('*')->from("tabel_non_pok")->where('tabel_non_pok.flag', '4')->get()->result_array();
    }

    public function get_id($id_non_pok)
    {
        return  $this->db->select('*')->from("tabel_non_pok")->where('id_non_pok', $id_non_pok)->get()->row_array();
    }
}