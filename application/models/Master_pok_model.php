<?php

// extends class Model
class Master_pok_model extends CI_Model
{

    public function get_all()
    {
        return $this->db->select('*,count(tabel_master_kegiatan.id_pok) as jumlah_pekerjaan')->from("tabel_pok")
            ->join('tabel_master_kegiatan', 'tabel_pok.id_pok=tabel_master_kegiatan.id_pok', 'left')
            ->join('master_satker', 'tabel_pok.satker=master_satker.kode_satker', 'left')
            ->join('master_bidang', 'tabel_pok.bidang=master_bidang.kode_bidang', 'left')
            ->where('tabel_pok.flag', '4')
            ->group_by('tabel_pok.id_pok')
            ->get()->result_array();
    }

    public function get_pok_bidang($kode_bidang)
    {
        return $this->db->select('*')->from('tabel_pok')->where('bidang', $kode_bidang)->where('flag', 4)->get()->result_array();
    }

    public function get_id($id_pok)
    {
        return  $this->db->select('*')->from("tabel_pok")->where('id_pok', $id_pok)->get()->row_array();
    }
}
