<?php

// extends class Model
class Master_bidang_model extends CI_Model
{

    public function get_all()
    {
        return  $this->db->select('*')->from("master_bidang")->get()->result_array();
    }

    public function get_id($kode_bidang)
    {
        return  $this->db->select('*')->from("master_bidang")->where('kode_bidang', $kode_bidang)->get()->row_array();
    }
}
