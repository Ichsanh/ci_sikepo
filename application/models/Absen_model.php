<?php

// extends class Model
class Absen_model extends CI_Model
{
    public function get_absen($nip_pegawai_lama, $tahun, $bulan)
    {
        $all = $this->db->select('*')->from("geo_absen")->where('nip', $nip_pegawai_lama)->where('tahun', $tahun)->where('bulan', $bulan)->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['absenData'] = $all;
        return $response;
    }

    public function insert_absen($nip,
            $address,
            $latitude,
            $longitude,
            $accuracy, $catatan)
    {
        $data = array(
            "nip"=> $nip,
            "address"=> $address,
            "latitude"=> $latitude,
            "longitude"=> $longitude,
            "accuracy"=> $accuracy,
            "catatan"=> $catatan,
            "tahun" => date('Y'),
            "bulan" => date('m'),
            "tanggal" => date('d'),
            "waktu" => date('H:i'),
            "pagi" => (date('H:i')<= "12:00") ? 1 : 0, 

            );

        $insert = $this->db->insert("geo_absen", $data);
        $response['status'] = 200;
        $response['error'] = false;
        $response['locData'] = $insert;
        return $response;
    }

    public function get_rekap_nip($nip)
    {
        // $nip = 1001;
        $all = $this->db->select('*')->from("geo_absen")->where('nip', $nip)->order_by('timestamp', "desc")->get()->result();
        $response['status'] = 200;
        $response['error'] = false;
        $response['nip'] = $nip;
        $response['rekapData'] = ($all);
        return $response;
    }

    public function get_rekap_harian($nip)
    {
        $tahun = date('Y');
        $bulan = date('m');
        $tanggal = date('d');
        $address = "address";
        $pagi = $this->db->select('*')->from("geo_absen")->where('nip', $nip)->where('tanggal', $tanggal)->where('bulan', $bulan)->where('tahun', $tahun)->where('pagi', 1)->like('address', $address)->order_by('timestamp', "asc")->get()->row_array();
        $sore = $this->db->select('*')->from("geo_absen")->where('nip', $nip)->where('tanggal', $tanggal)->where('bulan', $bulan)->where('tahun', $tahun)->where('pagi', 0)->like('address', $address)->order_by('timestamp', "desc")->get()->row_array();
        $all = array(
            "pagi" => $pagi['waktu'],
            "sore" => $sore['waktu'],
        );

        $response['status'] = 200;
        $response['error'] = false;
        $response['rekapData'] = json_encode($all);
        return $response;
    }

    
    
}