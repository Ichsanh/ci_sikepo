<?php

// extends class Model
class Pengguna_model extends CI_Model
{
    public function login_pengguna($nip, $password)
    {
        $all = $this->db->select('*')->from("geo_pengguna")->where('nip', $nip)->where('password', $password)->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['userData'] = $all;
        return $response;
    }

    public function update_password($nip, $password)
    {
        $this->db->set('password', $password);
        $this->db->where('nip', $nip);
        $all = $this->db->update('geo_pengguna');
        // $all = $this->db->select('*')->from("geo_pengguna")->where('nip', $nip)->where('password', $password)->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['passwordData'] = $all;
        return $response;
    }
}