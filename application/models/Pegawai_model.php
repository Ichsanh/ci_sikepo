<?php

// extends class Model
class Pegawai_model extends CI_Model
{

    public function get_pegawai()
    {
        $this->db->select('*')->from("master_pegawai")
            ->join('master_unit_kerja', 'master_pegawai.kode_unit_kerja=master_unit_kerja.kode_unit_kerja')
            ->join('master_satker', 'master_pegawai.kode_satker=master_satker.kode_satker')
            ->join('master_pangkat', 'master_pegawai.pangkat=master_pangkat.gol_pangkat', 'left')
            ->join('master_jabatan_struktur', 'master_pegawai.kode_jabatan_struktur=master_jabatan_struktur.kode_jabatan_struktur', 'left')
            ->join('master_jabatan_fungsional', 'master_pegawai.kode_jabatan_fungsional=master_jabatan_fungsional.kode_jabatan_fungsional', 'left');

        if ($this->session->userdata('level_user') === 'superadmin') {
            # code...
        } elseif ($this->session->userdata('level_user') === 'admin_prov') {
            $this->db->where('flag_user', 0);
        } elseif ($this->session->userdata('level_user') === 'admin_bidang') {
            $this->db->where('flag_user', 0);
            $this->db->like('master_pegawai.kode_unit_kerja', substr($this->session->userdata('kode_unit_kerja'), 0, 3));
            // $this->db->like('master_pegawai.kode_unit_kerja', "92100" );

        } elseif ($this->session->userdata('level_user') === 'pejabat') {
            $this->db->where('flag_user', 0);
            $this->db->like('master_pegawai.kode_unit_kerja', substr($this->session->userdata('kode_unit_kerja'), 0, 4));
        } elseif ($this->session->userdata('level_user') === 'user') {
            $this->db->where('flag_user', 0);
            $this->db->like('master_pegawai.nip_pegawai', $this->session->userdata('nip_pegawai'));
        } else {
            redirect('login');
        }
        return $all = $this->db->order_by('nama_pegawai')->get()->result_array();
    }
    public function get_pegawai_id($id_pegawai)
    {
        return $this->db->select('*')->from("master_pegawai")
            ->join('master_unit_kerja', 'master_pegawai.kode_unit_kerja=master_unit_kerja.kode_unit_kerja')
            ->join('master_satker', 'master_pegawai.kode_satker=master_satker.kode_satker')
            ->join('master_pangkat', 'master_pegawai.pangkat=master_pangkat.gol_pangkat', 'left')
            ->join('master_jabatan_struktur', 'master_pegawai.kode_jabatan_struktur=master_jabatan_struktur.kode_jabatan_struktur', 'left')
            ->join('master_jabatan_fungsional', 'master_pegawai.kode_jabatan_fungsional=master_jabatan_fungsional.kode_jabatan_fungsional', 'left')
            ->where('id_pegawai', $id_pegawai)->get()->row_array();
    }
    function update($id_pegawai, $params)
    {
        $this->db->where('id_pegawai', $id_pegawai);
        return $this->db->update('master_pegawai', $params);
    }
    function add($params)
    {
        $this->db->insert('master_pegawai', $params);
        return $this->db->insert_id();
    }

    public function get_all()
    {
        return $this->db->select('nip_pegawai, nama_pegawai')->from('master_pegawai')->where('kode_satker', $this->session->userdata('kode_satker'))->where('flag_user', '0')->order_by('nama_pegawai', 'asc')->get()->result_array();
    }

    public function get_pegawai_nip($nip_pegawai)
    {
        return $this->db->select('*')->from("master_pegawai")
            ->join('master_unit_kerja', 'master_pegawai.kode_unit_kerja=master_unit_kerja.kode_unit_kerja')
            ->join('master_satker', 'master_pegawai.kode_satker=master_satker.kode_satker')
            ->join('master_pangkat', 'master_pegawai.pangkat=master_pangkat.gol_pangkat', 'left')
            ->join('master_jabatan_struktur', 'master_pegawai.kode_jabatan_struktur=master_jabatan_struktur.kode_jabatan_struktur', 'left')
            ->join('master_jabatan_fungsional', 'master_pegawai.kode_jabatan_fungsional=master_jabatan_fungsional.kode_jabatan_fungsional', 'left')
            ->where('nip_pegawai', $nip_pegawai)->get()->row_array();
    }

    public function get_atasan($kode_unit_kerja, $kode_jabatan_struktur)
    {
        if ($kode_jabatan_struktur == 6) {
            return $this->db->select('*')->from("master_pegawai")->where('kode_unit_kerja',$kode_unit_kerja)->where('kode_jabatan_struktur',$kode_jabatan_struktur-1)->or_where('kode_jabatan_struktur',$kode_jabatan_struktur-2)->get()->row_array();
        } else if($kode_jabatan_struktur == 5 or $kode_jabatan_struktur == 4 ){
            return $this->db->select('*')->from("master_pegawai")->where('kode_unit_kerja',substr($kode_unit_kerja,0,3).'00')->where('kode_jabatan_struktur',2)->or_where('kode_jabatan_struktur',3)->get()->row_array();
        }
            else if($kode_jabatan_struktur == 3 or $kode_jabatan_struktur == 2 ){
            return $this->db->select('*')->from("master_pegawai")->where('kode_unit_kerja',substr($kode_unit_kerja,0,2).'000')->where('kode_jabatan_struktur',1)->get()->row_array();
        } 
        else if($kode_jabatan_struktur == 1 ){
            return $this->db->select('*')->from("master_pegawai")->where('kode_unit_kerja',substr($kode_unit_kerja,0,2).'000')->where('kode_jabatan_struktur',1)->get()->row_array();
        }
        else if($kode_jabatan_struktur == 0 ){
            return $this->db->select('*')->from("master_pegawai")->where('kode_unit_kerja',substr($kode_unit_kerja,0,2).'000')->where('kode_jabatan_struktur',1)->get()->row_array();
        } 
        
    }
}