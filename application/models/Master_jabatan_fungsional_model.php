<?php

// extends class Model
class Master_jabatan_fungsional_model extends CI_Model
{

    public function get_all()
    {
        return $this->db->select('*')->from("master_jabatan_fungsional")->get()->result_array();
    }
    public function get_id($kode_jabatan_fungsional)
    {
        return $this->db->select('*')->from("master_jabatan_fungsional")->where('kode_jabatan_fungsional',$kode_jabatan_fungsional)->get()->row_array();
    }
}