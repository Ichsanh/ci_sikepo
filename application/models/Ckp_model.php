<?php

// extends class Model
class Ckp_model extends CI_Model
{
    public function get_ckp($nip_pegawai, $bulan,$utama)
    {
        return $this->db->select('*,count(a.target) as jumlah_target')->from("tabel_matriks_kinerja_individu as a")
            ->join('tabel_matriks_kinerja as b ', 'a.id_matriks=b.id_matriks_kinerja', 'left')
            ->join('tabel_master_kegiatan as c', 'b.id_kegiatan=c.id_kegiatan', 'left')
            ->join('tabel_master_pekerjaan as d', 'b.id_master_pekerjaan=d.id_master_pekerjaan', 'left')
            ->join('master_pegawai as e', 'a.nip_pegawai=e.nip_pegawai', 'left')
            ->join('master_butir_fungsional as f', 'a.id_butir=f.id_butir', 'left')
            ->where('a.nip_pegawai', $nip_pegawai)
            ->where('a.bulan', $bulan)
            ->where('b.utama', $utama)
            ->group_by('a.id_skp')->get()->result_array();
    }

    function update($id_skp, $params)
    {
        $this->db->where('id_skp', $id_skp);
        return $this->db->update('tabel_matriks_kinerja_individu', $params);
    }
}