<?php


class Master_pekerjaan_model extends CI_Model
{
    var $table         = 'tabel_master_pekerjaan';
    var $table1        = "master_jenis_pekerjaan";
    var $table2        = "master_approval";
    var $table3        = "master_pegawai as a";
    var $table4        = "master_pegawai as b";

    var $join1         = "tabel_master_pekerjaan.id_jenis_pekerjaan=master_jenis_pekerjaan.id_jenis_pekerjaan";
    var $join2         = "tabel_master_pekerjaan.status_approval=master_approval.status_approval";
    var $join3         = "tabel_master_pekerjaan.created_by=a.nip_pegawai";
    var $join4         = "tabel_master_pekerjaan.approval_by=b.nip_pegawai";

    var $column_order  = array('id_master_pekerjaan', 'jenis_pekerjaan', 'rincian_pekerjaan', 'keterangan_pekerjaan', 'keterangan_approval', 'a.nama_pegawai', 'b.nama_pegawai'); //set column field database for datatable orderable
    var $column_search = array('id_master_pekerjaan', 'jenis_pekerjaan', 'rincian_pekerjaan', 'keterangan_pekerjaan', 'keterangan_approval', 'a.nama_pegawai', 'b.nama_pegawai'); //set column field database for datatable searchable 
    var $order         = array('id_master_pekerjaan' => 'asc'); // default order 

    function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {

        // $this->db->from($this->table)->join($this->table1, $this->join1)->where('tabel_master_pekerjaan.id_jenis_pekerjaan', $id_jenis_pekerjaan);
        $this->db->select('*, a.nama_pegawai as pegawai_created, b.nama_pegawai as pegawai_approval')->from($this->table)->join($this->table1, $this->join1)
            ->join($this->table2, $this->join2, 'left')
            ->join($this->table3, $this->join3, 'left')
            ->join($this->table4, $this->join4, 'left');
        $this->db->order_by('tabel_master_pekerjaan.id_jenis_pekerjaan', 'asc');

        // $this->db->from($this->table);

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                    // $this->db->like('LOWER(CAST(' . $item . ' AS TEXT))', strtolower($_POST['search']['value']));
                } else {
                    //$item = ($item == 'deadline') ? ltrim(to_char(dateline,'99999999')) :  $item ;
                    // $this->db->or_like('LOWER(CAST(' . $item . ' AS TEXT))', strtolower($_POST['search']['value']));
                    $this->db->or_like($item, $_POST['search']['value']);
                    // $this->db->like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            if ($_POST['columns'][$i]['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    //$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['columns'][$i]['search']['value']);
                    //$this->db->group_end(); //close bracket
                } else {
                    //$this->db->group_start();
                    //$item = ($item == 'deadline') ? ltrim(to_char(dateline,'99999999')) :  $item ;
                    //$this->db->like('CAST(' . $item . ' AS TEXT)', $_POST['columns'][$i]['search']['value'], 'both', false); // untuk posgre
                    $this->db->or_like($item, $_POST['columns'][$i]['search']['value']);
                    //$this->db->like($item, $_POST['columns'][$i]['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table)->join($this->table1, $this->join1)->join($this->table2, $this->join2, 'left')
            ->join($this->table3, $this->join3, 'left')
            ->join($this->table4, $this->join4, 'left');;
        // $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function update($id, $params)
    {
        $this->db->where('id_master_pekerjaan', $id);
        return $this->db->update('tabel_master_pekerjaan', $params);
    }

    function tambah($params)
    {

        return $this->db->insert('tabel_master_pekerjaan', $params);
    }



    public function cari($judul)
    {
        $judul = str_replace(" ", "%%", $judul);

        if ($this->session->userdata('lang') == 'ind') {
            return  $this->db->select("judul,level,nama_wilayah ,tipe, tabel_master_pekerjaan.id_jenis_pekerjaan, id_api, judul_eng, mfd.mfd, metadata")->from($this->table)->join($this->table1, $this->join1)->join('kelompok', 'kelompok.id_jenis_pekerjaan=tabel_master_pekerjaan.id_jenis_pekerjaan')->join('kategori', 'kategori.id_kategori=kelompok.id_kategori')->like('LOWER(judul)', strtolower($judul), 'both', false)->where('kategori.show', '1')->group_by('judul,level,nama_wilayah ,tipe, tabel_master_pekerjaan.id_jenis_pekerjaan, id_api, judul_eng, mfd.mfd, metadata')->get()->result_array();
        } elseif ($this->session->userdata('lang') == 'eng') {
            return  $this->db->select("judul,level,nama_wilayah ,tipe, tabel_master_pekerjaan.id_jenis_pekerjaan, id_api, judul_eng, mfd.mfd, metadata")->from($this->table)->join($this->table1, $this->join1)->join('kelompok', 'kelompok.id_jenis_pekerjaan=tabel_master_pekerjaan.id_jenis_pekerjaan')->join('kategori', 'kategori.id_kategori=kelompok.id_kategori')->like('LOWER(judul_eng)', strtolower($judul), 'both', false)->where('kategori.show', '1')->group_by('judul,level,nama_wilayah ,tipe, tabel_master_pekerjaan.id_jenis_pekerjaan, id_api, judul_eng, mfd.mfd, metadata')->get()->result_array();
        }
    }

    public function get_all_jenis_pekerjaan()
    {
        return  $this->db->select('*')->from("master_jenis_pekerjaan")->get()->result_array();
    }
    public function get_id_jenis_pekerjaan($id_jenis_pekerjaan)
    {
        return  $this->db->select('*')->from("master_jenis_pekerjaan")->where('id_jenis_pekerjaan', $id_jenis_pekerjaan)->get()->row_array();
    }
    public function get_all()
    {
        return  $this->db->select('id_master_pekerjaan, rincian_pekerjaan')->from("tabel_master_pekerjaan")->get()->result_array();
    }

    public function get_id($id_master_pekerjaan)
    {
        return  $this->db->select('*')->from("tabel_master_pekerjaan")->where('id_master_pekerjaan', $id_master_pekerjaan)->get()->row_array();
    }

    public function get_pekerjaan_fungsional($id_jenis_pekerjaan)
    {
        return  $this->db->select('*')->from("tabel_master_pekerjaan")->where('id_jenis_pekerjaan', $id_jenis_pekerjaan)->get()->result_array();
    }

    public function get_all_master()
    {
        return  $this->db->select('*')->from("tabel_master_pekerjaan")->join('master_jenis_pekerjaan','tabel_master_pekerjaan.id_jenis_pekerjaan=master_jenis_pekerjaan.id_jenis_pekerjaan')->get()->result_array();
    }

    function tambah_dummy($params)
    {
        return $this->db->insert('dummy_tabel_master_pekerjaan', $params);
    }
    function cek_duplikat()
    {
        $this->db->query("UPDATE dummy_tabel_master_pekerjaan
        INNER JOIN tabel_master_pekerjaan ON tabel_master_pekerjaan.rincian_pekerjaan = dummy_tabel_master_pekerjaan.rincian_pekerjaan
        SET dummy_tabel_master_pekerjaan.status = 1");

        $this->db->query('INSERT INTO tabel_master_pekerjaan (rincian_pekerjaan, keterangan_pekerjaan, id_jenis_pekerjaan, status_approval, catatan_approval, status_flag, created_by, approval_by)  
        SELECT rincian_pekerjaan, keterangan_pekerjaan, id_jenis_pekerjaan, status_approval, catatan_approval, status_flag, created_by, approval_by
        FROM dummy_tabel_master_pekerjaan WHERE status != 1');
        
        return $this->db->select('*')->from('dummy_tabel_master_pekerjaan')->where('status',1)->get()->result_array();
    }
}