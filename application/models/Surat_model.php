<?php

// extends class Model
class Surat_model extends CI_Model
{
    

    function add($params)
    {
        $this->db->insert('sipadu_surat', $params);
        return $this->db->insert_id();
    }
    public function get_all()
    {
        return $this->db->select('*')->from("sipadu_surat")->join('master_pegawai','sipadu_surat.nip=master_pegawai.nip_pegawai')->order_by('tanggal','desc')->order_by('id_surat','desc')->get()->result_array();
    }
    public function update($id_surat, $params)
    {
        $this->db->where('id_surat', $id_surat);
        $update = $this->db->update("sipadu_surat",$params);
        if($update){
        $response['status']=200;
        $response['error']=false;
        $response['message']='Data person diubah.';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Data person gagal diubah.';
        return $response;
      }
    }

    function add_download($params)
    {
        $this->db->insert('sipadu_download_surat', $params);
        return $this->db->insert_id();
    }
}