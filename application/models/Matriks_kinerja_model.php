<?php

// extends class Model
class Matriks_kinerja_model extends CI_Model
{

    public function get_all()
    {
        return  $this->db->select('*')->from("tabel_matriks_kinerja")->get()->result_array();
    }
    public function get_id($id_matriks_kinerja)
    {
        return  $this->db->select('*')->from("tabel_matriks_kinerja")->where('id_matriks_kinerja', $id_matriks_kinerja)->get()->row_array();
    }


    function tambah($params)
    {

        $this->db->insert('tabel_matriks_kinerja', $params);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    function edit($id, $params)
    {
        $this->db->where('id_matriks_kinerja', $id);
        return $this->db->update('tabel_matriks_kinerja', $params);
    }

    function edit_kinerja_individu($id, $params)
    {
        $this->db->where('id_skp', $id);
        return $this->db->update('tabel_matriks_kinerja_individu', $params);
    }


    public function hapus($id_matriks_kinerja)
    {
        $this->db->where('id_matriks_kinerja', $id_matriks_kinerja);
        return $this->db->delete('tabel_matriks_kinerja');
    }

    public function get_matriks_kinerja_pok($kode_bidang, $id_pok)
    {
        $this->db->select("*,
            count(if(e.bulan=1,e.target,null)) as count_01,
            sum(if(e.bulan=1,e.target,null)) as sum_01,
            count(if(e.bulan=2,e.target,null)) as count_02,
            sum(if(e.bulan=2,e.target,null)) as sum_02,
            count(if(e.bulan=3,e.target,null)) as count_03,
            sum(if(e.bulan=3,e.target,null)) as sum_03,
            count(if(e.bulan=4,e.target,null)) as count_04,
            sum(if(e.bulan=4,e.target,null)) as sum_04,
            count(if(e.bulan=5,e.target,null)) as count_05,
            sum(if(e.bulan=5,e.target,null)) as sum_05,
            count(if(e.bulan=6,e.target,null)) as count_06,
            sum(if(e.bulan=6,e.target,null)) as sum_06,
            count(if(e.bulan=7,e.target,null)) as count_07,
            sum(if(e.bulan=7,e.target,null)) as sum_07,
            count(if(e.bulan=8,e.target,null)) as count_08,
            sum(if(e.bulan=8,e.target,null)) as sum_08,
            count(if(e.bulan=9,e.target,null)) as count_09,
            sum(if(e.bulan=9,e.target,null)) as sum_09,
            count(if(e.bulan=10,e.target,null)) as count_10,
            sum(if(e.bulan=10,e.target,null)) as sum_10,
            count(if(e.bulan=11,e.target,null)) as count_11,
            sum(if(e.bulan=11,e.target,null)) as sum_11,
            count(if(e.bulan=12,e.target,null)) as count_12,
            sum(if(e.bulan=12,e.target,null)) as sum_12,
            count(if(e.bulan<13,e.target,null)) as count_all,
            sum( a.target_bulan_01
            + a.target_bulan_02
            + a.target_bulan_03
            + a.target_bulan_04
            + a.target_bulan_05
            + a.target_bulan_06
            + a.target_bulan_07
            + a.target_bulan_08
            + a.target_bulan_09
            + a.target_bulan_10
            + a.target_bulan_11
            + a.target_bulan_12) sum_all");
        return $this->db->from("tabel_matriks_kinerja as a")
            ->join('tabel_master_pekerjaan as b', 'a.id_master_pekerjaan=b.id_master_pekerjaan', 'left')
            ->join('tabel_master_kegiatan as c', 'a.id_kegiatan=c.id_kegiatan', 'left')
            ->join('tabel_pok as d', 'a.id_pok=d.id_pok', 'left')
            ->join('tabel_matriks_kinerja_individu as e', 'e.id_matriks=a.id_matriks_kinerja', 'left')
            ->join('master_waktu_deskripsi as f', 'a.id_waktu_deskripsi=f.id_waktu_deskripsi', 'left')
            ->where('d.bidang', $kode_bidang)
            ->where('d.id_pok', $id_pok)
            ->group_by('a.id_matriks_kinerja')->get()->result_array();
    }

    public function get_matriks_kinerja_non_pok( $id_non_pok)
    {
        $this->db->select("*,
            count(if(e.bulan=1,e.target,null)) as count_01,
            sum(if(e.bulan=1,e.target,null)) as sum_01,
            count(if(e.bulan=2,e.target,null)) as count_02,
            sum(if(e.bulan=2,e.target,null)) as sum_02,
            count(if(e.bulan=3,e.target,null)) as count_03,
            sum(if(e.bulan=3,e.target,null)) as sum_03,
            count(if(e.bulan=4,e.target,null)) as count_04,
            sum(if(e.bulan=4,e.target,null)) as sum_04,
            count(if(e.bulan=5,e.target,null)) as count_05,
            sum(if(e.bulan=5,e.target,null)) as sum_05,
            count(if(e.bulan=6,e.target,null)) as count_06,
            sum(if(e.bulan=6,e.target,null)) as sum_06,
            count(if(e.bulan=7,e.target,null)) as count_07,
            sum(if(e.bulan=7,e.target,null)) as sum_07,
            count(if(e.bulan=8,e.target,null)) as count_08,
            sum(if(e.bulan=8,e.target,null)) as sum_08,
            count(if(e.bulan=9,e.target,null)) as count_09,
            sum(if(e.bulan=9,e.target,null)) as sum_09,
            count(if(e.bulan=10,e.target,null)) as count_10,
            sum(if(e.bulan=10,e.target,null)) as sum_10,
            count(if(e.bulan=11,e.target,null)) as count_11,
            sum(if(e.bulan=11,e.target,null)) as sum_11,
            count(if(e.bulan=12,e.target,null)) as count_12,
            sum(if(e.bulan=12,e.target,null)) as sum_12,
            count(if(e.bulan<13,e.target,null)) as count_all,
            sum( a.target_bulan_01
            + a.target_bulan_02
            + a.target_bulan_03
            + a.target_bulan_04
            + a.target_bulan_05
            + a.target_bulan_06
            + a.target_bulan_07
            + a.target_bulan_08
            + a.target_bulan_09
            + a.target_bulan_10
            + a.target_bulan_11
            + a.target_bulan_12) sum_all");
        return $this->db->from("tabel_matriks_kinerja as a")
            ->join('tabel_non_pok as b','b.id_non_pok=a.id_non_pok','left')
            ->join('tabel_matriks_kinerja_individu as e', 'e.id_matriks=a.id_matriks_kinerja', 'left')
            ->join('master_waktu_deskripsi as f', 'a.id_waktu_deskripsi=f.id_waktu_deskripsi', 'left')
            ->where('a.id_non_pok', $id_non_pok)
            ->group_by('a.id_matriks_kinerja')->get()->result_array();
    }

    public function get_matriks_kinerja_all()
    {
        $this->db->select("*,
            count(if(e.bulan=1,e.target,null)) as count_01,
            sum(if(e.bulan=1,e.target,null)) as sum_01,
            count(if(e.bulan=2,e.target,null)) as count_02,
            sum(if(e.bulan=2,e.target,null)) as sum_02,
            count(if(e.bulan=3,e.target,null)) as count_03,
            sum(if(e.bulan=3,e.target,null)) as sum_03,
            count(if(e.bulan=4,e.target,null)) as count_04,
            sum(if(e.bulan=4,e.target,null)) as sum_04,
            count(if(e.bulan=5,e.target,null)) as count_05,
            sum(if(e.bulan=5,e.target,null)) as sum_05,
            count(if(e.bulan=6,e.target,null)) as count_06,
            sum(if(e.bulan=6,e.target,null)) as sum_06,
            count(if(e.bulan=7,e.target,null)) as count_07,
            sum(if(e.bulan=7,e.target,null)) as sum_07,
            count(if(e.bulan=8,e.target,null)) as count_08,
            sum(if(e.bulan=8,e.target,null)) as sum_08,
            count(if(e.bulan=9,e.target,null)) as count_09,
            sum(if(e.bulan=9,e.target,null)) as sum_09,
            count(if(e.bulan=10,e.target,null)) as count_10,
            sum(if(e.bulan=10,e.target,null)) as sum_10,
            count(if(e.bulan=11,e.target,null)) as count_11,
            sum(if(e.bulan=11,e.target,null)) as sum_11,
            count(if(e.bulan=12,e.target,null)) as count_12,
            sum(if(e.bulan=12,e.target,null)) as sum_12,
            count(if(e.bulan<13,e.target,null)) as count_all,
            sum( a.target_bulan_01
            + a.target_bulan_02
            + a.target_bulan_03
            + a.target_bulan_04
            + a.target_bulan_05
            + a.target_bulan_06
            + a.target_bulan_07
            + a.target_bulan_08
            + a.target_bulan_09
            + a.target_bulan_10
            + a.target_bulan_11
            + a.target_bulan_12) sum_all");
        return $this->db->from("tabel_matriks_kinerja as a")
            ->join('tabel_master_pekerjaan as b', 'a.id_master_pekerjaan=b.id_master_pekerjaan', 'left')
            ->join('tabel_master_kegiatan as c', 'a.id_kegiatan=c.id_kegiatan', 'left')
            ->join('tabel_pok as d', 'a.id_pok=d.id_pok', 'left')
            ->join('tabel_matriks_kinerja_individu as e', 'e.id_matriks=a.id_matriks_kinerja', 'left')
            ->join('master_waktu_deskripsi as f', 'a.id_waktu_deskripsi=f.id_waktu_deskripsi', 'left')
            ->join('master_bidang as g', 'g.kode_bidang=d.bidang', 'left')
            ->group_by('a.id_matriks_kinerja')->get()->result_array();
    }

    public function get_target_pekerjaan($id_matriks)
    {
        $this->db->select('*,
        sum(if(a.bulan=1, a.target,null)) as target_01,
        sum(if(a.bulan=2, a.target,null)) as target_02,
        sum(if(a.bulan=3, a.target,null)) as target_03,
        sum(if(a.bulan=4, a.target,null)) as target_04,
        sum(if(a.bulan=5, a.target,null)) as target_05,
        sum(if(a.bulan=6, a.target,null)) as target_06,
        sum(if(a.bulan=7, a.target,null)) as target_07,
        sum(if(a.bulan=8, a.target,null)) as target_08,
        sum(if(a.bulan=9, a.target,null)) as target_09,
        sum(if(a.bulan=10, a.target,null)) as target_10,
        sum(if(a.bulan=11, a.target,null)) as target_11,
        sum(if(a.bulan=12, a.target,null)) as target_12');
        return $this->db->from('tabel_matriks_kinerja_individu as a')
            ->join('master_pegawai as b', 'a.nip_pegawai=b.nip_pegawai', 'left')
            ->where('a.id_matriks', $id_matriks)
            ->group_by('a.nip_pegawai')->get()->result_array();
    }

    public function get_detail_pekerjaan($id_matriks)
    {
        return $this->db->select('*')->from('tabel_matriks_kinerja as a')
            ->join('tabel_master_pekerjaan as b', 'a.id_master_pekerjaan=b.id_master_pekerjaan', 'left')
            ->join('tabel_master_kegiatan as c', 'a.id_kegiatan=c.id_kegiatan', 'left')
            ->join('tabel_pok as d', 'a.id_pok=d.id_pok', 'left')
            ->join('tabel_non_pok as e', 'a.id_non_pok=e.id_non_pok', 'left')
            ->where('a.id_matriks_kinerja', $id_matriks)
            ->group_by('a.id_matriks_kinerja')
            ->get()->row_array();
    }

    public function get_total_target($id_matriks)
    {
        $this->db->select('*,
        sum( a.target_bulan_01)+
        sum( a.target_bulan_02)+
        sum( a.target_bulan_03)+
        sum( a.target_bulan_04)+
        sum( a.target_bulan_05)+
        sum( a.target_bulan_06)+
        sum( a.target_bulan_07)+
        sum( a.target_bulan_08)+
        sum( a.target_bulan_09)+
        sum( a.target_bulan_10)+
        sum( a.target_bulan_11)+
        sum( a.target_bulan_12) as sum_total');
        return $this->db->from('tabel_matriks_kinerja as a')
            ->where('a.id_matriks_kinerja', $id_matriks)
            ->group_by('a.id_matriks_kinerja')->get()->row_array();
    }

    public function get_jumlah_alokasi($id_matriks)
    {
        return $this->db->select('*, sum( a.target) as jumlah_alokasi')->from('tabel_matriks_kinerja_individu as a')
            ->join('master_pegawai as b', 'a.nip_pegawai=b.nip_pegawai', 'left')
            ->where('a.id_matriks', $id_matriks)
            ->get()->row_array();
    }

    public function get_kinerja_individu($id_skp)
    {
        return $this->db->select('*')->from('tabel_matriks_kinerja_individu as a')
            ->join('master_pegawai as b', 'a.nip_pegawai=b.nip_pegawai', 'left')
            ->where('a.id_skp', $id_skp)
            ->get()->row_array();
    }

    public function tambah_kinerja_individu($params)
    {
        return $this->db->insert('tabel_matriks_kinerja_individu', $params);
    }

    public function get_matriks_kinerja_2()
    {
        $this->db->select('*,
            count( a.target) as jumlah_target,
            sum(if(a.bulan=1, a.target,null)) as target_01,
            sum(if(a.bulan=2, a.target,null)) as target_02,
            sum(if(a.bulan=3, a.target,null)) as target_03,
            sum(if(a.bulan=4, a.target,null)) as target_04,
            sum(if(a.bulan=5, a.target,null)) as target_05,
            sum(if(a.bulan=6, a.target,null)) as target_06,
            sum(if(a.bulan=7, a.target,null)) as target_07,
            sum(if(a.bulan=8, a.target,null)) as target_08,
            sum(if(a.bulan=9, a.target,null)) as target_09,
            sum(if(a.bulan=10, a.target,null)) as target_10,
            sum(if(a.bulan=11, a.target,null)) as target_11,
            sum(if(a.bulan=12, a.target,null)) as target_12');
        return $this->db->from('tabel_matriks_kinerja_individu as a')
            ->join('tabel_matriks_kinerja as b', 'a.id_matriks=b.id_matriks_kinerja', 'left')
            ->join('tabel_master_kegiatan as c', 'b.id_kegiatan=c.id_kegiatan', 'left')
            ->join('tabel_master_pekerjaan as d', 'b.id_master_pekerjaan=d.id_master_pekerjaan', 'left')
            ->join('master_pegawai as e', 'a.nip_pegawai=e.nip_pegawai', 'left')
            ->group_by('a.id_skp')->get()->result_array();
    }

    public function get_matriks_kinerja_3()
    {
        $this->db->select('nama_pegawai,
            count(if(a.bulan=1, a.target,null)) as count_01,
            sum(if(a.bulan=1, a.target*d.ak,null)) as ak_01,
            count(if(a.bulan=2, a.target,null)) as count_02,
            sum(if(a.bulan=2, a.target*d.ak,null)) as ak_02,
            count(if(a.bulan=3, a.target,null)) as count_03,
            sum(if(a.bulan=3, a.target*d.ak,null)) as ak_03,
            count(if(a.bulan=4, a.target,null)) as count_04,
            sum(if(a.bulan=4, a.target*d.ak,null)) as ak_04,
            count(if(a.bulan=5, a.target,null)) as count_05,
            sum(if(a.bulan=5, a.target*d.ak,null)) as ak_05,
            count(if(a.bulan=6, a.target,null)) as count_06,
            sum(if(a.bulan=6, a.target*d.ak,null)) as ak_06,
            count(if(a.bulan=7, a.target,null)) as count_07,
            sum(if(a.bulan=7, a.target*d.ak,null)) as ak_07,
            count(if(a.bulan=8, a.target,null)) as count_08,
            sum(if(a.bulan=8, a.target*d.ak,null)) as ak_08,
            count(if(a.bulan=9, a.target,null)) as count_09,
            sum(if(a.bulan=9, a.target*d.ak,null)) as ak_09,
            count(if(a.bulan=10, a.target,null)) as count_10,
            sum(if(a.bulan=10, a.target*d.ak,null)) as ak_10,
            count(if(a.bulan=11, a.target,null)) as count_11,
            sum(if(a.bulan=11, a.target*d.ak,null)) as ak_11,
            count(if(a.bulan=12, a.target,null)) as count_12,
            sum(if(a.bulan=12, a.target*d.ak,null)) as ak_12,
            sum( a.target*d.ak) as ak_all,
            count(a.target) as count_all,', false);
        return $this->db->from('tabel_matriks_kinerja_individu as a')
            ->join('tabel_matriks_kinerja as b', 'a.id_matriks=b.id_matriks_kinerja', 'left')
            ->join('master_pegawai as c', 'a.nip_pegawai=c.nip_pegawai', 'left')
            ->join('master_butir_fungsional as d', 'd.butir_fungsional_grouping=b.butir_fungsional and c.kode_jabatan_fungsional=d.pekerjaan_pelaksana', 'left')
            ->where('c.kode_satker', $this->session->userdata('kode_satker'))
            ->where('a.tahun', $this->session->userdata('tahun_anggaran'))
            ->where('c.flag_user', 0)
            ->group_by('c.nip_pegawai')->get()->result_array();
    }

    function tambah_dummy($params)
    {
        return $this->db->insert('dummy_tabel_matriks_kinerja', $params);
    }
    function cek_duplikat()
    {
        $this->db->query("UPDATE dummy_tabel_matriks_kinerja
        INNER JOIN tabel_matriks_kinerja ON tabel_matriks_kinerja.id_master_pekerjaan = dummy_tabel_matriks_kinerja.id_master_pekerjaan
        and tabel_matriks_kinerja.id_kegiatan = dummy_tabel_matriks_kinerja.id_kegiatan and tabel_matriks_kinerja.tahun = dummy_tabel_matriks_kinerja.tahun
        SET dummy_tabel_matriks_kinerja.status = 1");

        $this->db->query('INSERT INTO tabel_matriks_kinerja (id_pok,id_kegiatan,id_master_pekerjaan,satuan,tahun,waktu,id_waktu_deskripsi,biaya,jenis_fungsional,target_bulan_01,target_bulan_02,target_bulan_03,target_bulan_04,target_bulan_05,target_bulan_06,target_bulan_07,target_bulan_08,target_bulan_09,target_bulan_10,target_bulan_11,target_bulan_12)  
        SELECT id_pok,id_kegiatan,id_master_pekerjaan,satuan,tahun,waktu,id_waktu_deskripsi,biaya,jenis_fungsional,target_bulan_01,target_bulan_02,target_bulan_03,target_bulan_04,target_bulan_05,target_bulan_06,target_bulan_07,target_bulan_08,target_bulan_09,target_bulan_10,target_bulan_11,target_bulan_12
        FROM dummy_tabel_matriks_kinerja WHERE status != 1');
        
        return $this->db->select('*')->from('dummy_tabel_matriks_kinerja')->where('status',1)->get()->result_array();
    }

    function cek_duplikat_non_pok()
    {
        $this->db->query("UPDATE dummy_tabel_matriks_kinerja
        INNER JOIN tabel_matriks_kinerja ON tabel_matriks_kinerja.nama_pekerjaan_non_pok = dummy_tabel_matriks_kinerja.nama_pekerjaan_non_pok
        and tabel_matriks_kinerja.tahun = dummy_tabel_matriks_kinerja.tahun and tabel_matriks_kinerja.id_non_pok = dummy_tabel_matriks_kinerja.id_non_pok
        SET dummy_tabel_matriks_kinerja.status = 1");

        $this->db->query('INSERT INTO tabel_matriks_kinerja (id_non_pok,nama_pekerjaan_non_pok,satuan,tahun,waktu,id_waktu_deskripsi,biaya,jenis_fungsional,target_bulan_01,target_bulan_02,target_bulan_03,target_bulan_04,target_bulan_05,target_bulan_06,target_bulan_07,target_bulan_08,target_bulan_09,target_bulan_10,target_bulan_11,target_bulan_12,utama)  
        SELECT id_non_pok,nama_pekerjaan_non_pok,satuan,tahun,waktu,id_waktu_deskripsi,biaya,jenis_fungsional,target_bulan_01,target_bulan_02,target_bulan_03,target_bulan_04,target_bulan_05,target_bulan_06,target_bulan_07,target_bulan_08,target_bulan_09,target_bulan_10,target_bulan_11,target_bulan_12,utama
        FROM dummy_tabel_matriks_kinerja WHERE status != 1');
        
        return $this->db->select('*')->from('dummy_tabel_matriks_kinerja')->where('status',1)->get()->result_array();
    }
}