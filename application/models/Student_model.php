<?php

// extends class Model
class Student_model extends CI_Model
{

    // response jika field ada yang kosong
    public function empty_response()
    {
        $response['status'] = 502;
        $response['error'] = true;
        $response['message'] = 'Field tidak boleh kosong';
        return $response;
    }

    public function login_student($username, $password)
    {
        $all = $this->db->select('*')->from("student")->where('username', $username)->where('password', $password)->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['userData'] = $all;
        return $response;
    }

    // function untuk insert data ke tabel student
    public function add_student($name, $address, $age)
    {

        if (empty($name) || empty($address) || empty($age)) {
            return $this->empty_response();
        } else {
            $data = array(
                "name" => $name,
                "address" => $address,
                "age" => $age
            );

                $insert = $this->db->insert("student", $data);

            if ($insert) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Data student ditambahkan.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Data student gagal ditambahkan.';
                return $response;
            }
        }
    }

    // mengambil semua data student
    public function all_student()
    {

        $all = $this->db->get("student")->result();
        $response['status'] = 200;
        $response['error'] = false;
        $response['student'] = $all;
        return $response;
    }

    // mengambil semua data student by id
    public function student_id($id)
    {

        $all = $this->db->select('*')->from("student")->where('id', $id)->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['student'] = $all;
        return $response;
    }

    // hapus data student
    public function delete_student($id)
    {

        if ($id == '') {
            return $this->empty_response();
        } else {
            $where = array(
                "id" => $id
            );

            $this->db->where($where);
            $delete = $this->db->delete("student");
            if ($delete) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Data student dihapus.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Data student gagal dihapus.';
                return $response;
            }
        }
    }

    // update student
    public function update_student($id, $name, $address, $age)
    {

        if ($id == '' || empty($name) || empty($address) || empty($age)) {
            return $this->empty_response();
        } else {
            $where = array(
                "id" => $id
            );

            $set = array(
                "name" => $name,
                "address" => $address,
                "age" => $age
            );

            $this->db->where($where);
            $update = $this->db->update("student", $set);
            if ($update) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Data student diubah.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Data student gagal diubah.';
                return $response;
            }
        }
    }
}