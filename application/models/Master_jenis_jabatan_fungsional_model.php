<?php

// extends class Model
class Master_jenis_jabatan_fungsional_model extends CI_Model
{

    public function get_all()
    {
        return  $this->db->select('*')->from("master_jenis_jabatan_fungsional")->get()->result_array();
    }

    public function get_id($kode_jenis_jabatan_fungsional)
    {
        return  $this->db->select('*')->from("master_jenis_jabatan_fungsional")->where('kode_jenis_jabatan_fungsional', $kode_jenis_jabatan_fungsional)->get()->row_array();
    }
}
