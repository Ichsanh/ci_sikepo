<?php

// extends class Model
class Waktu_deskripsi_model extends CI_Model
{

    public function get_all()
    {
        return  $this->db->select('*')->from("master_waktu_deskripsi")->get()->result_array();
    }
    public function get_id($id_waktu_deskripsi)
    {
        return  $this->db->select('*')->from("master_waktu_deskripsi")->where('id_waktu_deskripsi', $id_waktu_deskripsi)->get()->row_array();
    }
}
