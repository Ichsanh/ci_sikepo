<?php

// extends class Model
class Survei_model extends CI_Model
{
    public function get_survei($nip_pegawai_lama, $tahun, $bulan)
    {
        $all = $this->db->select('*')->from("pegawai_rincian_survei")->where('nip', $nip_pegawai_lama)->where('tahun', $tahun)->where('bulan', $bulan)->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['surveiData'] = $all;
        return $response;
    }

    public function insert_survei($nilai)
    {
        $data = array(
                "nilai" => $nilai,
                "tahun" => date('Y'),
                "bulan" => date('m'),
                "tanggal" => date('d'),

            );

        $insert = $this->db->insert("pst_survei", $data);
        $response['status'] = 200;
        $response['error'] = false;
        $response['surveiInsert'] = $insert;
        return $response;
    }
    
}