<?php

// extends class Model
class Gaji_model extends CI_Model
{
    public function get_gaji($nip_pegawai_lama, $tahun, $bulan)
    {
        $all = $this->db->select('*')->from("pegawai_rincian_gaji")->where('nip', $nip_pegawai_lama)->where('tahun', $tahun)->where('bulan', $bulan)->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['gajiData'] = $all;
        return $response;
    }
    public function get_gaji_terakhir($nip_pegawai_lama)
    {
        $all = $this->db->select('*')->from("pegawai_rincian_gaji")->where('nip', $nip_pegawai_lama)->order_by('id', 'DESC')->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['gajiData'] = $all;
        return $response;
    }
    public function get_tukin($nip_pegawai_lama, $tahun, $bulan)
    {
        $all = $this->db->select('*')->from("pegawai_rincian_tukin")->where('nip', $nip_pegawai_lama)->where('tahun', $tahun)->where('bulan', $bulan)->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['tukinData'] = $all;
        return $response;
    }
    public function get_tukin_terakhir($nip_pegawai_lama)
    {
        $all = $this->db->select('*')->from("pegawai_rincian_tukin")->where('nip', $nip_pegawai_lama)->order_by('id', 'DESC')->get()->row();
        $response['status'] = 200;
        $response['error'] = false;
        $response['tukinData'] = $all;
        return $response;
    }

    public function get_gajiTukin_terakhir($nip_pegawai_lama)
    {
        $gaji = $this->db->select('bulan as bulan_gaji, nilai_netto_2 as nilai_gaji')->from("pegawai_rincian_gaji")->where('nip', $nip_pegawai_lama)->order_by('id', 'DESC')->get()->row_array();
        $tukin = $this->db->select('bulan as bulan_tukin, nilai_netto as nilai_tukin')->from("pegawai_rincian_tukin")->where('nip', $nip_pegawai_lama)->order_by('id', 'DESC')->get()->row_array();
        $all = array_merge($gaji, $tukin);

        $response['status'] = 200;
        $response['error'] = false;
        $response['feedData'] = ($all);
        return $response;
    }
}