<?php

// extends class Model
class Skp_model extends CI_Model
{
    public function get_skp($nip_pegawai, $tahun)
    {
        return $this->db->select('*,
            sum(a.target) as sum_target,
            sum(a.realisasi) as sum_realisasi,
            avg(a.kualitas) as avg_kualitas')
            ->from("tabel_matriks_kinerja_individu as a")
            ->join('tabel_matriks_kinerja as b ', 'a.id_matriks=b.id_matriks_kinerja', 'left')
            ->join('tabel_master_kegiatan as c', 'b.id_kegiatan=c.id_kegiatan', 'left')
            ->join('tabel_master_pekerjaan as d', 'b.id_master_pekerjaan=d.id_master_pekerjaan', 'left')
            ->join('master_pegawai as e', 'a.nip_pegawai=e.nip_pegawai', 'left')
            ->join('master_butir_fungsional as f', 'a.id_butir=f.id_butir', 'left')
            ->where('a.nip_pegawai', $nip_pegawai)
            ->where('a.tahun', $tahun)
            ->group_by('a.id_matriks')->get()->result_array();
    }

    function update($id_skp, $params)
    {
        $this->db->where('id_skp', $id_skp);
        return $this->db->update('tabel_matriks_kinerja_individu', $params);
    }
}